#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
"""
Run a model simulation from the arguments given in the command.
"""
import numpy as np
from iEEG_STC import utils
import models
import os
import sys
import time
import ast
import pandas as pd
t0 = time.perf_counter()

# Default parameters
params = {'N': 400,  # Network size. If 'grid' is given this will be ignored
          'k': .9,  # Largest eigenvalue of the network before applying fi, fe
          'fi': 1.0,  # Scaling of inhibitory nodes
          'fe': 1.0,  # Scaling of excitatory nodes
          'alpha': .2,  # Fraction of inhibitory nodes
          'plink': 1.0,  # Probabiltiy that an link exists
          'n_runs': 2,  # Number of simulations
          'from_run': 0,  # Start with this run number
          'max_t': 200,  # How long the simulations should run and until which timestep the measure should be calculated
          'from_t': 0,  # From which timestep on the measure should be calculated
          'data_path': '../../stc_model_data',  # If undefined this will save to the data directory looking above
          'prewire': None,  # Rewiring probability, defaults to None using the Erdos-Renyi network topology
          'subsamplingrate': 1,  # Fractions of subsampling of the neurons
          'initial_activity': .1,  # Which fraction of initial active neurons should be used
          'dd_connectivity': None,  # Distance dependent connectivity sigma
          'grid': 40  # If given make grid layout, must be tuple(dim,dim)
          }
# Read arguments and parse them to the parameters
for arg in sys.argv[1:]:
    key, item = arg.split('=')
    if 'data_path' in key:
        params[key] = item
    else:
        params[key] = ast.literal_eval(item)

if params['grid'] is not None:
    params['N'] = int(params['grid']**2)
print(params)
# Construct the the path names
save_name = os.path.join(params['data_path'], f"XXXX_{params['k']}_{params['N']}_{params['alpha']}_" +
                         f"{params['fe']}_{params['fi']}_{params['plink']}_")
if params['prewire'] is not None:
    save_name += f"{params['prewire']}_"
if params['dd_connectivity'] is not None:
    save_name += f"ddc{params['dd_connectivity']}_"
if params['grid'] is not None:
    save_name += 'grid_'
    dists, coords = models.make_2dgrid_distances(params['grid'])
    dd_connectivity = np.exp(-(dists**2) / 4)
save_name += (f"{params['from_t']}_{params['max_t']}_" +
              f"{params['subsamplingrate']}_" +
              f"ia{params['initial_activity']}")

save_name += f"_{params['from_run']}_{params['n_runs']}.pkl"

acf = pd.DataFrame(columns=np.arange(params['max_t']))
surr_acf = acf.copy(deep=True)
bins = np.linspace(0, np.max(dists), int(np.max(dists))+1)
cc = pd.DataFrame(columns=bins)
surr_cc = cc.copy(deep=True)
# Every initial_activity_coarseness step gives a initial activity
# Start simulations
for n in range(params['from_run'], params['from_run'] + params['n_runs']):
    t1 = time.perf_counter()

    np.random.seed(n)
    # Make initial activities
    initial_activity = np.full(fill_value=False, shape=(params['N']))
    # initial_activity[np.random.randint(params['N'])] = True
    initial_activity[np.random.choice(params['N'], 1, replace=False)] = True
    # Build the weights for the calculation
    weights = models.make_uniform_weights(params['N'], alpha=params['alpha'])
    if (params['dd_connectivity'] is not None) and (params['plink'] == 1):
        weights *= models.make_distance_dependent_connectivity(params['N'], params['dd_connectivity'])
    elif params['dd_connectivity'] is not None:
        raise ValueError('If dd_connectivity is given plink must be 1')
    if params['plink'] != 1 or params['prewire'] is not None:
        if params['prewire'] is not None:
            weights *= models.make_watts_strogatz_adjacency(params['N'], params['plink'], params['prewire'], seed=n)
        else:
            weights *= models.make_erdos_renyi_adjacency(params['N'],  params['plink'], seed=n)
    if params['grid'] is not None:

        weights *= dd_connectivity
    weights /= models.get_largest_eigenvalue(weights)
    weights *= params['k']
    # Change the weights to conform with fi, fe
    weights[weights < 0] *= params['fi']
    weights[weights > 0] *= params['fe']

    # Always seed the simulations with the same random numbers according to its index
    np.random.seed(n)
    # Simulate
    data = pd.DataFrame(models.meisel2020_run_t_series(weights, initial_activity, max_t=params['max_t'],
                                                       rands=np.random.random(size=(params['max_t'], params['N']))))
    # background = np.full(shape=(params['max_t'], params['N']), fill_value=False)
    # background[np.arange(params['max_t']), np.random.choice(params['N'], params['max_t'])] = True
    # data = pd.DataFrame(models.meisel2020_run_t_series_with_background(weights, initial_activity, max_t=params['max_t'],
    #                                                                    background=background,
    #                                                                    rands=np.random.random(size=(params['max_t'],
    #                                                                                                 params['N']))))

    # full_data[n] = data.to_numpy()
    print(f'Simulation {n} complete in {time.perf_counter()-t1:.2f} sec.')
    t1 = time.perf_counter()
    acf.loc[n] = utils.estimated_autocorrelation(data.copy(deep=True).mean(axis=1).to_frame()).mean(axis=1)
    # out = np.array([np.dot(dists, row) for _, row in data.iterrows()]) / 5
    # out = data[(data != 0).any(axis=1)]
    where = np.random.choice(params['N'], 50, replace=False)
    sparse_dists = pd.DataFrame(dists).iloc[where, where]
    cc.loc[n] = utils.cc_by_binned(pd.DataFrame(data).iloc[:, where], sparse_dists, cc.columns)
    data = data.apply(np.random.permutation)
    surr_acf.loc[n] = utils.estimated_autocorrelation(data.copy(deep=True).mean(axis=1).to_frame()).mean(axis=1)
    surr_cc.loc[n] = utils.cc_by_binned(pd.DataFrame(data).iloc[:, where], sparse_dists, cc.columns)

    print(f'Measure {n} calculated in {time.perf_counter() - t1:.2f} sec.')
# acf.to_pickle(save_name.replace('XXXX', 'acf'))
# cc.to_pickle(save_name.replace('XXXX', 'cc'))
# surr_cc.to_pickle(save_name.replace('XXXX', 'cc').replace('.pkl', '_surr.pkl'))
# surr_acf.to_pickle(save_name.replace('XXXX', 'acf').replace('.pkl', '_surr.pkl'))
print(f'Execution Time {time.perf_counter()-t0:.2f} sec.')
