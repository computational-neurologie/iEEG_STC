#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import numpy as np
from iEEG_STC import utils
import pandas as pd
import glob
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import seaborn as sns
from scipy.stats import spearmanr
from scipy.optimize import curve_fit
from matplotlib.ticker import ScalarFormatter
sns.set()
sns.set_theme(style='whitegrid')
font = {'family': 'sans-serif',
        'sans-serif': 'Arial',
        'weight': 'normal',
        'size': 11}
plt.rc('font', **font)
plt.rc('xtick', labelsize=11)
plt.rc('ytick', labelsize=11)
plt.rc('text', usetex=True)
colors = np.array([(0.8392156862745098, 0.15294117647058825, 0.1568627450980392),
                   (1.0, 0.4980392156862745, 0.054901960784313725),
                   (0.17254901960784313, 0.6274509803921569, 0.17254901960784313),
                   (0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
                   (0.5803921568627451, 0.403921568627451, 0.7411764705882353)])
AED_colors = sns.color_palette('tab10')
names = ['MT', 'LIP', 'LPFC', 'OFC', 'ACC']
txt_box_props = dict(boxstyle='round', facecolor='white', alpha=.5)
empty_rect = Rectangle((0, 0), 1, 1, fill=False, edgecolor='none', visible=False)


def AnnoMe(x1, x2, y, TXT, ax_object, dh=.1):

    ax_object.plot([x1, x1, x2, x2], [y, y+dh, y+dh, y], lw=1, c='k')
    ax_object.text((x1+x2)*.5, y+dh, TXT, ha='center', va='bottom', color='k', size=12)


def make_sig_str(pval):
    if pval > .05:
        return 'n.s.'
    elif pval == 0:
        return '$p<$m.p.'
    else:
        return f'$p<${0.001}'


def power_law(x, a, b):
    return np.log(x/b)/np.log(a)

# For now you have to change all glob.glob(files) by hand before running. This will be changes in the future
fe_settings = [0.95, 1.0]
fe_lrtc = pd.DataFrame(columns=fe_settings)
fe_lrsc = pd.DataFrame(columns=fe_settings)
surr_fe_lrtc = pd.DataFrame(columns=fe_settings)
surr_fe_lrsc = pd.DataFrame(columns=fe_settings)
from_avg_fe_lrtc = pd.DataFrame(columns=fe_settings, index=['data', 'surr'], dtype=float)
from_avg_fe_lrsc = pd.DataFrame(columns=fe_settings, index=['data', 'surr'], dtype=float)
path = '/Users/paulmueller/BIH_Neuro/coding_projects/LRTC_MODEL/lrstc_project_model_data'
for s in fe_settings:
    # surrogate data
    acf = pd.DataFrame()
    for file in glob.glob(f'{path}/acf_1.0_1600_0.2_{s}_1.0_1.0_grid_0_2000_1_ia0.1_*1000_bg_surr.pkl'):
        acf = pd.concat([acf, pd.read_pickle(file)], ignore_index=True)
    print(len(acf))
    cc = pd.DataFrame()
    for file in glob.glob(f'{path}/cc_1.0_1600_0.2_{s}_1.0_1.0_grid_0_2000_1_ia0.1_*1000_bg_surr.pkl'):
        cc = pd.concat([cc, pd.read_pickle(file)], ignore_index=True)
    print(len(cc))
    tmp, _ = utils.hwhm(acf.T)
    surr_fe_lrtc[s] = tmp
    tmp = cc.loc[:, 1:10].mean(axis=1)
    surr_fe_lrsc[s] = tmp
    acf = acf.mean(axis=0)
    cc = cc.mean(axis=0)
    from_avg_fe_lrsc.loc['surr', s] = cc.loc[1:10].mean()
    from_avg_fe_lrtc.loc['surr', s] = utils.hwhm(acf.to_frame())[0].squeeze()
    # Real data
    acf = pd.DataFrame()
    for file in glob.glob(f'{path}/acf_1.0_1600_0.2_{s}_1.0_1.0_grid_0_200_1_ia0.1_*1000_bg.pkl'):
        acf = pd.concat([acf, pd.read_pickle(file)], ignore_index=True)
    cc = pd.DataFrame()
    for file in glob.glob(f'{path}/cc_1.0_1600_0.2_{s}_1.0_1.0_grid_0_2000_1_ia0.1_*1000_bg.pkl'):
        cc = pd.concat([cc, pd.read_pickle(file)], ignore_index=True)
    tmp, _ = utils.hwhm(acf.T)
    fe_lrtc[s] = tmp
    tmp = cc.loc[:, 1:10].mean(axis=1)
    fe_lrsc[s] = tmp
    cc = cc.mean(axis=0)
    acf = acf.mean(axis=0)
    from_avg_fe_lrsc.loc['data', s] = cc.loc[1:10].mean()
    from_avg_fe_lrtc.loc['data', s] = utils.hwhm(acf.to_frame())[0].squeeze()

#
settings =  [ 0.8, 0.85,  0.9,  0.95, 1.0, 1.05, 1.1, 1.15, 1.2]
# cov_settings, roi_settings, settings = [0.8,0.9,1.0,1.1, 1.2],   [0.8,0.9,1.0,1.1, 1.2],  [0.8,0.9,1.0,1.1, 1.2]
cov_settings =   [ 0.8, 0.85,  0.9,  0.95, 1.0, 1.05, 1.1, 1.15, 1.2]
roi_settings =  [ 0.8, 0.85,  0.9,  0.95, 1.0, 1.05, 1.1, 1.15, 1.2]
# settings = [0.9, 0.925, 0.95, 0.975, 1.0,1.05,1.1, 1.15, 1.2, 1.25]

lrtc = pd.DataFrame(columns=settings)
lrsc = pd.DataFrame(columns=settings)
surr_lrtc = pd.DataFrame(columns=settings)
surr_lrsc = pd.DataFrame(columns=settings)
from_avg_lrtc = pd.DataFrame(columns=settings, index=['data', 'surr'], dtype=float)
from_avg_lrsc = pd.DataFrame(columns=settings, index=['data', 'surr'], dtype=float)
fig_cc, ax_cc = plt.subplots()
for s in settings:
    # surrogate data
    acf = pd.DataFrame()
    for file in glob.glob(f'{path}/acf_{s}_1600_0.2_1.0_1.0_1.0_grid_0_2000_1_ia0.1_*1000_bg_surr.pkl'):
        acf = pd.concat([acf, pd.read_pickle(file)], ignore_index=True)
    print(len(acf))
    cc = pd.DataFrame()
    for file in glob.glob(f'{path}/cc_{s}_1600_0.2_1.0_1.0_1.0_grid_0_2000_1_ia0.1_*1000_bg_surr.pkl'):
        cc = pd.concat([cc, pd.read_pickle(file)], ignore_index=True)

    print(len(cc))
    tmp, _ = utils.hwhm(acf.T)
    surr_lrtc[s] = tmp
    # tmp = cc.loc[:, 1:10].mean(axis=1)
    # surr_lrsc[s] = tmp
    cc = cc.mean(axis=0)
    tmp, _ = utils.hwhm(cc.loc[ 1:].to_frame())
    surr_lrsc[s] = tmp

    acf = acf.mean(axis=0)

    from_avg_lrsc.loc['surr', s] = cc.loc[1:10].mean()
    from_avg_lrtc.loc['surr', s] = utils.hwhm(acf.to_frame())[0].squeeze()
    # Real data
    acf = pd.DataFrame()
    for file in glob.glob(f'{path}/acf_{s}_1600_0.2_1.0_1.0_1.0_grid_0_2000_1_ia0.1_*1000_bg.pkl'):
        acf = pd.concat([acf, pd.read_pickle(file)], ignore_index=True)
    print(len(acf))
    cc = pd.DataFrame()
    for file in glob.glob(f'{path}/cc_{s}_1600_0.2_1.0_1.0_1.0_grid_0_2000_1_ia0.1_*1000_bg.pkl'):
        cc = pd.concat([cc, pd.read_pickle(file)], ignore_index=True)
    print(len(cc))
    tmp, _ = utils.hwhm(acf.T)
    lrtc[s] = tmp
    # tmp = cc.loc[:, 1:10].mean(axis=1)

    # lrsc[s] = tmp
    cc = cc.mean(axis=0)
    tmp, _ = utils.hwhm(cc.loc[ 1:].to_frame())
    lrsc[s] = tmp

    ax_cc.plot(cc.loc[ 1:].index, cc.loc[ 1:], label=s)
    acf = acf.mean(axis=0)

   # from_avg_lrsc.loc['data', s] = cc.loc[1:10].mean()
    from_avg_lrsc.loc['data', s] = utils.hwhm(cc.to_frame())[0].squeeze()
    from_avg_lrtc.loc['data', s] = utils.hwhm(acf.to_frame())[0].squeeze()
ax_cc.legend()
fig_cc.savefig('../../cc_test.pdf')
non_null = ~(pd.isnull(lrtc).any(axis=1) | pd.isnull(lrsc).any(axis=1))
lrtc = lrtc.loc[non_null]
lrsc = lrsc.loc[non_null]
non_null = ~(pd.isnull(surr_lrtc).any(axis=1) | pd.isnull(surr_lrsc).any(axis=1))
surr_lrtc = surr_lrtc.loc[non_null]
surr_lrsc = surr_lrsc.loc[non_null]
data = pd.merge(pd.melt(lrtc.loc[:, cov_settings], value_name='LRTC')['LRTC'],
                pd.melt(lrsc.loc[:, cov_settings], value_name='LRSC')['LRSC'],
                left_index=True, right_index=True)
surr_data = pd.merge(pd.melt(surr_lrtc.loc[:, cov_settings], value_name='LRTC')['LRTC'],
                     pd.melt(surr_lrsc.loc[:, cov_settings], value_name='LRSC')['LRSC'],
                     left_index=True, right_index=True)

lrtc = lrtc.loc[:, roi_settings]
lrsc = lrsc.loc[:, roi_settings]
# Plotting
width = 0.5
ci = 95

fig = plt.figure(figsize=(9.6, 6.4))
ax0 = plt.subplot2grid((2, 10), (0, 0), colspan=2, rowspan=1)
ax1 = plt.subplot2grid((2, 10), (0, 2), colspan=2, rowspan=1)
ax2 = plt.subplot2grid((2, 10), (0, 4), colspan=3, rowspan=1)
ax3 = plt.subplot2grid((2, 10), (0, 7), colspan=3, rowspan=1, sharey=ax2)
ax4 = plt.subplot2grid((2, 10), (1, 0), colspan=5, rowspan=1)
ax5 = plt.subplot2grid((2, 10), (1, 5), colspan=5, rowspan=1)
# Plot LRSC

bplot_0 = sns.barplot(x=r'$f_\mathrm{e}$', y="LRSC",
                      data=pd.melt(fe_lrsc, var_name=r'$f_\mathrm{e}$', value_name='LRSC'),
                      capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=AED_colors, zorder=2, ax=ax0,
                      palette=AED_colors, ci=ci)
surr_bp0 = sns.barplot(x=r'$f_\mathrm{e}$', y="LRSC",
                       data=pd.melt(surr_fe_lrsc, var_name=r'$f_\mathrm{e}$', value_name='LRSC'),
                       capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=['gray', 'gray'], zorder=2, ax=ax0,
                       palette=['gray', 'gray'], ci=ci)

i = 0
for b0 in bplot_0.patches:
    if i < 2:
        centre = b0.get_x() + b0.get_width() / 2.
    else:
        centre = b0.get_x() + b0.get_width() / 2. + width / 2
    b0.set_x(centre - width / 2)
    b0.set_width(width)

    i += 1
i = 0
for l0 in bplot_0.lines:
    if i > 5:
        if l0._linewidth==1.2:
            l0.set_data(l0._x+width/2, l0._y)
            pass
    i += 1


bplot_1 = sns.barplot(x=r'$f_\mathrm{e}$', y="LRTC",
                      data=pd.melt(fe_lrtc, var_name=r'$f_\mathrm{e}$', value_name='LRTC'),
                      capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=AED_colors, zorder=2, ax=ax1,
                      palette=AED_colors, ci=ci)
surr_bplot_1 = sns.barplot(x=r'$f_\mathrm{e}$', y="LRTC",
                           data=pd.melt(surr_fe_lrtc, var_name=r'$f_\mathrm{e}$', value_name='LRTC'),
                           capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=['gray', 'gray'], zorder=2, ax=ax1,
                           palette=['gray', 'gray'], ci=ci)
i = 0
for b0 in bplot_1.patches:
    if i < 2:
        centre = b0.get_x() + b0.get_width() / 2.
    else:
        centre = b0.get_x() + b0.get_width() / 2. + width / 2
    b0.set_x(centre - width / 2)
    b0.set_width(width)
    i += 1
i = 0
for l0 in bplot_1.lines:
    if i > 5:
        if l0._linewidth==1.2:
            l0.set_data(l0._x+width/2, l0._y)
            pass
    i += 1

randomer = np.arange(0, len(data))  # , len(data)//1000)
sns.scatterplot(x=data.loc[randomer, 'LRTC'], y=data.loc[randomer, 'LRSC'], ax=ax2, alpha=1)


sns.scatterplot(x=surr_data.loc[randomer, 'LRTC'], y=surr_data.loc[randomer, 'LRSC'], ax=ax3, color='gray', alpha=1)
#
# fit_power = curve_fit(power_law, data['LRTC'], data['LRSC'], bounds=(0.00001, np.inf), max_nfev=2000)
x = np.linspace(1, np.max(data['LRTC'])*1.05, 100)
# log_fit, = ax2.plot(x, power_law(x, fit_power[0][0], fit_power[0][1]), color='k')
rho, pval = spearmanr(data['LRTC'], data['LRSC'])
print(f'data {rho}, {pval}')
if pval == 0:
    pstr = '$p<$m.p.'
elif pval < 0.01:
    pstr = f'$p=${pval:.2e}'
else:
    pstr = f'$p=${pval:.2f}'
# ax2.legend(handles=[log_fit,
#                     empty_rect, empty_rect],
#            labels=[f'$\log_{{{fit_power[0][0]:.0e}}}(x/{fit_power[0][1]:.2f})$',
#                    r'Spearman $\rho=$' + f'{rho:.2f}', pstr])
rho, pval = spearmanr(surr_data['LRTC'], surr_data['LRSC'])
print(f'surr {rho}, {pval}')
pstr = f'$p=${pval:.2e}' if pval < 0.01 else f'$p=${pval:.2f}'
ax3.legend(handles=[empty_rect, empty_rect],
           labels=[r'Spearman $\rho=$' + f'{rho:.2f}', pstr])
bplot_4 = sns.barplot(x=r'$\lambda$', y="LRSC",
                      data=pd.melt(lrsc.loc[:, roi_settings], var_name=r'$\lambda$', value_name='LRSC'),
                      capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=colors[3], zorder=2, ax=ax4,
                      palette=[colors[3], colors[3]], ci=ci)
surr_bplot_4 = sns.barplot(x=r'$\lambda$', y="LRSC",
                           data=pd.melt(surr_lrsc.loc[:, roi_settings], var_name=r'$\lambda$', value_name='LRSC'),
                           capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=['gray', 'gray'], zorder=2, ax=ax4,
                           palette=['gray', 'gray'], ci=ci)
i = 0
for b2 in bplot_4.patches:
    if i < 11:
        centre = b2.get_x() + b2.get_width() / 2.
    else:
        centre = b2.get_x() + b2.get_width() / 2. + width / 2
    b2.set_x(centre - width / 2)
    b2.set_width(width)
    i += 1
i = 0
for l2 in bplot_4.lines:
    if i > 32:
        if l2._linewidth == 1.2:
            l2.set_data(l2._x + width / 2, l2._y)
    i += 1


bplot_5 = sns.barplot(x=r'$\lambda$', y="LRTC",
                      data=pd.melt(lrtc.loc[:, roi_settings], var_name=r'$\lambda$', value_name='LRTC'),
                      capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=[colors[3], colors[3]], zorder=2, ax=ax5,
                      palette=[colors[3], colors[3]], ci=ci)
surr_bplot_5 = sns.barplot(x=r'$\lambda$', y="LRTC",
                           data=pd.melt(surr_lrtc.loc[:, roi_settings], var_name=r'$\lambda$', value_name='LRTC'),
                           capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=['gray', 'gray'], zorder=2, ax=ax5,
                           palette=['gray', 'gray'], ci=ci)
i = 0
for b2 in bplot_5.patches:
    if i < 11:
        centre = b2.get_x() + b2.get_width() / 2.
    else:
        centre = b2.get_x() + b2.get_width() / 2. + width / 2
    b2.set_x(centre - width / 2)
    b2.set_width(width)
    i += 1
i = 0
for l2 in bplot_5.lines:
    if i > 32:
        if l2._linewidth == 1.2:
            l2.set_data(l2._x + width / 2, l2._y)
    i += 1


ax0.set_xlim(-width*2/3, 1+width*2/3+width/2)
ax0.set_ylim(0, 0.05)
ax1.set_xlim(-width*2/3, 1+width*2/3+width/2)
ymin = np.min(data['LRSC']) - np.abs(0.05 * np.min(data['LRSC']))
ymax = 0.10 #np.max(data['LRSC']) + np.abs(0.02 * np.max(data['LRSC']))
ax2.set_xlim(np.min(data['LRTC']) * 0.98, np.max(data['LRTC']) * 1.05)
ax2.set_ylim(ymin, ymax)
ax3.set_xlim(np.min(surr_data['LRTC']) * 0.95, np.max(surr_data['LRTC']) * 1.05)
ax4.set_xlim(-width*2/3, len(roi_settings)-1+width*2/3+width/2)
ax5.set_xlim(-width*2/3,  len(roi_settings)-1+width*2/3+width/2)


ax0.set_xlabel('$f_\mathrm{exc}$')
ax1.set_xlabel('$f_\mathrm{exc}$')
ax0.set_ylabel('SC')
ax1.set_ylabel('TC')
ax2.set_ylabel('SC')
ax3.set_ylabel('SC')
ax4.set_ylabel('SC (similar to TC)')
ax5.set_ylabel('TC')
ax2.set_xlabel('TC')
ax3.set_xlabel('TC')
ax4.set_xlabel('$\lambda$')
# plt.setp(ax3.get_yticklabels(), visible=False)
# ax3.set_ylabel(None)
ax2.set_xscale('log')
ax3.set_xscale('log')

ax2.set_xticks([2, 10, 50])
ax2.get_xaxis().set_major_formatter(ScalarFormatter())
ax3.set_xticks([2, 10])
ax3.get_xaxis().set_major_formatter(ScalarFormatter())

sns.despine(offset=1, trim=True, left=True, bottom=True, ax=ax0)
sns.despine(offset=1, trim=True, left=True, bottom=True, ax=ax1)
sns.despine(offset=1, trim=True, left=True, bottom=True, ax=ax4)
sns.despine(offset=1, trim=True, left=True, bottom=True, ax=ax5)

plt.tight_layout()

plt.savefig('../../lrstc_to_criticality_hwhm_sc.png', dpi=1000)
