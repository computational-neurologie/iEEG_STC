#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  8 14:01:37 2021

Content
1. Functions to evaluate network adjecency matrices
2. Build weight/adjecency matrices
3. Run model simulations (Meisel 2020, Shew et al. 2009, Kinouchi et al. 2006

@author: Paul Müller
"""


from numba import jit, prange
import numpy as np
import networkx as nx


# 1. Convenience matrix evaluations
def get_largest_eigenvalue(w):
    """Calculate the largest absolute eigenvalue

    w      : Input weighted adjeceny matrix

   return : Largest absolute eigenvalue of w (float)
   """
    return np.max(np.abs(np.linalg.eigvals(w)))


def get_pos_sub_matrix(w):
    """Return the matrix with the connections among excitatory nodes only.

    w      : Input weighted adjeceny matrix

    return : Adjecency matrix amoung the excitatory nodes (np.ndarray)
    """
    inds = (w > 0).any(axis=0)
    return w[inds][:, inds]


# 2.  Weight initializations
@jit(nopython=True, cache=True)
def make_uniform_weights(n, alpha=.2, fi=1, fe=1):
    """
    Set up an Exhibitory/Inhibitory all to all connected network (no self links) with uniform weight distribution.
    Weights are set after the approximation on Larremore et al. to yield largest eigenvalue of approx. 1.

    n      : number of nodes
    alpha  : fraction of inhibitory nodes (default = 2)
    fi     : scale factor for inhibitory nodes (defalut = 1)
    fe     : scale factor excitatory nodes (default = 1)

    return : nxn adjecency matrix (np.ndarray)
    """
    weights = np.random.uniform(0, 1, size=(n, n))
    np.fill_diagonal(weights, 0)
    tmp = np.arange(0, n)
    np.random.shuffle(tmp)
    ind = tmp[:int(n*alpha)]
    exci_ind = tmp[int(n*alpha):]
    weights[:, ind] *= -1
    weights /= (weights.mean()*n)
    weights[:, ind] *= fi
    weights[:, exci_ind] *= fe
    return weights


def make_erdos_renyi_adjacency(n, plink, seed=None):
    """
    Get a directed Erdos-Renyi random adjecency matrix without self links (after networkx)

    n      : number of nodes
    plink  :  link probability

    return : nxn adjecency matrix (np.ndarray)

    Note the deletion of self links leads to an effective decrease in plink which is not accounted for.
    """
    n = int(n)
    adjecency = nx.to_numpy_array(nx.generators.fast_gnp_random_graph(n, plink, directed=True, seed=seed))
    np.fill_diagonal(adjecency, 0)
    return adjecency


def make_watts_strogatz_adjacency(n, plink, prewire=0.01, seed=None):
    """
    Get an approximated directed Watts-Strogatz small world adjecency matrix without self links.
    Two undirected Watts-Strogatz matrices are constructed and their upper and lower triangle are joined to get the
    directed matrix

    n       : number of nodes
    plink   : link probability -> Is transformed to the average degree k = n*plink
    prewire :  rewiring probability (default = 0.01)

    return  : nxn adjecency matrix (np.ndarray)
    """
    k = int(n*plink)
    n = int(n)
    adjecency = (np.triu(nx.to_numpy_array(nx.generators.watts_strogatz_graph(n, k, prewire, seed=seed)), 1) +
                 np.tril(nx.to_numpy_array(nx.generators.watts_strogatz_graph(n, k, prewire, seed=seed)), -1))
    return adjecency

#
# def make_distance_dependent_connectivity(distances, sigma=0.1):
#     """Make a connectivity matrix (ring distance) with distance dependent connectivity probability
#     See Yger et al. 2010"""
#     adjecency = np.zeros(shape=(n, n))
#     for i in range(n):
#         for j in range(n):
#             if i == j:
#                 continue
#             prob = np.exp(-np.min((np.abs(i-j), np.abs(np.abs(i-j)-n)))**2/sigma**2)
#             adjecency[i, j] = np.random.random() < prob
#     return adjecency


def make_2dgrid_distances(dim):
    """Make a nxn grid and return all euclidian distances (cyclic)"""
    n = dim*dim
    coords = np.array([(i, j) for i in range(dim) for j in range(dim)])
    distances = np.zeros(shape=(n, n))
    for i in range(n):
        for j in range(n):
            xi, yi = coords[i]
            xj, yj = coords[j]
            delta_x = np.min(((xi-xj)**2, (np.abs(xi-xj)-dim)**2))
            delta_y = np.min(((yi - yj) ** 2, (np.abs(yi - yj) - dim) ** 2))
            distances[i, j] = np.sqrt(delta_x + delta_y)
    return distances, coords


def make_grid_adjecency(dim=[10, 10], periodic=True):
    """Convience function to return a grid layout adjecency matrix"""
    return nx.to_numpy_array(nx.grid_graph(dim, periodic))


# 3. Models time series
@jit(nopython=True, cache=True, parallel=True)
def meisel2020_run_t_series(weights, neuron_v, max_t, rands):
    """
    Neuronal network simulation after Meisel 2020 (and previous publications)
    weights  : Adjecency matrix of the system
    neuron_v : Initial neuron vector
    max_t    : Maximal length of time series
    rands    : Random numbers for the calculation, should be of the shape (max_t, len(neuron_v))

    return   : time_series (np.ndarray, (max_t, len(neuron_v)) Activity of neurons
    """
    n = len(weights)
    time_series = np.full(shape=(max_t, n), fill_value=False)
    time_series[0] = neuron_v  # .astype(bool)
    t = 1
    while (t < max_t): #and time_series[t-1].any():
        time_series[t] = np.dot(weights, time_series[t-1].astype(np.float64)) > rands[t-1]
        t += 1
    return time_series


@jit(nopython=True, cache=True, parallel=True)
def meisel2020_run_t_series_with_background(weights, neuron_v, max_t, background, rands):
    """
    Neuronal network simulation after Meisel 2020 (and previous publications)
    weights  : Adjecency matrix of the system
    neuron_v : Initial neuron vector
    max_t    : Maximal length of time series
    rands    : Random numbers for the calculation, should be of the shape (max_t, len(neuron_v))

    return   : time_series (np.ndarray, (max_t, len(neuron_v)) Activity of neurons
    """
    n = len(weights)
    time_series = np.full(shape=(max_t, n), fill_value=False)
    time_series[0] = neuron_v  # .astype(bool)
    t = 1
    while (t < max_t):
        time_series[t] = (np.dot(weights, time_series[t-1].astype(np.float64)) > rands[t-1]) | background[t]
        t += 1
    return time_series


# @jit(nopython=True, cache=True, parallel=True)
def meisel2020_run_detailed_for_all_inits(weights, max_t, rands):
    """ Convenience fast function for meisel2020_run_t_series to simulate for all initial activity levels.
    Initial neuron vectors are initiated in order => Not suited for ordered networks.

    weights  : Adjecency matrix of the system
    max_t    : Maximal length of time series
    rands    : Random numbers for the calculation, should be of the shape (max_t, len(w))

    return   : time_series (np.ndarray, (len(w), max_t, len(w)) Activity of neurons

    Caution! this is high memory use as it needs to store all  neuron outcomes ->
    Memory aprrox: len(weights)^2 * max_t bytes"""
    n = len(weights)
    inits = np.full(shape=(n, n), fill_value=False)
    for i in prange(n):  # Start all possible initial activities parallel
        inits[i][:i+1] = True
    return meisel2020_run_detailed_for_specific_inits(weights, inits, max_t, rands)


@jit(nopython=True, cache=True, parallel=True)
def meisel2020_run_detailed_for_specific_inits(weights, inits, max_t, rands):
    """ Convenience fast function for meisel2020_run_t_series to simulate for all initial activity levels.
    Initial neuron vectors are initiated in order => Not suited for ordered networks.

    weights  : Adjecency matrix of the system
    inits    : matrix with initial conditions (must be given as bool). (int X len(weights))
    max_t    : Maximal length of time series
    rands    : Random numbers for the calculation, should be of the shape (max_t, len(w))

    return   : time_series (np.ndarray, (len(inits), max_t, len(w)) Activity of neurons

    Caution! this is high memory use as it needs to store all  neuron outcomes ->
    Memory aprrox: len(weights)*len(inits) * max_t bytes"""
    n = len(weights)
    out_put = np.full(shape=(len(inits), max_t, n), fill_value=False)
    out_put[:, 0, :] = inits
    for i in prange(len(inits)):  # Start all possible initial activities parallel
        t = 1
        while (t < max_t) and (out_put[i, t-1] != 0).any():
            out_put[i, t, :] = np.dot(weights, out_put[i, t-1].astype(np.float64)) > rands[t-1]
            t += 1
    return out_put


@jit(nopython=True, cache=True, parallel=True)
def meisel2020_run_for_all_inits(weights, max_t, rands):
    """
    Convenience fast function for meisel2020_run_t_series to simulate for all initial activity levels.
    Initial neuron vectors are initiated in order => Not suited for ordered networks.
    !!! Only gives the summed activity per time step and therefore does need less memory than the detailed version !!!

    weights  : Adjecency matrix of the system
    max_t    : Maximal length of time series
    rands    : Random numbers for the calculation, should be of the shape (max_t, len(w))

    return   : time_series (np.ndarray, (len(w), max_t) Summed activity of neurons
    """
    n = len(weights)
    out_put = np.zeros(shape=(n, max_t))
    neuron_v = np.zeros(shape=(n, n))
    for i in prange(n):
        t = 1
        neuron_v[i, :i+1] = 1
        out_put[i, 0] = i+1
        while (t < max_t) and (neuron_v != 0).any():
            neuron_v[i] = (np.dot(weights, neuron_v[i]) > rands[t-1]).astype(np.float64)
            out_put[i, t] = neuron_v[i].sum()
            t += 1
    return out_put


@jit(nopython=True)
def meisel2020_deterministic_run_t_series(weights, neuron_v, max_t):
    """
    Determinisitc neuronal network simulation after Meisel 2020 (and previous publications)
    weights  : Adjecency matrix of the system
    neuron_v : Initial neuron vector
    max_t    : Maximal length of time series

    return   : time_series (np.ndarray, (max_t, len(neuron_v)) Activity of neurons
     """
    n = len(neuron_v)
    neuron_v = neuron_v.astype(np.float64)
    time_series = np.zeros(shape=(max_t, n), dtype=np.float64)
    time_series[0] = neuron_v
    t = 1
    while (t < max_t) and (time_series[t - 1] != 0).any():
        neuron_v = np.dot(weights, neuron_v)
        neuron_v[neuron_v > 1] = 1
        neuron_v[neuron_v < 0] = 0
        time_series[t] = neuron_v
        t = t + 1

    return time_series


@jit(nopython=True, parallel=True)
def shew2009_run_t_series(weights, neuron_v, max_t, rands):
    """
    Neuronal network simulation after Shew 2009
    weights  : Adjecency matrix of the system
    neuron_v : Initial neuron vector
    max_t    : Maximal length of time series
    rands    : Random numbers for the calculation, should be of the shape (max_t, len(neuron_v))

    return   : time_series (np.ndarray, (max_t, len(neuron_v)) Activity of neurons
    """
    n = len(neuron_v)
    time_series = np.full(shape=(max_t, n), fill_value=False)
    time_series[0] = neuron_v.astype(bool)
    t = 1
    while t < max_t and time_series[t-1].any():
        neuron_v = np.full(n, False)  # Reset the neuron_v to no activity
        for i in prange(n):  # Loop over all neurons and calculate if they are active from the presynaptic activities
            neuron_v[i] = (1-np.prod(1-weights[i] * time_series[t-1].astype(np.float64))) > rands[i]
        time_series[t] = neuron_v
        t += 1
    return time_series


@jit(nopython=True)
def kinouchi2006_run_t_series(weights, neuron_v, max_t, refractory_steps=0, background_rate=0):
    """
    Neuronal network simulation after Kinouchi et al. 2006
    weights  : Adjecency matrix of the system
    neuron_v : Initial neuron vector
    max_t    : Maximal length of time series
    rands    : Random numbers for the calculation, should be of the shape (max_t, len(neuron_v))
    refractory_steps : Number of quiescent states per neuron after its activation
    background_rate  : Rate with which background noise is added

    return   : time_series (np.ndarray, (max_t, len(neuron_v)) Activity of neurons
    """
    transition_probabiltity = 1 - np.exp(-background_rate)  # Calculate the transition probabilty (Poisson process)
    time_series = np.full(shape=(max_t, len(neuron_v)), fill_value=False)
    time_series[0] = neuron_v.astype(bool)
    t = 1
    while t < max_t:
        excitable = (neuron_v == 0)  # Which neurons are excitable, i.e., were quiescent
        spikes = (neuron_v == 1).astype(np.float64)   # Which neurons have spiked
        #  Calculate which of the excitable are excitated
        neuron_v[excitable] = ((transition_probabiltity > np.random.uniform(0, 1, size=(excitable.sum())))
                               | (weights[excitable, :]@spikes > np.random.uniform(0, 1, size=(excitable.sum()))))
        neuron_v[~excitable] += 1  # Previously spiked neurons are cycled through the refractory states
        neuron_v[neuron_v == 2+refractory_steps] = 0  # After the refractory period neurons are reset to be excitable
        time_series[t] = (neuron_v == 1)
        t += 1
    return time_series


def main():
    print('You started models.py as the main.')
    # weights = make_uniform_weights(100)
    # weights /= get_largest_eigenvalue(weights)
    # inits = np.full(fill_value=False, shape=(5, 100))
    # for i in range(5):
    #     inits[i, :20*i] = True
    # out = meisel2020_run_detailed_for_specific_inits(weights, inits, 100, np.random.random(size=(100, 100)))
    # print(out)
    # out2 = meisel2020_run_detailed_for_all_inits(weights, 100, np.random.random(size=(100, 100)))
    # print(out2)
if __name__ == '__main__':
    main()
