#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 13:37:38 2021

@author: paulmueller
"""
import glob
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import wilcoxon
from iEEG_STC import utils
import os

sns.set_palette("tab10")
sns.set_style("whitegrid")
font = {'family': 'sans-serif',
        'sans-serif': 'Arial',
        'weight': 'normal',
        'size': 14}
plt.rc('font', **font)
plt.rc('xtick', labelsize=11) 
plt.rc('ytick', labelsize=11) 
plt.rc('text', usetex=True)
colors = sns.color_palette('tab10')


def AnnoMe(x1, x2, y, TXT, ax_object, dh=.1):
    """Make an annotation with a bracket in the data"""
    ax_object.plot([x1, x1, x2, x2], [y, y+dh, y+dh, y], lw=1, c='k')
    ax_object.text((x1+x2)*.5, y+dh, TXT, ha='center', va='bottom', color='k')


def make_sig_str(pval):
    """Make a string for significance levels (0.05,0.01,0.001) depending on the pval"""
    if pval > .05:
        return 'n.s.'
    elif pval < 0.001:
        return r'$p<0.001$'
    elif pval < 0.01:
        return r'$p<0.01$'
    else:
        return r'$p<0.05$'


PATH = '/Users/paulmueller/BIH_Neuro/LRTC_project'   # Path to data
states = ['SWS', 'nonSWS', 'low_AED', 'high_AED']  # States
measure = 'tau'
ind_plots = True  # Set to true to do plots for each patient

measures = pd.DataFrame()
surr_measures = pd.DataFrame()
# Prepare Frames for data
low_acf = pd.DataFrame(index=np.linspace(0, 120, 8*120, endpoint=False))
high_acf = pd.DataFrame(index=np.linspace(0, 120, 8*120, endpoint=False))
low_acf_surr = pd.DataFrame(index=np.linspace(0, 120, 8*120, endpoint=False))
high_acf_surr = pd.DataFrame(index=np.linspace(0, 120, 8*120, endpoint=False))


for path in glob.glob(f'{PATH}/pat*'):
    patID = path.split('/')[-1]
    print(patID)
    # Extract the averaged ACF
    acf = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                      '0.125_8_hanning_0.5_constant_256_60_True_056_096_averaged_acf_new_not_norm.pkl')) #,

    acf = acf.loc[:, pd.MultiIndex.from_product([['Total', ], states], names=['ROI', 'State'])]
    acf.columns = acf.columns.droplevel('ROI')
    acf.reset_index(inplace=True)
    # Get surr data
    surr_acf = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                           '0.125_8_hanning_0.5_constant_256_60_True_056_096_averaged_acf_new_not_norm_00surr.pkl'))

    surr_acf = surr_acf.loc[:, pd.MultiIndex.from_product([['Total', ], states], names=['ROI', 'State'])]
    surr_acf.columns = surr_acf.columns.droplevel('ROI')
    surr_acf.reset_index(inplace=True)
    # Get LRTC for data
    exp_coefs = utils.fit_exponential_and_give_decay_time(acf.set_index('Lag', drop=True).iloc[1:len(acf)//2])
    acw0 = utils.acw0(acf)
    ind, HM = utils.hwhm(acf)
    ind.name = 'TC'
    HM.name = 'HM'
    print((acf['low_AED'].iloc[:20] - acf['high_AED'].iloc[:20]).mean())
    AUC = acf.set_index('Lag', drop=True).loc[.125:10].mean()
    AUC.name = 'AUC'
    tmp = pd.concat([ind, HM, acw0, exp_coefs.loc['tau'], AUC, utils.lag_x(acf, x=1)], axis=1).T
    tmp.index = pd.MultiIndex.from_product([[patID], tmp.index])
    measures = pd.concat([measures, tmp])

    # Get LRTC for surr
    ind, HM = utils.hwhm(surr_acf)
    ind.name = 'TC'
    HM.ind = 'HM'
    acw0 = utils.acw0(surr_acf)
    AUC = surr_acf.set_index('Lag', drop=True).loc[.125:10].mean()
    AUC.name = 'AUC'
    exp_coefs_surr = utils.fit_exponential_and_give_decay_time(surr_acf.set_index('Lag', drop=True).iloc[1:len(acf) // 4])
    tmp = pd.concat([ind, HM, acw0, exp_coefs_surr.loc['tau'], AUC, utils.lag_x(surr_acf, x=1)], axis=1).T
    tmp.index = pd.MultiIndex.from_product([[patID], tmp.index])
    surr_measures = pd.concat([surr_measures, tmp])

    # Make seperate frames for the states
    low_acf[patID] = acf.loc[:, ['Lag', 'low_AED']].set_index('Lag', drop=True)
    high_acf[patID] = acf.loc[:, ['Lag', 'high_AED']].set_index('Lag', drop=True)
    low_acf_surr[patID] = surr_acf.loc[:, ['Lag', 'low_AED']].set_index('Lag', drop=True)
    high_acf_surr[patID] = surr_acf.loc[:, ['Lag', 'high_AED']].set_index('Lag', drop=True)
    # Plot ACF for each pat individually
    if ind_plots:
        fig, (ax1, ax2) = plt.subplots(nrows=2, figsize=(4.8, 3.2), sharex=True)
        ax1.plot(acf['Lag'][1:], acf['nonSWS'][1:], label='nonSWS', color=colors[0])
        ax1.axhline(HM['nonSWS'], linestyle='--', color=colors[0])
        ax1.axvline(ind['nonSWS'], linestyle='--', color=colors[0])
        ax1.plot(acf['Lag'][1:], acf['SWS'][1:], label='SWS', color=colors[1])
       # ax1.axhline(HM['SWS'], linestyle='--', color=colors[1])
        ax1.axvline(ind['SWS'], linestyle='--', color=colors[1])
        ax2.plot(acf['Lag'][1:], acf['low_AED'][1:], label='low AED', color=colors[0])
      #  ax2.axhline(HM['low_AED'], linestyle='--', color=colors[0])
        ax2.axvline(ind['low_AED'], linestyle='--', color=colors[0])
        ax2.plot(acf['Lag'][1:], utils.generalized_exponential(acf['Lag'][1:], exp_coefs.loc['tau', 'low_AED'],
                                                               exp_coefs.loc['a', 'low_AED'],
                                                               exp_coefs.loc['c', 'low_AED']), color='k')
        ax2.plot(acf['Lag'][1:], utils.generalized_exponential(acf['Lag'][1:], exp_coefs.loc['tau', 'high_AED'],
                                                               exp_coefs.loc['a', 'high_AED'],
                                                               exp_coefs.loc['c', 'high_AED']), color='green')
        ax2.plot(acf['Lag'][1:], acf['high_AED'][1:], label='high AED', color=colors[1])
    #    ax2.axhline(HM['high_AED'], linestyle='--', color=colors[1])
        ax2.axvline(ind['high_AED'], linestyle='--', color=colors[1])
        ax2.set_xlim(0, 60)
        ax1.legend()
        ax2.legend()
        fig.suptitle(patID)
        fig.supylabel('Autocorrelation')
        plt.tight_layout()
        plt.savefig(os.path.join(PATH, 'spec_figures',
                                 f'{patID}_average_acf_new_subsampling_not_norm.pdf'))
            #path, 'processed', f'{patID}_figures', 'average_acf_new_subsampling_not_norm.pdf'))


measures.columns = [c.replace('_', ' ') for c in measures.columns]
surr_measures.columns = [c.replace('_', ' ') for c in surr_measures.columns]
print(f"SWS vs. nonSWS {wilcoxon(measures.loc[(slice(None), measure), 'SWS'], measures.loc[(slice(None), measure), 'nonSWS'])}")
print(f"high vs. low AED {wilcoxon(measures.loc[(slice(None), measure), 'high AED'], measures.loc[(slice(None), measure), 'low AED'])}")

fig, (ax1, ax2) = plt.subplots(figsize=(4.8, 3.2), ncols=2, sharey=True)

width = .4
for pat in measures.index.levels[0]:
    
    ax1.plot([0, 1], measures.loc[(pat, measure), ['SWS', 'nonSWS']], color='k', linewidth=.5, alpha=.5, zorder=1)
    
    ax2.plot([0, 1], measures.loc[(pat, measure), ['high AED', 'low AED']], color='k', linewidth=.5, alpha=.5, zorder=1)

# Plot the sws and nonsws LRTC
bplot1 = sns.barplot(data=pd.melt(measures.loc[(slice(None), measure), ['SWS', 'nonSWS']]), x='variable', y='value', ax=ax1, palette=colors[2:4],
                     capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=colors[2:4], zorder=2)
# and its surr
# surr_bp1 = sns.barplot(data=pd.melt(surr_measures.loc[(slice(None), measure), ['SWS', 'nonSWS']]), x='variable', y='value', ax=ax1,
#                        palette=['gray', 'gray'], capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=['gray', 'gray'],
#                        zorder=2)
# And for AED data
bplot2 = sns.barplot(data=pd.melt(measures.loc[(slice(None), measure), ['high AED', 'low AED']]), x='variable', y='value', ax=ax2,
                     palette=colors[0:2], capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=colors[0:2], zorder=2)
# And AED surr
# surr_bp2 = sns.barplot(data=pd.melt(surr_measures.loc[(slice(None), measure), ['high AED', 'low AED']]), x='variable', y='value', ax=ax2,
#                        palette=['gray', 'gray'], capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=['gray', 'gray'],
#                        zorder=2)
# Adjust the width and positon of the bars
i = 0
for b1, b2 in zip(bplot1.patches, bplot2.patches):
    if i < 2:  # Non surrogates only the width
        centre = b1.get_x() + b1.get_width()/2.
        b1.set_x(centre-width/2)
        b1.set_width(width)
        centre = b2.get_x() + b2.get_width()/2.
        b2.set_x(centre-width/2)
        b2.set_width(width)
    else:  # Surrogate sligthly to the right
        pass
        centre = b1.get_x() + b1.get_width()/2+width/2
        b1.set_x(centre-width/2)
        b1.set_width(width)
        centre = b2.get_x() + b2.get_width()/2+width/2
        b2.set_x(centre-width/2)
        b2.set_width(width)
    i += 1
# Adjust the whiskers to conform to the bars
i = 0
for l1, l2 in zip(bplot1.lines, bplot2.lines):
    if i > 28:
        if l1._linewidth==1.2:
            l1.set_data(l1._x + width/2, l1._y)
            l2.set_data(l2._x + width/2, l2._y)
    i += 1
# Calculate the median of the LRTC
meds = measures.groupby(level=1).median()
# Plot the medians of the data
ax1.plot([-width/2, width/2], [meds.loc[measure, 'SWS'], meds.loc[measure, 'SWS']], color='k', linestyle='--', linewidth=2)
ax1.plot([1-width/2, 1+width/2], [meds.loc[measure, 'nonSWS'], meds.loc[measure, 'nonSWS']], color='k', linestyle='--', linewidth=2)

ax2.plot([-width/2, width/2], [meds.loc[measure, 'high AED'], meds.loc[measure, 'high AED']], color='k', linestyle='--', linewidth=2)
ax2.plot([1-width/2, 1+width/2], [meds.loc[measure, 'low AED'], meds.loc[measure, 'low AED']], color='k', linestyle='--', linewidth=2)
# Set labels
ax1.set_ylabel(measure)
ax1.set_xlabel(None)
ax2.set_ylabel(None)
ax2.set_xlabel(None)

# Draw the significance indicators
offset = 0
dh = offset/35
AnnoMe(0, 1, offset, make_sig_str(wilcoxon(measures.loc[(slice(None), measure), 'SWS'],
                                           measures.loc[(slice(None), measure), 'nonSWS'])[1]),
       ax1, dh=dh)

AnnoMe(0, 1, offset, make_sig_str(wilcoxon(measures.loc[(slice(None), measure), 'high AED'],
                                           measures.loc[(slice(None), measure), 'low AED'])[1]), ax2, dh=dh)
#
sns.despine(offset=10, trim=True, left=True, bottom=True)
# Set limits
ax1.set_xlim(-width*2/3, 1+width*2/3+width/2)
ax2.set_xlim(-width*2/3, 1+width*2/3+width/2)
# ax1.set_ylim(-dh/7, offset+dh)
# Save
plt.tight_layout()
plt.show()
# plt.savefig(os.path.join(PATH, 'figures', f'new_{measure}_AED_VI_new_subsampling.pdf'))

# Plot the complete averaged autocorrelation funciton
fig, ax = plt.subplots(figsize=(4.8, 3.2))
# Make names of columns nicer and avoid LaTeX bug
low_acf.columns = [c.replace('_', ' ') for c in low_acf.columns]
high_acf.columns = [c.replace('_', ' ') for c in high_acf.columns]
low_acf_surr.columns = [c.replace('_', ' ') for c in low_acf_surr.columns]
high_acf_surr.columns = [c.replace('_', ' ') for c in high_acf_surr.columns]
# Set the length of the acf to show (8* 20) = (samples_per_sec*limit_in_seconds)
lim_in_sec = 20
low_acf = low_acf.iloc[1:8*lim_in_sec]
high_acf = high_acf.iloc[1:8*lim_in_sec]
low_acf_surr = low_acf_surr.iloc[1:8*lim_in_sec]
high_acf_surr = high_acf_surr.iloc[1:8*lim_in_sec]
# Plot with ci (default is bootstraping)
sns.lineplot(data=pd.melt(low_acf, ignore_index=False).reset_index(), x='index', y='value', ax=ax, label='low AED',
             color=colors[1])  # ,ci='sd')
sns.lineplot(data=pd.melt(high_acf, ignore_index=False).reset_index(), x='index', y='value', ax=ax, label='high AED',
             color=colors[0])  # ,ci='sd')
sns.lineplot(data=pd.melt(low_acf_surr, ignore_index=False).reset_index(), x='index', y='value', ax=ax,
             label='surrogate/low AED', color=colors[1], linestyle=':', alpha=.8)
sns.lineplot(data=pd.melt(high_acf_surr, ignore_index=False).reset_index(), x='index', y='value', ax=ax,
             label='surrogate/high AED', color=colors[0], linestyle=':', alpha=.8)
ax.set_ylabel('Autocorrelation')
ax.set_xlabel('Lag (sec.)')
# Set limit
ax.set_xlim(0, lim_in_sec)
# Make legend
ax.legend(borderaxespad=0)
# Save
plt.tight_layout()
plt.savefig(os.path.join(PATH, 'figures', 'new_average_acf_new_subsampling_not_norm.pdf'))
