import glob
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import spearmanr, linregress, wilcoxon, combine_pvalues
from matplotlib.patches import Rectangle

from iEEG_STC import utils
import os

sns.set_palette("tab10")
sns.set_style("whitegrid")
font = {'family': 'sans-serif',
        'sans-serif': 'Arial',
        'weight': 'normal',
        'size': 11}
plt.rc('font', **font)
plt.rc('xtick', labelsize=11)
plt.rc('ytick', labelsize=11)
plt.rc('text', usetex=True)


def AnnoMe(x1, x2, y, TXT, ax_object, dh=.1):
    """Make an annotation with a bracket in the data"""
    ax_object.plot([x1, x1, x2, x2], [y, y + dh, y + dh, y], lw=1, c='k')
    ax_object.text((x1 + x2) * .5, y + dh, TXT, ha='center', va='bottom', color='k')


def make_sig_str(pval):
    """Make a string for significance levels (0.05,0.01,0.001) depending on the pval"""
    if pval > .05:
        return 'n.s.'
    elif pval < 0.001:
        return r'$p<0.001$'
    elif pval < 0.01:
        return r'$p<0.01$'
    else:
        return r'$p<0.05$'


# Define the colors to plot the different rois
colors = np.array([(0.8392156862745098, 0.15294117647058825, 0.1568627450980392),
                   (1.0, 0.4980392156862745, 0.054901960784313725),
                   (0.17254901960784313, 0.6274509803921569, 0.17254901960784313),
                   (0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
                   (0.5803921568627451, 0.403921568627451, 0.7411764705882353)])

PATH = '/Users/paulmueller/BIH_Neuro/LRTC_project'   # Path to data
bands = ['056_096'] #,'004_008', '008_012', '012_028', '032_044',  '004_096']
states = ['nonSWS_low_AED'] #'Total', 'SWS', 'nonSWS', 'low_AED', 'high_AED',
          #'SWS_low_AED', 'SWS_high_AED', 'nonSWS_low_AED', 'nonSWS_high_AED']
# combinations_to_allow = [('low_AED', 'high_AED'), ('SWS_low_AED', 'SWS_high_AED'),
#                          ('nonSWS_low_AED', 'nonSWS_high_AED'),
#                          ('SWS', 'nonSWS'), ('SWS_low_AED', 'nonSWS_low_AED'),
#                          ('SWS_high_AED', 'nonSWS_high_AED')]
exp_fit_len = 60
all_p_values = pd.DataFrame()
PATH = '/Users/paulmueller/BIH_Neuro/LRTC_project'  # Where to find the data
measure = 'TC (sec.)'
width = 0.5
ind_plots = False  # Set to true to do plots for each patient
rois = ['MT', 'LIP', 'LPFC', 'OFC', 'ACC']  # Which rois are you interested
lrtc = pd.DataFrame(columns=pd.Index(rois, name='ROI'))  # DataFrame for the lrtc for all regions of interest
surr_lrtc = pd.DataFrame(columns=pd.Index(rois, name='ROI'))  # DataFrame for the surr -""-
k = 0
max_k = len(bands)*len(states)
handles = [Rectangle((0, 0), 1, 1, fill=False, edgecolor='none', visible=False),
           Rectangle((0, 0), 1, 1, fill=False, edgecolor='none', visible=False)]
for band in bands:
    fig1 = plt.figure(figsize=(4.8, 3.2))
    # Make the two subplots for number of pats with roi and LRTC per roi
    ax1 = []
    ax1.append(plt.subplot2grid((4, 1), (0, 0), rowspan=1))
    ax1.append(plt.subplot2grid((4, 1), (1, 0), rowspan=3, sharex=ax1[0]))
    #fig1, ax1 = plt.subplots(figsize=(4.8, 1.8*(len(states)+1)), nrows=len(states)+1, sharex=True)
    pvals = []
    offset = 0
    for i, state_0 in enumerate(states):
        k += 1
        print(f'{k}/{max_k} : {band}: {state_0}')

        file = f'0.125_8_hanning_0.5_constant_256_60_True_{band}_averaged_acf_noIED_nosubs.pkl'

        lim_in_sec = 40



        measures = pd.DataFrame()
        surr_measures = pd.DataFrame()
        for path in glob.glob(f'{PATH}/pat*'):
            patID = path.split('/')[-1]

            acf = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                              file))
            acf.columns = pd.MultiIndex.from_tuples(acf.columns, names=['ROI', 'State'])
            acf = acf.loc[:, (rois, state_0)]
            acf = acf.reset_index()
            acf.columns = acf.columns.droplevel('State')

            # Extract the averaged surrogate ACF
            surr_acf = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                                   file.replace('.pkl', '_surr01.pkl')))
            surr_acf.columns = pd.MultiIndex.from_tuples(surr_acf.columns, names=['ROI', 'State'])
            surr_acf = surr_acf.loc[:, (rois, state_0)]
            surr_acf = surr_acf.reset_index()
            surr_acf.columns = surr_acf.columns.droplevel('State')


            acw0 = utils.acw0(surr_acf)
            ind, HM = utils.hwhm(surr_acf)
            ind.name = 'TC (sec.)'
            HM.name = 'HM'
            ind2, HM2 = utils.hwhm(surr_acf, hmfunc=lambda x: utils.hm_from_lag_x_med_8_4(x, x=1))
            ind2.name = 'TC2'
            HM2.name = 'HM2'
            indto0, hmto0 = utils.hwhm(surr_acf, hmfunc=lambda x: pd.Series(utils.lag_x(x, 1) / 2, name='Lag1Half'))
            indto0.name = 'TCto0'
            AUC = surr_acf.set_index('Lag', drop=True).loc[.125:10].mean()
            AUC.name = 'AUC'
            tmp = pd.concat([ind, HM, ind2, HM2, indto0, hmto0, acw0, AUC,
                             utils.lag_x(surr_acf, x=1)], axis=1).T
            tmp.index = pd.MultiIndex.from_product([[patID], tmp.index])
            surr_measures = pd.concat([surr_measures, tmp])

            # Get LRTC for data

            acw0 = utils.acw0(acf)
            ind, HM = utils.hwhm(acf)
            ind.name = 'TC (sec.)'
            HM.name = 'HM'
            ind2, HM2 = utils.hwhm(acf, hmfunc=lambda x: utils.hm_from_lag_x_med_8_4(x, x=1))
            ind2.name = 'TC2'
            HM2.name = 'HM2'
            indto0, hmto0 = utils.hwhm(acf, hmfunc=lambda x: pd.Series(utils.lag_x(x, 1) / 2, name='Lag1Half'))
            indto0.name = 'TCto0'
            AUC = acf.set_index('Lag', drop=True).loc[.125:10].mean()
            AUC.name = 'AUC'
            tmp = pd.concat([ind, HM, ind2, HM2, acw0, indto0, hmto0, AUC,
                             utils.lag_x(acf, x=1)], axis=1).T
            tmp.index = pd.MultiIndex.from_product([[patID], tmp.index])
            measures = pd.concat([measures, tmp])
        if i == 0:
            # Plot number of pat with ROI
            bplot2 = sns.barplot(x=measures.columns, y=measures.loc[(slice(None), measure), :].notnull().sum(), ax=ax1[0], palette=colors,
                                 capsize=width / 2, errwidth=1.2, alpha=.7, edgecolor=colors, zorder=2, ci=None)
            ax1[0].set_ylabel('\#Patients')
            ax1[0].set_xlabel(None)
            plt.setp(ax1[0].get_xticklabels(), visible=False)
            # Go through the number plot and adjust the width of the bars as well
            for b2 in bplot2.patches:
                centre = b2.get_x() + b2.get_width() / 2.
                b2.set_x(centre - width / 2)
                b2.set_width(width)
        bplot1 = sns.barplot(data=pd.melt(measures.loc[(slice(None), measure), :]), x='ROI', y='value', ax=ax1[i+1],
                             palette=colors,
                             capsize=width / 2, errwidth=1.2, alpha=.7, edgecolor=colors, zorder=2)
        # Plot surr LRTC per ROI
        surrbp1 = sns.barplot(data=pd.melt(surr_measures.loc[(slice(None), measure), :]), x='ROI', y='value', ax=ax1[i+1],
                              palette=np.full(5, 'gray'),
                              capsize=width / 2, errwidth=1.2, alpha=.7, edgecolor=np.full(5, 'gray'), zorder=2)
        ax1[i+1].set_ylabel(measure+' \n' + f"({state_0.replace('_', ' ')})")
        ax1[i+1].set_xlabel(None)
        # Go through the LRTC plot and adjust there width and position
        for j, b1 in enumerate(bplot1.patches):
            if j < 5:  # Keep lrtc in place
                centre = b1.get_x() + b1.get_width() / 2.
                b1.set_x(centre - width / 2)
                b1.set_width(width)
            else:  # Move the surrogate slightly to the right
                centre = b1.get_x() + b1.get_width() / 2. + width / 2
                b1.set_x(centre - width / 2)
                b1.set_width(width)


        # Go through all whiskers and move them accordingly
        for j, l1 in enumerate(bplot1.lines):
            if j > 14:
                if l1._linewidth == 1.2:
                    l1.set_data(l1._x + width / 2, l1._y)
                    pass

        # Plot the individual lines connecting LRTC of single patients
        for pat in measures.index.levels[0]:
            ax1[i+1].plot(np.arange(0, 5)[measures.loc[(pat, measure), :].notnull()],
                          measures.loc[pat].loc[measure, measures.loc[pat].loc[measure].notnull()].squeeze(),
                          color='k', linewidth=.5, alpha=.5, zorder=1)
        meds = measures.loc[(slice(None), measure), :].median()
        # Plot the median data (only for real data)
        for j, c in enumerate(meds.index):
            ax1[i+1].plot([j - width / 2, j + width / 2], [meds[c], meds[c]], color='k', linestyle='--', linewidth=2)
        slopes = []
        spears = []
        rhos = []
        pvals = []
        # Cycle through all patients
        for j in measures.index.levels[0]:
            tmp = measures.loc[(j, measure), :]
            tmp.index = [rois.index(j) for j in tmp.index]
            if np.sum(~tmp.isnull()) >= 2:  # only regress if the at least two non nan values are found
                tmp = tmp.loc[~tmp.isnull()]
                # Linearily regress
                reg_params = linregress(tmp.index, tmp.values)
                slopes.append(reg_params[0])
                # and spearmanrs and pvals
                r, p = spearmanr(tmp.index, tmp.values)
                # Skipp if the spearman is not defined
                if np.isnan(r):
                    continue
                spears.append(p)
                rhos.append(r)
                if r <= 0:
                    pvals.append(1)
                else:
                    pvals.append(p)

        # Show all slopes
        print('Slopes')
        print(slopes)
        print(np.mean(slopes), np.std(slopes) / np.sqrt(len(slopes)))
        print(f'Different from zero {wilcoxon(slopes)}')
        # Show the Spearmanrs
        print('Spearmanr rhos')
        print(rhos)
        print(np.mean(rhos), np.std(rhos) / np.sqrt(len(rhos)))
        print(f'Different from zero {wilcoxon(rhos)}')
        print(f'combined spearmanr pvalues {combine_pvalues(pvals)}')
        print('Without ACC')
        slopes = []
        spears = []
        rhos = []
        # Cycle through all patients
        for j in measures.index.levels[0]:
            tmp = measures.loc[(j, measure), ['MT', 'LIP', 'LPFC', 'OFC']]
            tmp.index = [rois.index(j) for j in tmp.index]
            if np.sum(~tmp.isnull()) >= 2:  # only regress if the at least two non nan values are found
                tmp = tmp.loc[~tmp.isnull()]
                # Linearily regress
                reg_params = linregress(tmp.index, tmp.values)
                slopes.append(reg_params[0])
                # and spearmanrs and pvals
                r, p = spearmanr(tmp.index, tmp.values)
                # Skipp if the spearman is not defined
                if np.isnan(r):
                    continue
                spears.append(p)
                rhos.append(r)
        # Show all slopes
        print('Slopes')
        print(slopes)
        print(np.mean(slopes), np.std(slopes) / np.sqrt(len(slopes)))
        print(f'Different from zero {wilcoxon(slopes)}')
        # Show the Spearmanrs
        print('Spearmanr rhos')
        print(rhos)
        print(np.mean(rhos), np.std(rhos) / np.sqrt(len(rhos)))
        print(f'Different from zero {wilcoxon(rhos)}')
        # ax1[i+1].legend(handles, [f"Slopes {make_sig_str(wilcoxon(slopes)[1])}",
        #                           f"Rhos {make_sig_str(wilcoxon(rhos)[1])}"], loc=2)

    sns.despine(offset=10, trim=True, left=True, bottom=True)
    ax1[-1].set_xlim(-width * 2 / 3, 4 + width * 2 / 3 + width / 2)
   # fig1.suptitle(measure)
    fig1.tight_layout()
    fig1.savefig(os.path.join(PATH, 'figures', measure, f'{band}_{measure}_noIED_nosubs_ROI.pdf'))
