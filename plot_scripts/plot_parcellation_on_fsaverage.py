# -*- coding: utf-8 -*-


import mne
import pandas as pd
import numpy as np
from  scipy.spatial.distance import cdist 
import glob
import seaborn as sns
Brain = mne.viz.get_brain_class()

subjects_dir = str(mne.datasets.sample.data_path()) + '/subjects'
mne.datasets.fetch_hcp_mmp_parcellation(subjects_dir=subjects_dir,
                                        verbose=True)

mne.datasets.fetch_aparc_sub_parcellation(subjects_dir=subjects_dir,
                                          verbose=True)

labels = mne.read_labels_from_annot(
    'fsaverage', 'HCPMMP1', 'lh', subjects_dir=subjects_dir)
MT = mne.Label(hemi='lh',subject='fsaverage')
LIP =  mne.Label(hemi='lh',subject='fsaverage')
LPFC =  mne.Label(hemi='lh',subject='fsaverage')
OFC =  mne.Label(hemi='lh',subject='fsaverage')
ACC  =  mne.Label(hemi='lh',subject='fsaverage')
counter = np.zeros(5)
for L in labels:
    # 
    if L.name in ['L_MST_ROI-lh','L_MT_ROI-lh', # Here come the differences
                   'L_V3CD_ROI-lh', 'L_LO1_ROI-lh', 'L_LO2_ROI-lh', 'L_LO3_ROI-lh',  'L_V4t_ROI-lh', 'L_FST_ROI-lh', 'L_PH_ROI-lh'
                  
                  ]:
        print(L.name,'MT')
        MT +=L
        counter[0] +=1
        
    elif L.name in ['L_VIP_ROI-lh','L_MIP_ROI-lh','L_AIP_ROI-lh','L_LIPd_ROI-lh']:
        print(L.name,'LIP')
        LIP += L
        counter[1] +=1
    elif L.name in ['L_8Ad_ROI-lh','L_8Av_ROI-lh','L_9p_ROI-lh','L_8C_ROI-lh','L_p9-46v_ROI-lh','L_46_ROI-lh','L_a9-46v_ROI-lh',
                    'L_i6-8_ROI-lh','L_s6-8_ROI-lh','L_8BL_ROI-lh','L_SFL_ROI-lh',# Here come the differences
                    'L_9a_ROI-lh','L_9-46d_ROI-lh',
                    #Next are the old includes
                    'L_44_ROI-lh','L_45_ROI-lh', ## And some more corresponding to
                     'L_IFJp_ROI-lh', 'L_IFJa_ROI-lh', 'L_IFSp_ROI-lh', 'L_IFSa_ROI-lh', 'L_47l_ROI-lh',  'L_p47r_ROI-lh'
                    ]:
        print(L.name,'LPFC')
        LPFC +=L
        counter[2] +=1
    elif L.name in ['L_OFC_ROI-lh','L_pOFC_ROI-lh', # Here come the differencs (See Kringelback 2005)
                    'L_47s_ROI-lh', 'L_47m_ROI-lh', 'L_11l_ROI-lh', 'L_13l_ROI-lh', 'L_a10p_ROI-lh', 'L_p10p_ROI-lh', 'L_10pp_ROI-lh', 'L_10d_ROI-lh', 'L_a47r_ROI-lh'
                    ]:
        print(L.name,'OFC')
        OFC +=L
        counter[3] +=1
    elif L.name in ['L_33pr_ROI-lh','L_p24pr_ROI-lh','L_a24pr_ROI-lh','L_a24_ROI-lh','L_d32_ROI-lh','L_p24_ROI-lh',
                    'L_a32pr_ROI-lh','L_s32_ROI-lh','L_p32pr_ROI-lh']:
        print(L.name,'ACC')
        ACC +=L
        counter[4] +=1


MT.name = 'MT'
MT.color= (0.8392156862745098, 0.15294117647058825, 0.1568627450980392)
LIP.name = 'LIP'
LIP.color = (1.0, 0.4980392156862745, 0.054901960784313725)
LPFC.name  = 'LPFC'
LPFC.color = (0.17254901960784313, 0.6274509803921569, 0.17254901960784313)
OFC.name = 'OFC'
OFC.color = (0.12156862745098039, 0.4666666666666667, 0.7058823529411765)
ACC.name = 'ACC'
ACC.color = (0.5803921568627451, 0.403921568627451, 0.7411764705882353)

brain = Brain('fsaverage', 'lh', 'pial', subjects_dir=subjects_dir,
              cortex='low_contrast', background='white', size=(800, 600))
brain.add_label(MT, borders=False)
brain.add_label(LIP, borders=False)
brain.add_label(LPFC, borders=False)
brain.add_label(OFC, borders=False)
brain.add_label(ACC, borders=False)
# brain.add_annotation('HCPMMP1',color=(0,0,0),borders=1)

colors = sns.color_palette("viridis", as_cmap=True)
# print(colors)
#### Assess electrode data
vertices_lh = mne.vertex_to_mni(range(163842),hemis=0,subject='fsaverage')
i = 0
for pat in glob.glob('/Users/paulmueller/BIH_Neuro/LRTC_project/pat*'):
    pat = pat.split('/')[-1]
    print(pat)
    if pat != 'pat958':
        continue
    # if pat in  ['pat620','pat583','pat384','pat1150','pat1125','pat818','pat635','pat139','pat115','pat548']:
    #     continue
    # elecs = pd.read_csv(f'/Users/paulmueller/BIH_Neuro/LRTC_project/{pat}/meta/{pat}_on_pat958_likely_zone_b.csv')
    elecs = pd.read_csv(f'/Users/paulmueller/BIH_Neuro/LRTC_project/{pat}/meta/{pat}_likely_zone_b.csv')
    # print(elecs)
    elecs =[np.array([row['x'],row['y'],row['z']]) for i,row in elecs.iterrows() if (row['x']<0) and (row['kind']!='depth')]# if row['dist_to_zone']<10]
    if len(elecs) == 0:
        print('only on the other side')
        continue
    # elecs =np.array(elecs)
    # brain.add_foci(elecs,scale_factor=.4,color=colors(i/17),hemi='lh')
    
    closest = np.array([np.argmin(cdist(vertices_lh,[E]),axis=0)[0] for E in elecs ])
   
    brain.add_foci(closest,coords_as_verts=True,scale_factor=.4,hemi='lh',color='k')#colors(i/17))
    i+=1


mne.viz.set_3d_view(brain,azimuth = 180,elevation = 80)
brain.save_image('/Users/paulmueller/BIH_Neuro/LRTC_project/figures/cortical_parcelation_fsaverage_b_pat958.png')
