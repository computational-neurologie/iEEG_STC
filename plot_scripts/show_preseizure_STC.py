import glob
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import datetime
from scipy.stats import zscore
from scipy.stats import spearmanr, mannwhitneyu, linregress, wilcoxon

from iEEG_STC import utils
import os

colors = sns.color_palette("tab10")
sns.set_style("whitegrid")
font = {'family': 'sans-serif',
        'sans-serif': 'Arial',
        'weight': 'normal',
        'size': 11}
plt.rc('font', **font)
plt.rc('xtick', labelsize=11)
plt.rc('ytick', labelsize=11)
plt.rc('text', usetex=True)
AED_state = ''
vig_state = ''
delta_t_between_seizures = 3600*2
patients_without_seizure_segments = ['pat620', 'pat862', 'pat253']
all_tcs, origin_tcs = pd.DataFrame(), pd.DataFrame()
all_scs = pd.DataFrame()
for patient in glob.glob(f'/Users/paulmueller/BIH_Neuro/LRTC_project/pat*'):
    patient = patient.split('/')[-1]
    if patient in patients_without_seizure_segments:
        print(f'{patient} defined as without seizure segments')
        continue
    print(f'Starting {patient}')
    #patient = 'pat922'
    path = f'/Users/paulmueller/BIH_Neuro/LRTC_project/{patient}'
    classifier = pd.read_csv(f'{path}/meta/{patient}_classifier_with_seizures.csv', index_col=0)

    acf = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                      '0.125_8_hanning_0.5_constant_256_60_True_pre_seizure_acf.pkl'))
    cc = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                     '0.125_8_hanning_0.5_constant_256_60_True_pre_seizure_cc.pkl'))
    cc.index = cc.index.droplevel('Band')
    acf.index = acf.index.droplevel('Band')
    acf = acf.sort_index(level=0)
    cc = cc.sort_index(level=0)
    tmp_classifier = classifier.loc[acf.index.levels[0]]
    tmp_classifier2 = classifier.loc[cc.index.levels[0]]
    # Change to AED state
    if AED_state == 'low_AED':
        acf = acf.loc[tmp_classifier[tmp_classifier['low_AED'] == True].index]
        cc = cc.loc[tmp_classifier2[tmp_classifier2['low_AED'] == True].index]
        tmp_classifier = tmp_classifier.loc[tmp_classifier[tmp_classifier['low_AED'] == True].index]
        tmp_classifier2 = tmp_classifier2.loc[tmp_classifier2[tmp_classifier2['low_AED'] == True].index]
    elif AED_state == 'high_AED':
        acf = acf.loc[tmp_classifier[tmp_classifier['low_AED'] == False].index]
        cc = cc.loc[tmp_classifier2[tmp_classifier2['low_AED'] == False].index]
        tmp_classifier = tmp_classifier.loc[tmp_classifier[tmp_classifier['low_AED'] == False].index]
        tmp_classifier2 = tmp_classifier2.loc[tmp_classifier2[tmp_classifier2['low_AED'] == True].index]
    # Change to Vigilance state
    if vig_state == 'SWS':
        acf = acf.loc[tmp_classifier[tmp_classifier['sws'] == True].index]
        cc = cc.loc[tmp_classifier2[tmp_classifier2['sws'] == True].index]
    elif vig_state == 'nonSWS':
        acf = acf.loc[tmp_classifier[tmp_classifier['sws'] == False].index]
        cc = cc.loc[tmp_classifier2[tmp_classifier2['sws'] == False].index]
    if len(tmp_classifier) != len(tmp_classifier2):
        print(f'Warning {patient} has different acf and cc windows')


    seizures = pd.read_csv(f'{path}/meta/{patient}_seizures.csv')
    seizures = seizures.sort_values('onset_ts')
    seizures = seizures.reset_index(drop=True)
    seizures.loc[pd.isnull(seizures['origin']), 'origin'] = 'Unknown'

    acf_aligned_by_seizures = pd.DataFrame()
    cc_aligned_by_seizures = pd.DataFrame()
    fig, (ax0, ax1, ax2) = plt.subplots(nrows=3,  figsize=(4.8, 4.8), sharex=True)

    i = 0

    for sei in seizures.index:
        onset = seizures.loc[sei, 'onset_ts']
        print(f'Onset: {datetime.datetime.fromtimestamp(onset)} ->', end='')
        try:
            if onset - seizures.loc[sei-1, 'onset_ts'] < delta_t_between_seizures:

                print('Seizure is preceeded by another seizure. ' +
                      str(datetime.datetime.fromtimestamp(onset)) + ', ' +
                      str(datetime.datetime.fromtimestamp(seizures.loc[sei-1, 'onset_ts'])))
                continue
        except KeyError:
            print(' First seizure')

        # Check if there is a seizure some
        origin = np.array(seizures.loc[sei, 'origin'].split(','))

        seizures_acf = acf.loc[np.logical_and(acf.index.get_level_values(0) <= onset,
                                              acf.index.get_level_values(0) >= onset-delta_t_between_seizures)]
        seizures_cc = cc.loc[np.logical_and(cc.index.get_level_values(0) <= onset,
                                              cc.index.get_level_values(0) >= onset - delta_t_between_seizures)]
        if len(seizures_acf) == 0:
            print(f'No seizure acf found for {datetime.datetime.fromtimestamp(onset)}')
            continue
        elif len(seizures_acf.index.get_level_values(0).unique()) < 20:
            print(
                f'Should: {datetime.datetime.fromtimestamp(onset - delta_t_between_seizures)}-{datetime.datetime.fromtimestamp(onset)}')
            print(
                f'Is    : {datetime.datetime.fromtimestamp(np.min(seizures_acf.index.get_level_values(0)))}-{datetime.datetime.fromtimestamp(np.max(seizures_acf.index.get_level_values(0)))}')
            print(f'Warning for seizure {datetime.datetime.fromtimestamp(onset)} only a few data points exist.')
            continue
        else:

            print(
                f'Should: {datetime.datetime.fromtimestamp(onset - delta_t_between_seizures)}-{datetime.datetime.fromtimestamp(onset)}')
            print(
                f'Is    : {datetime.datetime.fromtimestamp(np.min(seizures_acf.index.get_level_values(0)))}-{datetime.datetime.fromtimestamp(np.max(seizures_acf.index.get_level_values(0)))}')
            # Make timepoints relative to seizure start with rounding to 120 seconds for later alignment
            origin = origin[np.in1d(origin, seizures_acf.columns)]
            seizures_acf = seizures_acf.reset_index(level=0)
            seizures_cc = seizures_cc.reset_index(level=0)
            seizures_acf['Time'] = np.round((seizures_acf['Time'] - onset) / 120) * 120
            seizures_cc['Time'] = np.round((seizures_cc['Time']-onset)/120)*120
            seizures_acf.index = pd.MultiIndex.from_arrays([seizures_acf['Time'].values, seizures_acf.index],
                                                           names=['Time', 'Lag'])
            seizures_cc.index = pd.MultiIndex.from_arrays([seizures_cc['Time'].values, seizures_cc.index],
                                                          names=['Time', 'Distance'])
            seizures_acf = seizures_acf.sort_index(level=0)
            seizures_cc = seizures_cc.sort_index(level=0)
            seizures_acf = seizures_acf.drop('Time', axis=1)
            seizures_cc = seizures_cc.drop('Time', axis=1)

            tmp = pd.DataFrame(columns=['All', 'Origin'])
            tmp['All'] = seizures_acf.mean(axis=1)
            if len(origin) != 0:
                tmp['Origin'] = seizures_acf.loc[:, origin].mean(axis=1)
            tmp.columns = pd.MultiIndex.from_product([[onset], tmp.columns], names=['Onset', 'Channels'])
            acf_aligned_by_seizures = pd.concat([acf_aligned_by_seizures, tmp], axis=1)
            seizures_cc.columns = [onset]
            cc_aligned_by_seizures = pd.concat([cc_aligned_by_seizures, seizures_cc], axis=1)
            print(f'Seizure {datetime.datetime.fromtimestamp(onset)} detected.')

        tcs = pd.DataFrame(dtype=float)
        scs = pd.Series(dtype=float, name='All')
        if len(origin) == 0:
            print(f'There is no good data for the origin electrodes at {datetime.datetime.fromtimestamp(onset)}')
        else:
            for t in sorted(seizures_acf.index.get_level_values(0).unique()):
                ind, HM = utils.hwhm(seizures_acf.loc[t, origin].mean(axis=1).to_frame())
                tcs.loc[t, 'Origin'] = ind.squeeze()
        for t in sorted(seizures_acf.index.get_level_values(0).unique()):
            ind, HM = utils.hwhm(seizures_acf.loc[t].mean(axis=1).to_frame())
            tcs.loc[t, 'All'] = ind.squeeze()
        for t in sorted(seizures_cc.index.get_level_values(0).unique()):
            scs.loc[t] = seizures_cc.loc[(t, slice(7, 79)), :].mean().squeeze()
        tcs.index /= 60
        scs.index /= 60
        ax0.plot(tcs.index, tcs['All'], color=colors[i], label=datetime.datetime.fromtimestamp(onset))
        if len(origin) != 0:

            ax1.plot(tcs.index, tcs['Origin'], color=colors[i])
        ax2.plot(scs.index, scs.values, color=colors[i])
        i += 1

    tcs = pd.DataFrame(columns=['All', 'Origin'])
    scs = pd.Series(name='All', dtype=float)
    if len(acf_aligned_by_seizures) == 0:
        continue
    for t in acf_aligned_by_seizures.index.levels[0]:
        ind, HM = utils.hwhm(acf_aligned_by_seizures.loc[t, (slice(None), 'All')].mean(axis=1).to_frame())
        tcs.loc[t, 'All'] = ind.squeeze()
        ind, HM = utils.hwhm(acf_aligned_by_seizures.loc[t, (slice(None), 'Origin')].mean(axis=1).to_frame())
        tcs.loc[t, 'Origin'] = ind.squeeze()
        scs.loc[t] = cc_aligned_by_seizures.loc[t].sort_index().loc[7:79].mean().mean()
    tcs.index /= 60
    scs.index /= 60
    all_tcs = pd.concat([all_tcs, pd.Series(tcs['All'].values, index=tcs['All'].index,
                                            name=patient).to_frame().T])
    origin_tcs = pd.concat([origin_tcs, pd.Series(tcs['Origin'].values, index=tcs['All'].index,
                                                  name=patient).to_frame().T])
    scs.name = patient
    all_scs = pd.concat([all_scs, scs.to_frame().T])
    ax0.plot(tcs.index,  tcs['All'], color='k', label='Mean')
    ax1.plot(tcs.index,  tcs['Origin'], color='k', label='Mean')
    ax2.plot(scs.index, scs.values, color='k', label='Mean')
    ax1.set_xlim(-120, -2)
    ax1.set_xlabel('Time pre Seizure (min.)')
    ax0.set_ylabel('All')
    ax1.set_ylabel('Origin')
    ax2.set_ylabel('SC')
    fig.suptitle(patient)
    ax0.legend(fontsize='xx-small', loc='center left', bbox_to_anchor=(1, 0.5))
    plt.tight_layout()
    plt.savefig(f'{path}/processed/{patient}_figures/preseizure_TC{AED_state}{vig_state}.pdf')
    plt.close(fig)
fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, figsize=(4.8, 4.8), sharex=True)
all_tcs = all_tcs.astype(float)
all_scs = all_scs.astype(float)
for row in all_tcs.index:
    all_tcs.loc[row, ~pd.isnull(all_tcs.loc[row])] = zscore(all_tcs.loc[row, ~pd.isnull(all_tcs.loc[row])])
all_melted = pd.melt(all_tcs, ignore_index=False, var_name='Time', value_name='All Zscore(TC)').reset_index()
origin_tcs = origin_tcs.astype(float)
for row in origin_tcs.index:
    origin_tcs.loc[row, ~pd.isnull(origin_tcs.loc[row])] = zscore(origin_tcs.loc[row, ~pd.isnull(origin_tcs.loc[row])])
origin_melted = pd.melt(origin_tcs, ignore_index=False, var_name='Time', value_name='Origin Zscore(TC)').reset_index()

for row in all_scs.index:
    all_scs.loc[row, ~pd.isnull(all_scs.loc[row])] = zscore(all_scs.loc[row, ~pd.isnull(all_scs.loc[row])])
all_scs_melted = pd.melt(all_scs, ignore_index=False, var_name='Time', value_name='All Zscore(SC)').reset_index()


sns.lineplot(data=all_melted, x='Time', y='All Zscore(TC)', ax=ax0)
sns.lineplot(data=origin_melted, x='Time', y='Origin Zscore(TC)', ax=ax1)
sns.lineplot(data=all_scs_melted, x='Time', y='All Zscore(SC)', ax=ax2)
ax2.set_xlim(-120, -2)
plt.tight_layout()
plt.savefig(f'/Users/paulmueller/BIH_Neuro/LRTC_project/figures/preseizure_TC{AED_state}{vig_state}.pdf')
print('Done')
