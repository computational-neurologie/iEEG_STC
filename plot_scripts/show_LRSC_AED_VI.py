#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 10:42:19 2021

@author: paulmueller
"""

import glob
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import wilcoxon
from iEEG_STC import utils
import os

sns.set_palette("tab10")
sns.set_style("whitegrid")
font = {'family': 'sans-serif',
        'sans-serif': 'Arial',
        'weight': 'normal',
        'size': 11}
plt.rc('font', **font)
plt.rc('xtick', labelsize=11) 
plt.rc('ytick', labelsize=11) 
plt.rc('text', usetex=True)
colors = sns.color_palette('tab10')


def AnnoMe(x1, x2, y, TXT, ax_object, dh=.1):
    ax_object.plot([x1, x1, x2, x2], [y, y+dh, y+dh, y], lw=1, c='k')
    ax_object.text((x1+x2)*.5, y+dh, TXT, ha='center', va='bottom', color='k')


def make_sig_str(pval):
    if pval > .05:
        return 'n.s.'
    elif pval < 0.001:
        return r'$p<0.001$'
    elif pval < 0.01:
        return r'$p<0.01$'
    else:
        return r'$p<0.05$'


PATH = '/Users/paulmueller/BIH_Neuro/LRTC_project'  # path to the data
states = ['SWS', 'nonSWS', 'low_AED', 'high_AED']  # States of interest

ind_plots = False  # Plot individual CCs
lrsc = pd.DataFrame(columns=pd.Index(states, name='State'))
surr_lrsc = pd.DataFrame(columns=pd.Index(states, name='State'))

# Containers for the crosscorrealtion functions per state for each pat
low_AED_cc = pd.DataFrame()
high_AED_cc = pd.DataFrame()
low_AED_cc_surr = pd.DataFrame()
high_AED_cc_surr = pd.DataFrame()

for path in glob.glob(f'{PATH}/pat*'):
    patID = path.split('/')[-1]
    print(patID)
    try:
        # Read the data and surrogate data
        cc = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                         '0.125_8_hanning_0.5_constant_256_60_True_056_096_averaged_cc_new_not_norm.pkl'),
                            )
        surr_cc = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                              '0.125_8_hanning_0.5_constant_256_60_True_056_096_averaged_cc_new_not_norm_00surr.pkl'),
                                 )
    except FileNotFoundError:
        print(f'{patID} files not found')
        continue
    low_AED_cc[patID] = cc.loc[:, 'low_AED'].squeeze()
    high_AED_cc[patID] = cc.loc[:, 'high_AED'].squeeze()
    low_AED_cc_surr[patID] = surr_cc.loc[:, 'low_AED'].squeeze()
    high_AED_cc_surr[patID] = surr_cc.loc[:, 'high_AED'].squeeze()
    # Calculate LRSC in the data
    ind = cc.loc[np.logical_and(cc.index >= 9, cc.index < 79)].mean()
    ind.name = patID
    lrsc = lrsc.append(ind)
    # and in the surrogate
    ind = surr_cc.loc[np.logical_and(surr_cc.index >= 9, surr_cc.index < 79)].mean()
    ind.name = patID
    surr_lrsc = surr_lrsc.append(ind)
    # Make individual plots of cc
    if ind_plots:
        cc.dropna(axis=0, inplace=True)
        fig, (ax1, ax2) = plt.subplots(nrows=2, figsize=(4.8, 3.2), sharex=True)
        
        ax1.plot(cc.index, cc['nonSWS'], label='nonSWS', color=colors[0])

        ax1.plot(cc.index, cc['SWS'], label='SWS', color=colors[1])

        ax2.plot(cc.index, cc['low_AED'], label='low AED', color=colors[0])

        ax2.plot(cc.index, cc['high_AED'], label='high AED', color=colors[1])

        ax1.axvspan(7, 79, color='gray', alpha=.5, zorder=1)
        ax2.axvspan(7, 79, color='gray', alpha=.5, zorder=1)
        ax1.set_xlim(0, cc.index[-1])
        ax2.set_xlim(0, cc.index[-1])
        ax1.set_ylim(0, 1)
        ax2.set_ylim(0, 1)
        ax1.legend(loc=1)
        ax2.legend(loc=1)
        
        fig.suptitle(patID)
        fig.supylabel('Crosscorrelation')
        plt.tight_layout()
        plt.savefig(os.path.join(path, 'processed', f'{patID}_figures', 'average_cc_new_subsampling_not_norm.pdf'))
        plt.close(fig)
# Show statistical differences of LRSC between different states
print(lrsc['low_AED'] - lrsc['high_AED'])
print(f"SWS vs. nonSWS {wilcoxon(lrsc['SWS'],lrsc['nonSWS'])}")
print(f"high vs. low AED {wilcoxon(lrsc['high_AED'],lrsc['low_AED'])}")
       
# Plot the barsplots for LRSC
fig, (ax1, ax2) = plt.subplots(figsize=(4.8, 3.2), ncols=2, sharey=True)
# Chhange column names to make LaTeX plotable
lrsc.columns = [c.replace('_', ' ') for c in lrsc.columns]
surr_lrsc.columns = [c.replace('_', ' ') for c in surr_lrsc.columns]
# Define the width of the bars
width = .4
# Plot LRSC lines patient wise
for pat in lrsc.index:
    # print(f"{pat} low - high AED = {lrsc.loc[pat, 'low AED']-lrsc.loc[pat, 'high AED']}")
    ax1.plot([0, 1], lrsc.loc[pat, ['SWS', 'nonSWS']], color='k', linewidth=.5, alpha=.5, zorder=0)
    ax2.plot([0, 1], lrsc.loc[pat, ['high AED', 'low AED']], color='k', linewidth=.5, alpha=.5, zorder=0)
# Barplots
bplot1 = sns.barplot(data=pd.melt(lrsc.loc[:, ['SWS', 'nonSWS']]), x='variable', y='value', ax=ax1, palette=colors[2:4],
                     capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=colors[2:4], zorder=1)
# also for surrogate data
surr_bplot1 = sns.barplot(data=pd.melt(surr_lrsc.loc[:, ['SWS', 'nonSWS']]), x='variable', y='value', ax=ax1,
                          palette=['gray', 'gray'], capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=['gray', 'gray'],
                          zorder=1)

bplot2 = sns.barplot(data=pd.melt(lrsc.loc[:, ['high AED', 'low AED']]), x='variable', y='value', ax=ax2,
                     palette=colors[0:2], capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=colors[0:2])

surr_bplot2 = sns.barplot(data=pd.melt(surr_lrsc.loc[:, ['high AED', 'low AED']]), x='variable', y='value', ax=ax2,
                          palette=['gray', 'gray'], capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=['gray', 'gray'])
# Adjust width and position of the bars
i = 0
for b1, b2 in zip(bplot1.patches, bplot2.patches):
    if i < 2:  # For the data adjust only the width
        centre = b1.get_x() + b1.get_width()/2.
        b1.set_x(centre-width/2)
        b1.set_width(width)
        centre = b2.get_x() + b2.get_width()/2.
        b2.set_x(centre-width/2)
        b2.set_width(width)
    else:  # For the surrogate also shift slightly to the right
        centre = b1.get_x() + b1.get_width()/2+width/2
        b1.set_x(centre-width/2)
        b1.set_width(width)
        centre = b2.get_x() + b2.get_width()/2+width/2
        b2.set_x(centre-width/2)
        b2.set_width(width)
    i += 1
# Adjust the whisker position accordingly
i = 0
for l1, l2 in zip(bplot1.lines, bplot2.lines):
    if i > 28:
        if l1._linewidth == 1.2:  # This is the width of error lines
            l1.set_data(l1._x+width/2, l1._y)
            l2.set_data(l2._x+width/2, l2._y)
            pass
    i += 1
# Calculate median values
meds = lrsc.median()
# Plot the median
ax1.plot([-width/2, width/2], [meds['SWS'], meds['SWS']], color='k', linestyle='--', linewidth=2)
ax1.plot([1-width/2, 1+width/2], [meds['nonSWS'], meds['nonSWS']], color='k', linestyle='--', linewidth=2)

ax2.plot([-width/2, width/2], [meds['high AED'], meds['high AED']], color='k', linestyle='--', linewidth=2)
ax2.plot([1-width/2, 1+width/2], [meds['low AED'], meds['low AED']], color='k', linestyle='--', linewidth=2)

# Set axis labels
ax1.set_ylabel('SC')
ax1.set_xlabel(None)
ax2.set_ylabel(None)
ax2.set_xlabel(None)
# Set axis limits
offset = .65
dh = offset/35
ax1.set_xlim(-width*2/3, 1+width*2/3+width/2)
ax2.set_xlim(-width*2/3, 1+width*2/3+width/2)
ax1.set_ylim(-dh/7, offset+dh)
# Draw the significance markers
AnnoMe(0, 1, offset, make_sig_str(wilcoxon(lrsc['SWS'], lrsc['nonSWS'])[1]), ax1, dh=dh)
AnnoMe(0, 1, offset, make_sig_str(wilcoxon(lrsc['high AED'], lrsc['low AED'])[1]), ax2, dh=dh)

sns.despine(offset=10, trim=True, left=True, bottom=True)
plt.tight_layout()
plt.savefig(os.path.join(PATH, 'figures', 'new_SC_AED_VI_new_subsampling.pdf'))

# Draw the average crosscorrelation function
fig = plt.figure(figsize=(4.8, 3.6))

ax2 = plt.subplot2grid((3, 1), (2, 0))
# Plot number of patients with elctrode pairs at a given distance
ax2.plot(low_AED_cc.index, low_AED_cc.apply(lambda x: np.sum(~ pd.isnull(x)), axis=1), color='k')
ax2.axvspan(9, 79, alpha=.3, color='gray')

ax2.set_ylabel('\#Patients')
ax2.set_xlabel('Inter Electrode Distance (mm)')
# ax2.axvspan(9, 79, alpha=.3, color='gray')
# Plot the actual ccc function
ax1 = plt.subplot2grid((3, 1), (0, 0), rowspan=2, sharex=ax2)
# Bring the cc into a format plotable with ci for low AED
low_AED_cc = pd.melt(low_AED_cc, ignore_index=False)
low_AED_cc.reset_index(inplace=True)
low_AED_cc_surr = pd.melt(low_AED_cc_surr, ignore_index=False)
low_AED_cc_surr.reset_index(inplace=True)
# Plot commands data low AED
sns.lineplot(data=low_AED_cc.loc[~pd.isnull(low_AED_cc['value'])], x='Dist', y='value', ax=ax1, label='low AED',
             color=colors[1])
# and surrogate
sns.lineplot(data=low_AED_cc_surr.loc[~pd.isnull(low_AED_cc_surr['value'])], x='Dist', y='value', ax=ax1,
             label='surrogate/low AED', color=colors[1], alpha=.8, linestyle=':')
# And high AED
high_AED_cc = pd.melt(high_AED_cc, ignore_index=False)
high_AED_cc.reset_index(inplace=True)
high_AED_cc_surr = pd.melt(high_AED_cc_surr, ignore_index=False)
high_AED_cc_surr.reset_index(inplace=True)
# Plot
sns.lineplot(data=high_AED_cc.loc[~pd.isnull(high_AED_cc['value'])], x='Dist', y='value', ax=ax1, label='high AED',
             color=colors[0])
sns.lineplot(data=high_AED_cc_surr.loc[~pd.isnull(high_AED_cc_surr['value'])], x='Dist', y='value', ax=ax1,
             label='surrogate/high AED', color=colors[0], alpha=.8, linestyle=':')

ax1.axvspan(9, 79, alpha=.3, color='gray')

ax1.set_ylabel('Crosscorrelation')
ax1.set_xlim(0, 150)
plt.setp(ax1.get_xticklabels(), visible=False)
ax1.set_xlabel(None)
ax1.legend(borderaxespad=0)

plt.tight_layout()
plt.savefig(os.path.join(PATH, 'figures', 'new_cc_function_new_subsampling_not_norm.pdf'))
