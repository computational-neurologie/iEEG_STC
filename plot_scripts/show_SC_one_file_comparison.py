#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 13:37:38 2021

@author: paulmueller
"""
import glob
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import wilcoxon
from iEEG_STC import utils
import os
sns.set_palette("tab10")
sns.set_style("whitegrid")
font = {'family': 'sans-serif',
        'sans-serif': 'Arial',
        'weight': 'normal',
        'size': 11}
plt.rc('font', **font)
plt.rc('xtick', labelsize=11) 
plt.rc('ytick', labelsize=11) 
plt.rc('text', usetex=True)
colors = sns.color_palette('tab10')


def AnnoMe(x1, x2, y, TXT, ax_object, dh=.1):
    """Make an annotation with a bracket in the data"""
    ax_object.plot([x1, x1, x2, x2], [y, y+dh, y+dh, y], lw=1, c='k')
    ax_object.text((x1+x2)*.5, y+dh, TXT, ha='center', va='bottom', color='k')


def make_sig_str(pval):
    """Make a string for significance levels (0.05,0.01,0.001) depending on the pval"""
    if pval > .05:
        return 'n.s.'
    elif pval < 0.001:
        return r'$p<0.001$'
    elif pval < 0.01:
        return r'$p<0.01$'
    else:
        return r'$p<0.05$'


PATH = '/Users/paulmueller/BIH_Neuro/LRTC_project'   # Path to data
bands = ['056_096', ]#'004_008', '008_012', '012_028', '032_044',  '004_096']
states = ['Total', 'SWS', 'nonSWS', 'low_AED', 'high_AED',
          'SWS_low_AED', 'SWS_high_AED', 'nonSWS_low_AED', 'nonSWS_high_AED']
combinations_to_allow = [('nonSWS_low_AED', 'nonSWS_high_AED'),# ('low_AED', 'high_AED'),
                       #  ('SWS_low_AED', 'SWS_high_AED'),
                          ('SWS_low_AED', 'nonSWS_low_AED'),
                         #('SWS', 'nonSWS'),
                       #  ('SWS_high_AED', 'nonSWS_high_AED')
                         ]
all_p_values = pd.DataFrame()
k = 0
max_k = len(bands)*len(combinations_to_allow)
for band in bands:
    fig1, ax1 = plt.subplots(figsize=(4.8, 3.2), ncols=len(combinations_to_allow), sharey=True)
    if not isinstance(ax1, (np.ndarray, list)):
        ax1  = [ax1]
    pvals = []
    offset = 0
    for i, (state_0, state_1) in enumerate(combinations_to_allow):
        cc_0, cc_1 = pd.DataFrame(), pd.DataFrame()
        surr_cc_0, surr_cc_1 = pd.DataFrame(), pd.DataFrame()
        k += 1
        print(f'{k}/{max_k} : {band}: {state_0} vs. {state_1}')
        print(band, state_0, state_1)

        file = f'0.125_8_hanning_0.5_constant_256_60_True_{band}_{state_0}VS{state_1}_cc_noIED_nosubs.pkl'

        lim_in_sec = 40

        measure = 'SC (AUC 9 - 79 mm)'
        ind_plots = False  # Set to true to do plots for each patient

        measures = pd.DataFrame()
        surr_measures = pd.DataFrame()

        for path in glob.glob(f'{PATH}/pat*'):
            patID = path.split('/')[-1]
            # Extract the averaged ACF
            cc = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                             file))
            surr_cc = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                                  file.replace('.pkl', '_surr01.pkl')))

            # if 'low' in state_0 and 'high' in state_1:
            #     state_0, state_1 = state_1, state_0
            #     cc = cc.loc[:, [state_0, state_1]]
            #     surr_cc = surr_cc.loc[:, [state_0, state_1]]
            surr_cc.index.name = 'Distance'
            AUC9_79 = surr_cc.loc[np.logical_and(surr_cc.index >= 9, surr_cc.index < 79)].mean()
            AUC9_79.name = 'SC (AUC 9 - 79 mm)'
            AUC50_79 = surr_cc.loc[np.logical_and(surr_cc.index >= 50, surr_cc.index < 79)].mean()
            AUC50_79.name = 'SC (AUC 50 - 79 mm)'
            AUC20_40 = surr_cc.loc[np.logical_and(surr_cc.index >= 20, surr_cc.index < 40)].mean()
            AUC20_40.name = 'SC (AUC 20 - 40 mm)'
            AUC9_50 = surr_cc.loc[np.logical_and(surr_cc.index >= 9, surr_cc.index < 79)].mean()
            AUC9_50.name = 'SC (AUC 9 - 50 mm)'
            tmp = pd.concat([AUC9_79, AUC50_79, AUC9_50, AUC20_40], axis=1).T
            tmp.index = pd.MultiIndex.from_product([[patID], tmp.index])
            surr_measures = pd.concat([surr_measures, tmp])
            cc.index.name = 'Distance'
            AUC9_79 = cc.loc[np.logical_and(cc.index >= 9, cc.index < 79)].mean()
            AUC9_79.name = 'SC (AUC 9 - 79 mm)'
            AUC50_79 = cc.loc[np.logical_and(cc.index >= 50, cc.index < 79)].mean()
            AUC50_79.name = 'SC (AUC 50 - 79 mm)'
            AUC20_40 = cc.loc[np.logical_and(cc.index >= 20, cc.index < 40)].mean()
            AUC20_40.name = 'SC (AUC 20 - 40 mm)'
            AUC9_50 = cc.loc[np.logical_and(cc.index >= 9, cc.index < 79)].mean()
            AUC9_50.name = 'SC (AUC 9 - 50 mm)'
            tmp = pd.concat([AUC9_79, AUC50_79, AUC9_50, AUC20_40], axis=1).T
            tmp.index = pd.MultiIndex.from_product([[patID], tmp.index])
            measures = pd.concat([measures, tmp])

            # Make seperate frames for the states
            cc_0[patID] = cc.iloc[:, 0]
            cc_1[patID] = cc.iloc[:, 1]
            surr_cc_0[patID] = surr_cc.iloc[:, 0]
            surr_cc_1[patID] = surr_cc.iloc[:, 1]
            # Plot ACF for each pat individually
            if ind_plots:

                cc.set_index('Distance', drop=True, inplace=True)
                figind, axind = plt.subplots(nrows=1, figsize=(4.8, 3.2), sharex=True)
                # Plot the ACFs
                axind.plot(cc.index[1:lim_in_sec*8], cc.values[1:lim_in_sec*8, 0],
                           label=cc.columns[0].replace('_', ' '), color=colors[0])
                axind.plot(cc.index[1:lim_in_sec * 8], cc.values[1:lim_in_sec * 8, 1],
                           label=cc.columns[1].replace('_', ' '), color=colors[1])

                axind.legend()
                axind.set_xlim(cc.index[1], cc.index[8*lim_in_sec])
                axind.set_xlabel('Lag (sec.)')
                axind.set_ylabel('Autocorrelation')
                axind.set_title(patID)
                figind.tight_layout()
                figind.savefig(os.path.join(PATH, 'spec_figures',
                               f'{patID}_{band}_{state_0}VS{state_1}_cc_noIED_nosubs.pdf'))

        # Change order for ascending
        if 'low' in state_0 and 'high' in state_1:
            state_0, state_1 = state_1, state_0
            measures = measures.loc[:, [state_0, state_1]]
            surr_measures = surr_measures.loc[:, [state_0, state_1]]
            cc_0, cc_1 = cc_1, cc_0
            surr_cc_0, surr_v_1 = surr_cc_1, surr_cc_0

        for m in measures.index.levels[1]:
            willi = wilcoxon(measures.loc[((~pd.isnull(measures)).all(axis=1), m), state_0],
                             measures.loc[((~pd.isnull(measures)).all(axis=1), m), state_1])
            pval = willi[1]
            print(f"{m}: {state_0} vs. {state_1} {willi}")
            all_p_values = pd.concat([all_p_values, pd.Series([band, state_0, state_1, m, pval],
                                                              index=['Band', 'State0', 'State1',
                                                                     'Measure', 'pval']).to_frame().T])
        for m in surr_measures.index.levels[1]:
            try:
                willi = wilcoxon(surr_measures.loc[((~pd.isnull(surr_measures)).all(axis=1), m), state_0],
                                 surr_measures.loc[((~pd.isnull(surr_measures)).all(axis=1), m), state_1])
                pval = willi[1]
                print(f"Surrogate {m}: {state_0} vs. {state_1} {willi}")
            except ValueError as e:
                print(e)
                print(f"Surrogate {m}: {state_0} vs. {state_1} {1.0}")

        #measures.columns = [c.replace('_', ' ') for c in measures.columns]



        width = .4
        for pat in measures.index.levels[0]:

            ax1[i].plot([0, 1], measures.loc[(pat, measure), :], color='k', linewidth=.5, alpha=.5, zorder=1)

        # Plot the sws and nonsws LRTC
        bplot1 = sns.barplot(data=pd.melt(measures.loc[(slice(None), measure), :]), x='variable', y='value', ax=ax1[i],
                             palette=colors[2*(i/len(combinations_to_allow) >= 1/2):
                                                  2+2*(i/len(combinations_to_allow) >= 1/2)],
                             capsize=width/2, errwidth=1.2, alpha=.7,
                             edgecolor=colors[2*(i/len(combinations_to_allow) >= 1/2):
                             2+2*(i/len(combinations_to_allow) >= 1/2)], zorder=2)
        surr_bplot1 = sns.barplot(data=pd.melt(surr_measures.loc[(slice(None), measure), :]), x='variable', y='value',
                                  ax=ax1[i],
                             palette=['gray', 'gray'],
                             capsize=width / 2, errwidth=1.2, alpha=.7, edgecolor=['gray', 'gray'], zorder=2)

        for j, b1 in enumerate(bplot1.patches):
            if j < 2:  # Non surrogates only the width
                centre = b1.get_x() + b1.get_width() / 2.
                b1.set_x(centre - width / 2)
                b1.set_width(width)
            else:  # Surrogate sligthly to the right
                centre = b1.get_x() + b1.get_width() / 2 + width / 2
                b1.set_x(centre - width / 2)
                b1.set_width(width)
            j += 1
            # Adjust the whiskers to conform to the bars
        for j, l1 in enumerate(bplot1.lines):
            if j > 28:
                if l1._linewidth == 1.2:
                    l1.set_data(l1._x + width / 2, l1._y)
            j += 1
        # Calculate the median of the LRTC

        meds = measures.groupby(level=1).median()
        # Plot the medians of the data
        ax1[i].plot([-width/2, width/2], [meds.loc[measure, state_0], meds.loc[measure, state_0]],
                     color='k', linestyle='--', linewidth=2)
        ax1[i].plot([1-width/2, 1+width/2], [meds.loc[measure, state_1], meds.loc[measure, state_1]],
                     color='k', linestyle='--', linewidth=2)
        # Set labels
        ax1[i].set_ylabel(None)
        ax1[i].set_xlabel(None)



        sns.despine(offset=10, trim=True, left=True, bottom=True, ax=ax1[i])

        # Set limits
        ax1[i].set_xlim(-width*2/3, 1+width*2/3+width/2)
        if len(state_0.split('_')) < 3:
            if 'SWS' in state_0:
                plot_title = 'low \& high AED'
            else:
                plot_title = 'nonSWS \& SWS'
            xtick_labels = [state_0.replace('_', ' '), state_1.replace('_', ' ')]
        elif state_0[0] == state_1[0]:
            plot_title = state_0.split('_')[0]
            xtick_labels = [' '.join(state_0.split('_')[1:]), ' '.join(state_1.split('_')[1:])]
        else:
            plot_title = ' '.join(state_0.split('_')[1:])
            xtick_labels = [state_0.split('_')[0], state_1.split('_')[0]]
        ax1[i].set_xticks([0.1, 1.1])
        ax1[i].set_xticklabels(xtick_labels,
                               fontsize=9)  # [state_0 if len(state_0) < 8 else state_0.replace('SWS', 'SWS\n'),
        # state_1 if len(state_1) < 8 else state_1.replace('SWS', 'SWS\n')], rotation=90)
        ax1[i].set_xlabel(plot_title)

        pvals.append(wilcoxon(measures.loc[(slice(None), measure), state_0],
                              measures.loc[(slice(None), measure), state_1])[1])
        if np.max(measures.loc[(slice(None), measure), :].values.flatten()) > offset:
            offset = np.max(measures.loc[(slice(None), measure), :].values.flatten())

        # Plot the complete averaged autocorrelation funciton
        fig = plt.figure(figsize=(4.8, 3.2))
        #fig, ax = plt.subplots(figsize=(4.8, 3.2))
        ax2 = plt.subplot2grid((3, 1), (2, 0))
        ax = plt.subplot2grid((3, 1), (0, 0), rowspan=2, sharex=ax2)
        # Plot number of patients with elctrode pairs at a given distance
        ax2.plot(cc_0.index, cc_0.apply(lambda x: np.sum(~ pd.isnull(x)), axis=1), color='k')
        ax2.axvspan(9, 79, alpha=.3, color='gray')

        ax2.set_ylabel('\#Patients')
        ax2.set_xlabel('Inter Electrode Distance (mm)')
        lim_in_sec = 20
        # cc_0 = cc_0.iloc[1:8*lim_in_sec]
        # cc_1 = cc_1.iloc[1:8*lim_in_sec]

        state_0 = state_0.replace('_', ' ')
        state_1 = state_1.replace('_', ' ')
        # Plot with ci (default is bootstraping)
        ax.axvspan(9, 79, alpha=.3, color='gray')
        sns.lineplot(data=pd.melt(cc_0, ignore_index=False).reset_index(), x='Distance', y='value', ax=ax,
                     label=state_0, color=colors[0])  # ,ci='sd')
        sns.lineplot(data=pd.melt(cc_1, ignore_index=False).reset_index(), x='Distance', y='value', ax=ax,
                     label=state_1, color=colors[1])  # ,ci='sd')
        sns.lineplot(data=pd.melt(surr_cc_0, ignore_index=False).reset_index(), x='Distance', y='value', ax=ax,
                     label=state_0, color=colors[0], linestyle='--')  # ,ci='sd')
        sns.lineplot(data=pd.melt(surr_cc_1, ignore_index=False).reset_index(), x='Distance', y='value', ax=ax,
                     label=state_1, color=colors[1], linestyle='--')
        state_0 = state_0.replace(' ', '_')
        state_1 = state_1.replace(' ', '_')
        ax.set_ylabel(f'Crosscorrelation\n({plot_title})')
        print(ax.get_legend_handles_labels())
        ax.set_xlabel(None)#'Inter Electrode Distance (mm)')
        plt.setp(ax.get_xticklabels(), visible=False)
        # Set limit
        ax.set_xlim(0, 150)
        # Make legend
        ax.legend(borderaxespad=0, labels=np.append(xtick_labels, ['surrogate '+ x for x in xtick_labels]))
        # Save
        plt.tight_layout()
        fig.align_ylabels([ax, ax2])
        plt.savefig(os.path.join(PATH, 'figures', band, f'{band}_{state_0}VS{state_1}_noIED_nosubs_average_cc.pdf'))
        # Draw the significance indicators
    offset *= 1.01
    dh = offset / 35
    for i in range(len(combinations_to_allow)):
        AnnoMe(0, 1, offset, make_sig_str(pvals[i]),
               ax1[i], dh=dh)
    ax1[0].set_ylabel(measure)
   # fig1.suptitle(' - '.join([b.lstrip('0') + ' Hz'for b in band.split('_')]))
    fig1.tight_layout()
    if not os.path.isdir(os.path.join(PATH, 'figures', measure)):
        os.mkdir(os.path.join(PATH, 'figures', measure))
    fig1.savefig(os.path.join(PATH, 'figures',
                              measure, f'{band}_{measure}_noIED_nosubs_cc_{len(combinations_to_allow)}.pdf'))

# all_p_values.to_csv(os.path.join(PATH, 'ensemble_data', 'aggregated_pvalues_cc.csv'), index=False)

