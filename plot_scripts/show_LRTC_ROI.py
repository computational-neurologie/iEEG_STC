#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 11:25:33 2021

@author: paulmueller
"""

import glob
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import spearmanr, mannwhitneyu, linregress, wilcoxon

from iEEG_STC import utils
import os

sns.set_palette("tab10")
sns.set_style("whitegrid")
font = {'family': 'sans-serif',
        'sans-serif': 'Arial',
        'weight': 'normal',
        'size': 11}
plt.rc('font', **font)
plt.rc('xtick', labelsize=11) 
plt.rc('ytick', labelsize=11) 
plt.rc('text', usetex=True)


def AnnoMe(x1, x2, y, TXT, ax_object, dh=.1):
    """Make an annotation with a bracket in the data"""
    ax_object.plot([x1, x1, x2, x2], [y, y+dh, y+dh, y], lw=1, c='k')
    ax_object.text((x1+x2)*.5, y+dh, TXT, ha='center', va='bottom', color='k')


def make_sig_str(pval):
    """Make a string for significance levels (0.05,0.01,0.001) depending on the pval"""
    if pval > .05:
        return 'n.s.'
    elif pval < 0.001:
        return r'$p<0.001$'
    elif pval < 0.01:
        return r'$p<0.01$'
    else:
        return r'$p<0.05$'


# Define the colors to plot the different rois
colors = np.array([(0.8392156862745098, 0.15294117647058825, 0.1568627450980392),
                   (1.0, 0.4980392156862745, 0.054901960784313725),
                   (0.17254901960784313, 0.6274509803921569, 0.17254901960784313),
                   (0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
                   (0.5803921568627451, 0.403921568627451, 0.7411764705882353)])

PATH = '/Users/paulmueller/BIH_Neuro/LRTC_project'  # Where to find the data
states = ['Total']  # Which state are you interested
rois = ['MT', 'LIP', 'LPFC', 'OFC', 'ACC']  # Which rois are you interested
lrtc = pd.DataFrame(columns=pd.Index(rois, name='ROI'))  # DataFrame for the lrtc for all regions of interest
surr_lrtc = pd.DataFrame(columns=pd.Index(rois, name='ROI'))  # DataFrame for the surr -""-

# Cycle through all patitents
for path in glob.glob(f'{PATH}/pat*'):
    
    patID = path.split('/')[-1]
    # Extract the averaged ACF
    acf = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                      '0.125_8_hanning_0.5_constant_256_60_True_056_096_averaged_acf_new_not_norm.pkl')).astype(float)

    # Get only the state of interest
    acf = acf.loc[:, pd.MultiIndex.from_product([rois, states], names=['ROI', 'State'])]
    # Drop the index level as only one state should be of interest
    acf.columns = acf.columns.droplevel('State')
    acf.reset_index(inplace=True)
    # Calculate the LRTC
    ind, HM = utils.hwhm(acf)
    ind.name = patID
    HM.name = patID
    # Add it to the corresponding DataFrame
    lrtc = lrtc.append(ind).astype(float)
    # Get surrogate data
    acf = pd.read_pickle(os.path.join(path, 'processed', '0.125_8_hanning_0.5_constant_256_60_True',
                                      '0.125_8_hanning_0.5_constant_256_60_True_056_096_averaged_acf_new_not_norm_00surr.pkl')).astype(float)

    # Get only the state of interest
    acf = acf.loc[:, pd.MultiIndex.from_product([rois, states], names=['ROI', 'State'])]
    # Drop the index level as only one state should be of interest
    acf.columns = acf.columns.droplevel('State')
    acf.reset_index(inplace=True)
    # Calculate the LRTC
    ind, HM = utils.hwhm(acf)
    ind.name = patID
    HM.ind = patID
    # Add it to the corresponding DataFrame
    surr_lrtc = surr_lrtc.append(ind).astype(float)

# Define the width of each bar
width = 0.5
fig = plt.figure(figsize=(4.8, 3.2))
# Make the two subplots for number of pats with roi and LRTC per roi
ax1 = plt.subplot2grid((4, 1), (1, 0), rowspan=3)
ax2 = plt.subplot2grid((4, 1), (0, 0), rowspan=1, sharex=ax1)
# Plot LRTC per ROI
bplot1 = sns.barplot(data=pd.melt(lrtc), x='ROI', y='value', ax=ax1, palette=colors,
                     capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=colors, zorder=2)
# Plot sur LRTC per ROI
surrbp1 = sns.barplot(data=pd.melt(surr_lrtc), x='ROI', y='value', ax=ax1, palette=np.full(5, 'gray'),
                      capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=np.full(5, 'gray'), zorder=2)
# Plot number of pat with ROI
bplot2 = sns.barplot(x=lrtc.columns, y=lrtc.notnull().sum(), ax=ax2, palette=colors,
                     capsize=width/2, errwidth=1.2, alpha=.7, edgecolor=colors, zorder=2)
# Go through the LRTC plot and adjust there width and position
i = 0
for b1 in bplot1.patches:
    if i < 5:  # Keep lrtc in place
        centre = b1.get_x() + b1.get_width()/2.
        b1.set_x(centre-width/2)
        b1.set_width(width)
    else:  # Move the surrogate slightly to the right
        centre = b1.get_x() + b1.get_width()/2.+width/2
        b1.set_x(centre-width/2)
        b1.set_width(width)
    i += 1
# Go through the number plot and adjust the width of the bars as well
for b2 in bplot2.patches:
    centre = b2.get_x() + b2.get_width()/2.
    b2.set_x(centre-width/2)
    b2.set_width(width)
# Go through all whiskers and move them accordingly
for l1 in bplot1.lines:
    if i > 24:
        if l1._linewidth == 1.2:

            l1.set_data(l1._x + width/2, l1._y)
          
            pass
    i += 1
# Plot the individual lines connecting LRTC of single patients
for pat in lrtc.index:
    ax1.plot(np.arange(0, 5)[lrtc.loc[pat].notnull()], lrtc.loc[pat, lrtc.loc[pat].notnull()],
             color='k', linewidth=.5, alpha=.5, zorder=1)
# Calculate the median LRTC for the data
meds = lrtc.median()
# Plot the median data (only for real data)
for i, c in enumerate(meds.index):
    ax1.plot([i-width/2, i+width/2], [meds[c], meds[c]], color='k', linestyle='--', linewidth=2)

# Set axisi labels
ax1.set_ylabel('TC (sec.)')
ax1.set_xlabel(None)
ax2.set_ylabel('\#Patients')
ax2.set_xlabel(None)
# Set axis limits
ax2.set_ylim(-.4, 14)
ax1.set_xlim(-width*2/3, 4+width*2/3+width/2)
# Remove the tick labels
plt.setp(ax2.get_xticklabels(), visible=False)

sns.despine(offset=10, trim=True, left=True, bottom=True)

plt.tight_layout()
plt.savefig(os.path.join(PATH, 'figures', 'TC_ROI_new_subsampling_not_norm.pdf'))

# Compare all rois with each other with a mann whitney u test
for i in range(0, 5):
    for j in range(i+1, 5):
        mwu = mannwhitneyu(lrtc.iloc[:, i].loc[~pd.isna(lrtc.iloc[:, i])],
                           lrtc.iloc[:, j].loc[~pd.isna(lrtc.iloc[:, j])])
        print(f"{lrtc.columns[i]} vs. {lrtc.columns[j]} {mwu} ")

# Calculate the spearman rank correlation of the means and the medians
print(f'Mean: {spearmanr(np.arange(0,5),lrtc.mean())}')
print(f'Median {spearmanr(np.arange(0,5),lrtc.median())}')
# Calculate slopes (spearmans) for every patient and calculate an average slope
slopes = []
spears = []
rhos = []
# Cycle through all patients
for i in lrtc.index[:]:
    tmp = lrtc.loc[i]
    tmp.index = [rois.index(j) for j in tmp.index]
    if np.sum(~tmp.isnull()) >= 2:  # only regress if the at least two non nan values are found
        tmp = tmp.loc[~tmp.isnull()]
        # Linearily regress
        reg_params = linregress(tmp.index, tmp.values)
        slopes.append(reg_params[0])
        # and spearmanrs and pvals
        r, p = spearmanr(tmp.index, tmp.values)
        # Skipp if the spearman is not defined
        if np.isnan(r):
            continue
        spears.append(p)
        rhos.append(r)
# Show all slopes
print('Slopes')
print(slopes)
print(np.mean(slopes), np.std(slopes)/np.sqrt(len(slopes)))
print(f'Different from zero {wilcoxon(slopes)}')
# Show the Spearmanrs
print('Spearmanr rhos')
print(rhos)
print(np.mean(rhos), np.std(rhos)/np.sqrt(len(rhos)))
print(f'Different from zero {wilcoxon(rhos)}')
