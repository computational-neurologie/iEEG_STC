"""
Read LRSTC files and plot for each patient a within correlation scatter plot
Make Linear regression on these and aggregate to one patient wide plot
"""
import glob
import pandas as pd
from matplotlib.ticker import FormatStrFormatter
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import seaborn as sns
import numpy as np
from scipy.stats import spearmanr, combine_pvalues
from scipy.optimize import curve_fit

sns.set_palette("tab10")
sns.set_style("whitegrid")
font = {'family': 'sans-serif',
        'sans-serif': 'Arial',
        'weight': 'normal',
        'size': 11}
plt.rc('font', **font)
plt.rc('xtick', labelsize=11)
plt.rc('ytick', labelsize=11)
plt.rc('text', usetex=True)
txt_box_props = dict(boxstyle='round', facecolor='white', alpha=.5)
empty_rect = Rectangle((0, 0), 1, 1, fill=False, edgecolor='none', visible=False)


def power_law(x, a, b):
    return np.log(x/b)/np.log(a)


PATH = '/Users/paulmueller/BIH_Neuro/LRTC_project'
spearman_outs = pd.DataFrame()
for path in glob.glob(f'{PATH}/pat*'):

    patID = path.split('/')[-1]
    lrstc_frame = pd.DataFrame()
    for file in glob.glob(f'{path}/processed/0.125_8_hanning_0.5_constant_256_60_True/' +
                          '0.125_8_hanning_0.5_constant_256_60_True_056_096_LRSTC_*[0-9].pkl'):
        tmp = pd.read_pickle(file)
        lrstc_frame = lrstc_frame.append(tmp)
    lrstc_frame = lrstc_frame.loc[~np.any(pd.isnull(lrstc_frame), axis=1)]

    classifier = pd.read_csv(f'{path}/meta/{patID}_classifier_new.csv', index_col=0)
    full_info = pd.merge(lrstc_frame, classifier, left_index=True, right_index=True)
    fig, ax = plt.subplots(figsize=(4.8, 3.2))
    ax = sns.scatterplot(data=full_info, x='LRTC', y='LRSC', ax=ax, alpha=1)  #
    rho, pval = spearmanr(full_info['LRTC'], full_info['LRSC'])
    spearman_outs = spearman_outs.append(pd.Series({'rho': rho, 'p': pval}, name=patID))

    fit_power = curve_fit(power_law, full_info['LRTC'], full_info['LRSC'], bounds=(0.001, np.inf))
    # place a text box in upper left in axes coords

    print(f'{patID}: r={rho}, p={pval}')

    x = np.linspace(np.min(full_info['LRTC']), np.max(full_info['LRTC']), 100)
    log_fit, = ax.plot(x, power_law(x, fit_power[0][0], fit_power[0][1]), color='k')
    ax.set_xlim(np.min(full_info['LRTC'])*0.95, np.max(full_info['LRTC'])*1.05)
    ax.set_ylim(np.min(full_info['LRSC'])*0.95, np.max(full_info['LRSC'])*1.05)
    pstr = f'$p=${pval:.2e}' if pval < 0.01 else f'$p=${pval:.2f}'
    ax.legend(handles=[log_fit,
                       empty_rect, empty_rect],
              labels=[f'$\log_{{{fit_power[0][0]:.0f}}}(x/{fit_power[0][1]:.2f})$',
                      r'Spearman $\rho=$'+f'{rho:.2f}', pstr])
    ax.set_xscale('log')

    # ax.xaxis.set_minor_formatter(FormatStrFormatter('%.2f'))
    # ax.grid(b=True, which='major', color='gray', alpha=.5)
    # ax.grid(b=True, which='minor', color='gray', alpha=.5)
    ymin = np.min(full_info['LRSC']) - np.abs(0.05*np.min(full_info['LRSC']))
    ymax = np.max(full_info['LRSC']) + np.abs(0.05*np.max(full_info['LRSC']))
    ax.set_xlim(np.min(full_info['LRTC']) * 0.95, np.max(full_info['LRTC']) * 1.05)
    ax.set_ylabel('SC')
    ax.set_xlabel('TC (sec.)')
    ax.set_ylim(ymin, ymax)
    ax.set_title(patID)
    plt.tight_layout()

    plt.savefig(f'{path}/processed/{patID}_figures/{patID}_stc_correlations_2.pdf')
print(spearman_outs['rho'].mean())
print(combine_pvalues(spearman_outs['p'].values))
