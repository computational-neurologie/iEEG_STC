#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 10:38:45 2021

@author: paulmueller
Runs calculation for each patient in one go
"""
import os
import subprocess
import glob


def write_submit_file(cmd, time_in_sec, job_name):
    """Create the submit file for the cluster."""

    h = str(int(time_in_sec // 3600)).zfill(2)
    m = str(int((time_in_sec % 3600) // 60)).zfill(2)
    s = str(int((time_in_sec % 3600) % 60)).zfill(2)

    new_sub_file_str = '#!/bin/bash\n\n'
    new_sub_file_str += f'#SBATCH --job-name={job_name}\n'
    new_sub_file_str += f'#SBATCH --output={job_name}.out\n'
    new_sub_file_str += f'#SBATCH --error={job_name}.err\n\n'
    new_sub_file_str += '#SBATCH --ntasks=1\n'
    new_sub_file_str += '#SBATCH --nodes=1\n'
    new_sub_file_str += '#SBATCH --mem-per-cpu=32G\n'
    new_sub_file_str += f'#SBATCH --time={h}:{m}:{s}\n \n'
    new_sub_file_str += cmd

    with open(f'{job_name}_submit.sh', 'w') as sub_file:
        sub_file.write(new_sub_file_str)


# Scirpt which should be run
CALCSCRIPT = '/data/gpfs-1/users/muellepm_c/scripts/iEEG_STC/scripts/calc_ACF_CC.py'
# Path were all the patients subdirectories (processed and meta data) are stored
PATH = '/data/gpfs-1/users/muellepm_c/work/LRTC_project'
bands = [[56,96],]#,[4,8],[8,12],[12,28],[32,44],[4,96]]
# Set calculate to False for testing purposes. (If all pat are found?)
calculate = True
# Scan for all patients
for i, pat in enumerate(glob.glob(f'{PATH}/pat*')[:]):
    # Get patient ID
    patID = pat.split('/')[-1]
    for j, b in enumerate(bands):
        print(patID,b)
        # if patID not in ['pat273']:
        #    continue

        command = f"python3.8 -u {CALCSCRIPT} {patID} {b[0]} {b[1]}"
        name = f'{patID}_{b[0]}_{b[1]}_NOSS'
        # Change to submit directory
        os.chdir('/data/gpfs-1/users/muellepm_c/scratch/submit_scripts')
        # Write submit file
        write_submit_file(command,  60*60 * 60, name)
        # Run the CALCSCRIPT for every patID
        if calculate:
            print(f'Starting job number: {i}.{j} / {patID}')
            print(f'Job_name: {name}\n')
            p = subprocess.Popen(['sbatch', f'{name}_submit.sh'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            p.wait()
