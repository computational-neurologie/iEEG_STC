#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 10:03:59 2021
@author: paulmueller
Takes a configuration file as first argument and datafiles as the others to calculate autocorrelation functions and
crosscorrelation functions from the data. Then calculates lrtc and lrsc measures from these.
"""
import os
import socket
import sys
import time
from shutil import copy
import glob
from datetime import datetime
import numpy as np
import pandas as pd

t0 = time.time()
# Check on which machine you are and add the path to epilepsiae_space_time_resolved (estr)
host = socket.gethostname()
# We assume that you are on the BIHC. If this is not the case add your machine and the location below
estr_dir = '/data/gpfs-1/users/muellepm_c/scripts/iEEG_STC'
# estr_dir = '../iEEG_STC'
sys.path.insert(0, estr_dir)

# Importing on the cluster only works within a try statement for unkonwn reasonss
try:
    import iEEG_STC.utils as utils
    import iEEG_STC.additional_functions as a_f
    from iEEG_STC.reader import ECOGReader
except ModuleNotFoundError:
    raise Exception('ImportError. Could not import iEEG_STC')
else:
    print('Importing completed')

force = False  # Force calculation even if data exists already. This overwrites old data
save_acf = False  # Save the autocorrelation function. Not really necessary as acf calculation is very fast
calc_is_surr_test = False  # Shuffle the data if before calculating the autocorrelation function if True
t_invest_before_seizure = 2*3600
# Path construction from the sys arguments
patient = sys.argv[1]  # 'pat958'
path_to_general = '/data/gpfs-1/users/muellepm_c/work/LRTC_project'
# path_to_general = '/Users/paulmueller/BIH_Neuro/LRTC_project'
path_to_raw_data = f'/data/gpfs-1/groups/ag_meisel/work/epilepsiae_data/{patient}/' +'*.data'
STC_CONFIGFILE = f'{path_to_general}/{patient}/meta/psd_0.125_full_length_5.0.config'
# Read the configuration file
stc_calc_parameters = a_f.read_config_file(STC_CONFIGFILE)
# Read seizures
seizures = pd.read_csv(f'{path_to_general}/{patient}/meta/{patient}_seizures.csv')
# Drop seizures which have any seizure 2h before their onset
seizures = seizures.sort_values('onset_ts')
seizures = seizures.reset_index(drop=True)
print(seizures)
for ind in seizures.index[:0:-1]:
    if seizures.loc[ind, 'onset_ts'] - seizures.loc[ind-1, 'offset_ts'] < t_invest_before_seizure:
        print('Dropping a seizure as it follows another by',
              seizures.loc[ind, 'onset_ts'] - seizures.loc[ind-1, 'offset_ts'])
        seizures = seizures.drop(ind)
# Read the classifier to identify which time windoes were calculated
classifier = pd.read_csv(f'{path_to_general}/{patient}/meta/{patient}_classifier_with_seizures.csv')
# Check which time windows were 2*3600 seconds bef
classifier['pre_seizure'] = False
for onset in seizures['onset_ts']:
    classifier.loc[np.logical_and(classifier['Time'] <= onset, onset-classifier['Time'] < t_invest_before_seizure),
                   'pre_seizure'] = True
if not any(classifier['pre_seizure']):
    raise Exception('Patient has no pre seizure segments')
# Delete all time points not around a seizure
classifier = classifier.loc[classifier['pre_seizure']]
# Sanity check. Should never run into that issue
if not all(classifier['pre_seizure']):
    raise ValueError('All time points should be around seizure')

DATA_FILES = sorted(glob.glob(path_to_raw_data))

t1 = time.time()
print(f'0. Initialization took {t1 - t0:.4f}sec')
acf = pd.DataFrame()
cc = pd.DataFrame()
for data_file in DATA_FILES:
    t1 = time.time()
    # Reads the .head file of the data
    data_object = ECOGReader(data_file)
    start_ts = data_object.head_dict['start_ts'].timestamp()
    print(data_object.head_dict['start_ts'])
    # Check if the calc_parameters file has the same patID as the
    try:
        if stc_calc_parameters['patID'] != data_object.head_dict['patID']:
            print(f"Skipping {data_file} because config file patID {stc_calc_parameters['patID']} " +
                  f"does not match data file patID {data_object.head_dict['patID']}")
            continue
    except KeyError:
        print(f"Skipping {data_file} because config file  misses the patID. Calculation without patID is deprecated")
        continue
    # Check if any of the time points within the file would be in range of a seizure
    if not any(np.logical_and(classifier['Time'] >= start_ts,
                              classifier['Time'] <= (start_ts +
                              data_object.head_dict['duration_in_sec'])
                              )):
        print(f"No data within range of seizure {data_object.head_dict['recordingID']}")
        continue
    else:
        print(f"Data in range of seizure{data_object.head_dict['recordingID']}")

    # Summarize parameters and print them
    stc_calc_parameters['recordingID'] = data_object.head_dict['recordingID']
    print(f"patID: {data_object.head_dict['patID']}, recordingID: {data_object.head_dict['recordingID']}")
    print(stc_calc_parameters)
    locations = a_f.data_file_locations_from_settings(stc_calc_parameters, path_to_general)

    # Extract electrodes
    elecs = pd.read_csv(f"{locations['meta']}/{stc_calc_parameters['patID']}_electrodes.csv", index_col=0)
    # Delete all non ecog electrodes, i.e., electrodes without coordinates
    elecs = elecs.loc[elecs['x'] != '-']
    # Get the distances between the channels
    dists = utils.get_channels_dists(elecs)

    # Construct the path to which processed data will be stored
    a_f.make_path(locations['processed'])

    t1_1 = time.time()
    # Create for each setting a folder
    a_f.make_path(locations['psd'])
    # Create a file_string for each band
    PSD_LOC = locations['psd'] + '/' + locations['settings']
    psd_files = [PSD_LOC + f'_{B[0]:03d}_{B[1]:03d}' + '_new_not_norm.csv' for B in stc_calc_parameters['bands']]
    # Check whether a file already exists for this band
    psd_band_exist = [os.path.isfile(F) for F in psd_files]

    if not all(psd_band_exist):
        raise FileNotFoundError('Not all PSD files exist and this script is not ment to calculate them.')
    # If all band powers exist, adjust the head_dict and read the band powers
    else:

        # Read power band by power band and put them all into one dataframe as if it was just calculated
        band_powers = pd.DataFrame()
        for f in psd_files:
            tmp = pd.read_csv(f)
            band_powers = pd.concat([band_powers, tmp])
        band_powers = band_powers.loc[:, np.intersect1d(band_powers.columns,
                                                        np.append(['Band', 'Time'], elecs.index))]

        t3 = time.time()
        print(f'1. PSD read. / {t3 - t1:.4f}sec')
    # Check if band powers (PSD) was found, raise an Exception if not
    if not isinstance(band_powers, pd.DataFrame):
        raise Exception('PSD not found')
    # ====== Calculate measures on the band power time series, e.g., acf/lrtc ====== #
    # Do calculations for each band seperately
    for band in set(band_powers['Band']):
        print(band)
        # Extract the specific band and adjust the dataframe to have time in the index and the channels in the cols
        tmp_psd = band_powers.loc[(band_powers['Band'] == band)].copy()
        tmp_psd.drop(columns='Band', inplace=True)
        tmp_psd.set_index('Time', drop=True, inplace=True)
        tmp_psd.sort_index(inplace=True)
        # Get the first time point of each window for which an acf will be calculated
        times = sorted(tmp_psd.index.unique())[::int(stc_calc_parameters['acf_length'] /
                                                     stc_calc_parameters['band_power_window_length_in_secs'])]
        # Check these timestamps against the ones found in the classifier and delete all which are not in it
        times = np.array(times)
        times = times[np.in1d(times, classifier['Time']-start_ts)]
        if len(times) == 0:
            raise ValueError('No times in seizure classifier. This must be an error in the code')
        # Convert band tuple to band string (with leading zeros)
        band = band[1:-1].split(',')
        band = f'{int(band[0]):03d}_{int(band[1]):03d}'
        # Construct the save path where XXXX is the place holder to put in LRTC or CC later
        save_path = (locations['processed'] + '/' + locations['settings'] +
                     f"_{band}_XXXX_{stc_calc_parameters['recordingID']}.csv")
        # Change the save path to account for surrogate data
        if calc_is_surr_test:
            save_path = save_path.replace('.csv', '_01surr.csv')

        for T in times:
            print(T)
            # Get the absolute time point
            current_t = start_ts + T
            # Extract the subframe of each segment
            bool_loc = np.logical_and(tmp_psd.index >= T, tmp_psd.index < T + stc_calc_parameters['acf_length'])
            data_window = tmp_psd.loc[bool_loc]
            # Shuffle the data if surrogate calculations should be done
            if calc_is_surr_test:
                data_window = utils.shuffle_all_channels(data_window)
            # Calculate the autocorrelation function
            tmp = utils.estimated_autocorrelation(data_window)
            tmp.index -= np.min(tmp.index)
            tmp.index = pd.MultiIndex.from_product([[band], [current_t], tmp.index], names=['Band', 'Time', 'Lag'])
            acf = pd.concat([acf, tmp])
            tmp = utils.cc_by_binned(data_window, dists)
            tmp.index = pd.MultiIndex.from_product([[band], [current_t], tmp.index], names=['Band', 'Time', 'Distance'])

            cc = pd.concat([cc, tmp.to_frame()], ignore_index=False)

    t4 = time.time()
    print(f'4. ACF/CC calculated. / {t4 - t3:.4f}sec')
save_path = f"{locations['processed']}/{locations['settings']}_pre_seizure_XXXX.pkl"
print(len(acf.index.levels[0]), len(cc.index.levels[0]))
acf.to_pickle(save_path.replace('XXXX', 'acf'))
cc.index = pd.MultiIndex.from_tuples(cc.index, names=['Band', 'Time', 'Distance'])
cc.to_pickle(save_path.replace('XXXX', 'cc'))
t5 = time.time()
print(f'Calculation finished without error. / {t5 - t0:.4f}sec ')
