#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 20 11:54:10 2021

@author: paulmueller


For each patient look for the afni directory
Then call for every electrode whereami and assign electrodes through this
"""

import pandas as pd
import numpy as np
import os
import glob
from iEEG_STC import get_approx_roi as gar


if __name__ == '__main__':
    # Path to the project where all patients are stored with their meta data
    path = 'PATH_TO_PROJECT'
    default_patient = 'pat958'
    # Cycle through all patients
    for p in glob.glob(f'{path}/pat*'):
        # Extract the patient ID
        patID = p.split('/')[-1]
        print(patID)
        # Get all electrodes of the patient
        elecs = pd.read_csv(f'{path}/{patID}/meta/{patID}_electrodes.csv')
        # Discard electordes which don't have coordinates
        elecs = elecs.loc[elecs['x'] != '-']
        elecs['x'] = elecs['x'].astype(float)
        elecs['y'] = elecs['y'].astype(float)
        elecs['z'] = elecs['z'].astype(float)
        # Change the coordinates from the EPILEPSIAE frame to standard MNI space
        elecs['x'] = 91 - elecs['x']
        elecs['y'] = elecs['y'] - 127
        elecs['z'] = elecs['z'] - 73
        # Build a new frame from this electrodes with the old parameters needed
        # but later also having the region of interest
        try:
            likely_zone = elecs.loc[:, ['name', 'moniker', 'x', 'y', 'z', 'kind']]
        # Same frame miss hte moniker. We add this here
        except KeyError:
            likely_zone = elecs.loc[:, ['name', 'x', 'y', 'z', 'kind']]
            likely_zone['moniker'] = likely_zone['name']
        # These are the new informations, the most likely zone and the electrodes distance to it
        likely_zone['zone'] = np.nan
        likely_zone['dist_to_zone'] = np.inf
        # Get the directory where the brain of a certain patient should be stored
        cwd = f'{path}/{patID}/meta/{patID}_afni'
        # Change to that directory. If it does not exist use pat958's brain
        try:
            os.chdir(cwd)
            save_path = f'{path}/{patID}/meta/{patID}_likely_zone.csv'
        except FileNotFoundError:
            print(f'Doing elec assignment on {default_patient} brain')
            cwd = f'{path}/{default_patient}/meta/{default_patient}_afni'
            os.chdir(cwd)
            save_path = f'{path}/{patID}/meta/{patID}_on_{default_patient}_likely_zone.csv'
        # Cylce through all electrodes
        for e_ind in elecs.index:
            # Get the most likely zone, its distance and the sub parcellation(only printed to console)
            lz, md, sub_parc = gar.assign_roi(elecs.loc[e_ind, 'x'], elecs.loc[e_ind, 'y'], elecs.loc[e_ind, 'z'],
                                              return_subparc=True, valid_atlases=['MNI_Glasser_HCP_v1.0'],
                                              max_dist=9.5, cwd=cwd)
            # Add it to the whole dataframe if a zone was found
            if lz is not None:
                likely_zone.loc[e_ind, 'zone'] = lz
                likely_zone.loc[e_ind, 'dist_to_zone'] = md
                print(elecs.loc[e_ind, 'name'], lz, md, sub_parc)
        # Save the frame
        likely_zone = likely_zone.sort_values('name')
        likely_zone.to_csv(save_path, index=False)
