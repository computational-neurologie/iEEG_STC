#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 10:03:59 2021
@author: paulmueller
Takes a configuration file as first argument and datafiles as the others to calculate autocorrelation functions and
crosscorrelation functions from the data. Then calculates lrtc and lrsc measures from these.
"""
import os
import socket
import sys
import time
from shutil import copy

from datetime import datetime
import numpy as np
import pandas as pd

t0 = time.time()

# Check on which machine you are and add the path to epilepsiae_space_time_resolved (estr)
host = socket.gethostname()
# We assume that you are on the BIHC. If this is not the case add your machine and the location below
estr_dir = '/data/gpfs-1/users/muellepm_c/scripts/iEEG_STC'
sys.path.insert(0, estr_dir)

# Importing on the cluster only works within a try statement for unkonwn reasonss
try:
    import iEEG_STC.utils as utils
    import iEEG_STC.additional_functions as a_f
    from iEEG_STC.reader import ECOGReader
except ModuleNotFoundError:
    raise Exception('ImportError. Could not import iEEG_STC')
else:
    print('Importing completed')


force = True  # Force calculation even if data exists already. This overwrites old data
save_acf = False  # Save the autocorrelation function. Not really necessary as acf calculation is very fast
calc_is_surr_test = False  # Shuffle the data if before calculating the autocorrelation function if True
calculate_only_psd = True  # Set to True to stop processing before the calculation of acf, cc, ...

# Path construction from the sys arguments
CONFIGFILE = sys.argv[1]
DATA_FILES = sys.argv[2:]
path_to_general = '/'.join(CONFIGFILE.split('/')[:-3])
# Read the configuration file
calc_parameters = a_f.read_config_file(CONFIGFILE)

# This is strongly discouraged
t1 = time.time()
print(f'0. Initialization took {t1-t0:.4f}sec')

for data_file in DATA_FILES:
    t1 = time.time()
    # Reads the .head file of the data
    data_object = ECOGReader(data_file)
    # Check if the calc_parameters file has the same patID as the
    try:
        if calc_parameters['patID'] != data_object.head_dict['patID']:
            print(f"Skipping {data_file} because config file patID {calc_parameters['patID']} " +
                  f"does not match data file patID {data_object.head_dict['patID']}")
            continue
    except KeyError:
        print(f"Skipping {data_file} because config file  misses the patID. Calculation without patID is deprecated")
        continue

    # Summarize parameters and print them
    calc_parameters['recordingID'] = data_object.head_dict['recordingID']
    print(f"patID: {data_object.head_dict['patID']}, recordingID: {data_object.head_dict['recordingID']}")
    print(calc_parameters)
    locations = a_f.data_file_locations_from_settings(calc_parameters, path_to_general)

    # Extract electrodes
    elecs = pd.read_csv(f"{locations['meta']}/{calc_parameters['patID']}_electrodes.csv", index_col=0)
    # Delete all non ecog electrodes, i.e., electrodes without coordinates
    elecs = elecs.loc[elecs['x'] != '-']
    # Read interictal events and delete them from the elecs
    #ievents = pd.read_csv(os.path.join(locations['meta'], f"{data_object.head_dict['patID']}_interictal_events.csv"),
    #                       index_col=0)
    # elecs = elecs.loc[~np.in1d(elecs.index, ievents['electrode'])]
    # Get the distances between the cannels
    dists = utils.get_channels_dists(elecs)

    # Construct the path to which processed data will be stored
    a_f.make_path(locations['processed'])

# =============================================================================
#  Make the vigilance index after Reed et al. 2018 
    if 'VI_Reed' in CONFIGFILE:
        VI_PATH = (locations['processed']+'/'+locations['settings'] +
                   f"_VI_Reed_{calc_parameters['recordingID']}.csv")
        print(f'VI under  {VI_PATH}')
        if (not os.path.isfile(VI_PATH)) or force:
            t1_1 = time.time()
            # Read the electrophyiscial data and preprocess it in default (downsample, notch, cut edges)
            try:
                data_object.default_data_read(path_to_quality=f"{locations['meta']}/" +
                                                              f"{calc_parameters['patID']}_segments_goodness.csv",
                                              include_electrodes=elecs.index,
                                              **calc_parameters)
            except Exception:
                print('Skipping file as there are no channels left to caclculate on')
                continue
            t2 = time.time()
            print(f'1. Data read and preprocessed. / {t2-t1_1:.4f}sec')
            try:
                copy(CONFIGFILE, locations['processed'])
            except (FileExistsError, OSError):
                pass
            print(f"Channels included: {data_object.head_dict['elec_names']}")
            vi = utils.get_vi_reed(data_object.data, fs=data_object.head_dict['sample_freq'], **calc_parameters)
            vi.index = vi.index + datetime.timestamp(data_object.head_dict['start_ts'])
            vi.to_csv(VI_PATH)
            t2 = time.time()
            del vi
            print(f'2. VI Reed calculated/read. / {t2-t1:.4f}sec')
            continue
        else:
            t2 = time.time()
            print(f'2. VI Reed already calculated. / {t2-t1:.4f}sec')

# =============================================================================
# Start calculating the ACF and LRTC measures
    else:
        t1_1 = time.time()
        # Create for each setting a folder
        a_f.make_path(locations['psd'])
        # Create a file_string for each band
        PSD_LOC = locations['psd']+'/'+locations['settings']
        psd_files = [PSD_LOC + f'_{B[0]:03d}_{B[1]:03d}' + '.csv' for B in calc_parameters['bands']]
        # Check whether a file already exists for this band
        psd_band_exist = [os.path.isfile(F) for F in psd_files]
        # If there are missing bands or the calculation is forced calculate the band powers
        # (multiple bands leads only to minor slow down. Slowest part is psd_welch)
        if force or not all(psd_band_exist):
            # Try to copy the CONFIGFILE to the psd and the processed folder for later reference
            try:
                copy(CONFIGFILE, locations['psd'])
                copy(CONFIGFILE, locations['processed'])
            except (FileExistsError, OSError):
                pass
            print(elecs.index)
            # Read the electrophyiscial data and preprocess it in default (downsample, notch, cut edges)
            try:
                data_object.default_data_read(path_to_quality=f"{locations['meta']}/" +
                                              f"{calc_parameters['patID']}_segments_goodness.csv",
                                              include_electrodes=elecs.index, **calc_parameters)
            except Exception:
                print('Skipping due to all channels bad')
                continue
            t2 = time.time()
            print(f'1. Data read and preprocessed. / {t2-t1_1:.4f}sec')

            # Calculate the power spectral density
            band_powers = utils.get_band_power_segments(data=data_object.data,  fs=data_object.head_dict['sample_freq'],
                                                        norm=False, **calc_parameters)
            # Parse the power spectral density into certain frequency bands
            for B, F in zip(calc_parameters['bands'], psd_files):
                print(B, F)
                # Build a seperate dataframe for each band
                tmp_bool = (band_powers['Band'] == str(B))
                tmp = band_powers.loc[tmp_bool]
                # Save the band power
                tmp.to_csv(F, index=None)
            t3 = time.time()
            print(f'2. PSD calculated. / {t3-t2:.4f}sec')
        # If all band powers exist, adjust the head_dict and read the band powers
        else:
            # Change the data_object head_dict as preprocessing was done but don't read data in the first place
            data_object.head_dict['num_samples'] /= (data_object.head_dict['sample_freq'] /
                                                     calc_parameters['down_sample_to'])
            data_object.head_dict['sample_freq'] = calc_parameters['down_sample_to']
            data_object.head_dict['num_samples'] -= (2 * data_object.head_dict['sample_freq'] *
                                                     calc_parameters['cut_sym_after_filter_in_sec'])

            data_object.head_dict['elec_names'] = data_object.head_dict['elec_names'][~np.in1d(data_object.head_dict['elec_names'], elecs.index)]
            data_object.head_dict['num_channels'] = len(data_object.head_dict['elec_names'])
            # Read power band by power band and put them all into one dataframe as if it was just calculated
            band_powers = pd.DataFrame()
            for f in psd_files:
                tmp = pd.read_csv(f)
                band_powers = band_powers.append(tmp, ignore_index=True)  # Read one band power
            band_powers = band_powers.loc[:, np.intersect1d(band_powers.columns,
                                                            np.append(['Band', 'Time'], elecs.index))]
            t3 = time.time()
            print(f'1. PSD read. / {t3-t1:.4f}sec')
        # Check if band powers (PSD) was found, raise an Exception if not
        if not isinstance(band_powers, pd.DataFrame):
            raise Exception('PSD not found')
        # Ignore the acf, cc and further measurement calculations if True
        if calculate_only_psd:
            continue
# ====== Calculate measures on the band power time series, e.g., acf/lrtc ====== #
        # Do calculations for each band seperately
        for band in set(band_powers['Band']):
            # Extract the specific band and adjust the dataframe to have time in the index and the channels in the cols
            tmp_psd = band_powers.loc[(band_powers['Band'] == band)].copy()
            tmp_psd.drop(columns='Band', inplace=True)
            tmp_psd.set_index('Time', drop=True, inplace=True)
            tmp_psd.sort_index(inplace=True)
            # Get the first time point of each window for which an acf will be calculated
            times = sorted(tmp_psd.index.unique())[::int((calc_parameters['acf_length'] *
                                                         (1-calc_parameters['acf_overlap'])) /
                                                         calc_parameters['band_power_window_length_in_secs'])]

            # Delete times for which the end of the acf would be outside of the time window
            times = times[:-int(np.ceil(calc_parameters['acf_overlap'])/(1-calc_parameters['acf_overlap']))]
            # Convert band tuple to band string (with leading zeros)
            band = band[1:-1].split(',')
            band = f'{int(band[0]):03d}_{int(band[1]):03d}'
            # Construct the save path where XXXX is the place holder to put in LRTC or CC later
            save_path = (locations['processed'] + '/'+locations['settings'] +
                         f"_{band}_XXXX_{calc_parameters['recordingID']}_noIED.csv")
            # Change the save path to account for surrogate data
            if calc_is_surr_test:
                save_path = save_path.replace('.csv', '_01surr.csv')
            # Set up the container for the LRTC
            LRTC_data = pd.DataFrame()
            LRTC_data.index.name = 'Time'
            CC_data = pd.DataFrame()
            CC_data.index.name = 'Time'
                
            for T in times:
                # Get the absolute time point
                current_t = datetime.timestamp(data_object.head_dict['start_ts'])+T
                # Extract the subframe of each segment
                bool_loc = np.logical_and(tmp_psd.index >= T, tmp_psd.index < T + calc_parameters['acf_length'])
                data_window = tmp_psd.loc[bool_loc]
                # Shuffle the data if surrogate calculations should be done
                if calc_is_surr_test:
                    data_window = utils.shuffle_all_channels(data_window)
                # Calculate the autocorrelation function
                acf = utils.estimated_autocorrelation(data_window)

                # Save the acf with
                if save_acf:
                    a_f.make_path(locations['processed'] + '/'+locations['settings'] + f"_{band}_acf")
                    acf.index = acf.index
                    acf.to_csv(locations['processed'] + '/' + locations['settings'] + f"_{band}_acf" + '/' +
                               locations['settings'] + f"_{band}_acf_{T}_{calc_parameters['recordingID']}.csv")
                    acf.index = acf.index
                # Calculate the measure (lrtc) here it is when the data crosses first time below the half maximum
                ind, hm = utils.hwhm(acf, hmfunc=lambda x: utils.hm_from_lag_x_med_3_2(x, x=1))

                # Save the data to DataFrames
                LRTC_data = LRTC_data.append(ind.rename(f'{current_t} {ind.name}'))
                LRTC_data = LRTC_data.append(hm.rename(f'{current_t} {hm.name}'))
                # Add another measure
                atLag1 = utils.lag_x(acf, x=1)
                LRTC_data = LRTC_data.append(atLag1.rename(f'{current_t} {atLag1.name}'))

                # Calculate crosscorrelation
                cc = utils.cc_by_binned(data_window, dists)
                cc.name = current_t
                CC_data = CC_data.append(cc)

            # Save the data together for all times
            print(f"Saving LRTC/CC to {save_path.replace('XXXX','LRTC/CC')}")
            LRTC_data.to_csv(save_path.replace('XXXX', 'LRTC'))
            CC_data.to_csv(save_path.replace('XXXX', 'CC'))

        t4 = time.time()
        print(f'4. ACF calculated. / {t4-t3:.4f}sec')
t5 = time.time()
print(f'Calculation finished without error. / {t5-t0:.4f}sec ')
