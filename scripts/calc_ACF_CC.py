#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 14:51:53 2021

@author: paulmueller

Calculates autocorrelation/crosscorrelation functions for a patient and writes them out for sws,aed,roi combinations.
OUTFORMAT for each ROI (TOTAL,MT,LIP,LPFC,OFC,ACC)
total,SWS,nonSWS,low_AED,high_AED,SWS_low_AED,nonSWS_low_AED,SWS_high_AED,nonSWS_high_AED
->9*6 = 54 columns with 120*8 entries
This script does not calculate power spectral densities or vigilance indices

"""
import os
import socket
import sys
import time
import glob
import datetime
import numpy as np
import pandas as pd
import ast
t0 = time.time()
# Raise all numpy warnings as errors for security of results
np.seterr(all='raise')
# Check on which machine you are and add the path to epilepsiae_space_time_resolved (estr)
host = socket.gethostname()
# We assume that you are on the BIHC. If this is not the case add your machine and the location below
estr_dir = '/data/gpfs-1/users/muellepm_c/scripts/iEEG_STC'
sys.path.insert(0, estr_dir)

# Importing on the cluster only works within a try statement for unkonwn reasonss
try:
    import iEEG_STC.utils as utils
    import iEEG_STC.additional_functions as a_f
    from iEEG_STC.reader import ECOGReader
except ModuleNotFoundError:
    raise Exception('ImportError. Could not import iEEG_STC')
else:
    print('Importing completed')

#
calc_acf = False
calc_cc = False
calc_ami = True

# Script accepts exactly one CONFIGFILE which must include a patID
patID = sys.argv[1]
print(sys.argv[1:])
bands = (ast.literal_eval(sys.argv[2]), ast.literal_eval(sys.argv[3]))
path_to_general = '/data/gpfs-1/users/muellepm_c/work/LRTC_project' 
CONFIGFILE = f'{path_to_general}/{patID}/meta/psd_0.125_full_length_5.0.config'
# CONFIGFILE = '/Users/paulmueller/BIH_Neuro/LRTC_project/pat958/meta/psd_0.125_full_length_5.0.config'
# Read the parameters form the file
calc_parameters = a_f.read_config_file(CONFIGFILE)
calc_parameters['bands'] = (bands,)
print(calc_parameters)
#patID = calc_parameters['patID']  # For convenience stored in varibale
# Construct the project general path
#path_to_general = CONFIGFILE.split(patID)[0]
# Path to where the head files of the raw data are stored
path_to_raw = '/data/gpfs-1/groups/ag_meisel/work/epilepsiae_data'
#'/Users/paulmueller/BIH_Neuro/LRTC_project' # 'PATH_TO_RAW_DATA'
# Construct the paths to all the other data
locations = a_f.data_file_locations_from_settings(calc_parameters, path_to_general)
# Set to surrogate
calc_is_surr = False#'_surr01'  # False or String to name the surrogate data
# Check how many bands were found. Exactly one is expected. More than one are discarded.
if len(calc_parameters['bands']) == 0:
    raise Exception('No band found in config file')
elif len(calc_parameters['bands']) > 1:
    print(f"Warning.  Multiple bands were found.This behaviour has to  be tested still")
    # calc_parameters['bands'] = calc_parameters['bands'][:1]

# Read in additional meta parameters. First the electrode data with region of interest
elecs = pd.read_csv(os.path.join(locations['meta'], f'{patID}_likely_zone_b.csv'), index_col=0)
# Read interictal events and delete them from the elecs
# ievents = pd.read_csv(os.path.join(locations['meta'], f"{patID}_interictal_events.csv"), index_col=0)
# elecs = elecs.loc[~np.in1d(elecs.index, ievents['electrode'])]
# and than the state classifier for SWS, and AED load
classifier = pd.read_csv(os.path.join(locations['meta'], f'{patID}_classifier_subs_dropped_with_overlap.csv'), index_col=0)
if np.any(pd.isnull(classifier)):
    raise ValueError('Something in the classifier is not correct')
# Read in the quality frame
quality = pd.read_csv(os.path.join(locations['meta'], f"{patID}_segments_goodness.csv"), index_col=0)

# Labels for regions of interest
rois = ['Total', 'MT', 'LIP', 'LPFC', 'OFC', 'ACC']
# Labels for different states
states = ['Total', 'SWS', 'nonSWS', 'low_AED', 'high_AED', 'SWS_low_AED', 'SWS_high_AED', 'nonSWS_low_AED', 'nonSWS_high_AED']
# Bins for the crosscorrelation
bins = np.arange(176)

# Find all head files of the to read in the meta data
head_files = sorted(glob.glob(f'{path_to_raw}/{patID}/*.head'))
if len(head_files) == 0:
    raise FileNotFoundError(f'There are no head files in {path_to_raw}/{patID}')

# Cycle through all bands
for band in calc_parameters['bands']:
    t01 = time.time()
    # Containers for the output of acf and crosscorrelation
    ACF_out = pd.DataFrame(columns=pd.MultiIndex.from_product([rois, states],
                                                              names=['ROI', 'State']))
    CC_out = pd.DataFrame(columns=pd.Index(states, name='State'), index=pd.Index(bins, name='Dist'))

    # For tracking how many time points are used per patient. Will only be printed to the console
    included_points = 0
    excluded_points = 0
    valid_time_points = []
    # Construct the general save path
    band_str = f"_{band[0]:03d}_{band[1]:03d}"
    save_path = os.path.join(locations['processed'], locations['settings'] + f'{band_str}_XXXX')
    # Add the surrogate addition if necessary
    if calc_is_surr:
        save_path += '_' + calc_is_surr
    # Containers for the acf and the crosscorrelation function which will later be averaged according to states
    acfs = pd.DataFrame(index=pd.MultiIndex(levels=[[], []], codes=[[], []], names=['Time', 'Lag']))
    crosses = pd.DataFrame(index=pd.Index([], name='Time'), columns=pd.Index(bins, name='Dist'))
    ami = pd.DataFrame(index=pd.MultiIndex(levels=[[], []], codes=[[], []], names=['Time', 'Lag']))
    i = 0
    # Cycle through all head files to extract of the patient to get all recordings
    for hf in head_files:
        print(hf)
        t1 = time.time()
        recID = hf.split('/')[-1].replace('.head', '')
        # Delete the electrodes from reacording which are probably bad
        rec_elecs = elecs.loc[np.intersect1d(quality.loc[recID].index[quality.loc[recID].values.astype(bool)], elecs.index)]
        if len(rec_elecs) == 0:
            print(f'skipping {recID} as no electrodes are good')
            continue
        # Read the head of the recording. Necessary only for timing issues
        tmp_head = ECOGReader(hf)
        start_ts = tmp_head.head_dict['start_ts']
        # Get the path to the power spectral density/band powers
        psd_path = os.path.join(path_to_general, patID, 'data', recID, locations['settings'],
                                locations['settings'] + band_str + '.csv')
        # Read the band powers and drop the band name
        psd = pd.read_csv(psd_path, index_col=1)
        psd.drop('Band', inplace=True, axis=1)
        # Exclude electrodes which are not good data quality wise
        
        psd = psd.loc[:, np.in1d(psd.columns, rec_elecs.index)]
        # Calculate the distances between the remaining electrodes
        dists = utils.get_channels_dists(rec_elecs)
        # Go through all acf/cc windows (defined by 'acf_length') of this data bunch
        times = np.arange(calc_parameters['cut_sym_after_filter_in_sec'],
                          tmp_head.head_dict['duration_in_sec']-calc_parameters['cut_sym_after_filter_in_sec'],
                          calc_parameters['acf_length']*(1-calc_parameters['acf_overlap']))
        times = times[:-int((calc_parameters['acf_overlap'])/(1-calc_parameters['acf_overlap']))]
        for i in times:
            if isinstance(start_ts, datetime.datetime):
                start_ts = start_ts.timestamp()
            if start_ts+i not in classifier.index:
                # Count if a time point was excluded due to not being in classifier (Probably an issue of quality)
                excluded_points += 1
                print(f'{start_ts+i} excluded due to classifier')
            else:
                # If the time point is ok save that it is included
                included_points += 1
                valid_time_points.append(start_ts+i)
                # Give the psd window on which acf and cc will be calculated
                data_window = psd.loc[np.logical_and(psd.index >= i, psd.index < i + calc_parameters['acf_length'])]
                # Shuffle if a surrogate run should be done
                if calc_is_surr:
                    data_window = utils.shuffle_all_channels(data_window)
                if calc_acf:
                    # Calcualte autocorrelation function
                    tmp_acf = utils.estimated_autocorrelation(data_window)
                    tmp_acf.index -= i
                    tmp_acf.index = pd.MultiIndex.from_product([[start_ts+i], tmp_acf.index], names=['Time', 'Lag'])
                    acfs = pd.concat([acfs, tmp_acf])
                # acfs = acfs.append(tmp_acf)
                if calc_cc:
                    # Calculate crosscorrelation function
                    tmp_cross = utils.cc_by_binned(data_window, dists, bins=bins)
                    tmp_cross.name = start_ts + i
                    tmp_cross.index.name = 'Dist'
                    crosses = pd.concat([crosses, tmp_cross.to_frame().T])
                # crosses = crosses.append(tmp_cross)
                if calc_ami:
                    # Calculate automutual infromation
                    tmp_ami = data_window.apply(lambda x:utils.auto_MI_1d(x.to_numpy(), normalize=True))
                    tmp_ami.index -= i
                    tmp_ami.index = pd.MultiIndex.from_product([[start_ts + i], tmp_ami.index], names=['Time', 'Lag'])
                    ami = pd.concat([ami, tmp_ami])


        print(f'Time per psd file {time.time()-t1:.4f}\n')
    tmp_classifier = classifier.loc[valid_time_points]
    state_classifiers = {'Total': tmp_classifier.index,
                     'SWS': tmp_classifier.index[tmp_classifier['sws']],
                     'nonSWS': tmp_classifier.index[~tmp_classifier['sws']],
                     'low_AED': tmp_classifier.index[tmp_classifier['low_AED']],
                     'high_AED': tmp_classifier.index[~tmp_classifier['low_AED']],
                     'SWS_low_AED': tmp_classifier.index[np.logical_and(tmp_classifier['sws'], tmp_classifier['low_AED'])],
                     'SWS_high_AED': tmp_classifier.index[np.logical_and(tmp_classifier['sws'], ~tmp_classifier['low_AED'])],
                     'nonSWS_low_AED': tmp_classifier.index[np.logical_and(~tmp_classifier['sws'], tmp_classifier['low_AED'])],
                     'nonSWS_high_AED': tmp_classifier.index[np.logical_and(~tmp_classifier['sws'], ~tmp_classifier['low_AED'])]
                     }

    #  We drop electrodes from the acfwhich have significantely less time points
    # than the others as they are most likely imbalanced
    if calc_acf:
        acfs.dropna(axis='columns', inplace=True, thresh=6*len(acfs)//8)
    if calc_ami:
        ami.dropna(axis='columns', inplace=True, thresh=6*len(ami)//8)

    print('Electrodes were rejected due to not enough acf available' +
          f' {elecs.loc[~np.in1d(elecs.index, acfs.columns)].index}')
    if calc_acf:
        # Build the final set of acf electrodes
        elecs = elecs.loc[np.in1d(elecs.index, acfs.columns)]
    elif calc_ami:
        elecs = elecs.loc[np.in1d(elecs.index, ami.columns)]

    for i, s in enumerate(states):
        s_class = state_classifiers[s]
        # Temporarily save the acf of this state only
        if calc_acf:
            tmp_acf = acfs.loc[np.in1d(acfs.index.get_level_values(0), s_class)].groupby(level=1).mean()
            # For each Region of interest add for the acf for each state
            for r in rois:
                if r == 'Total':
                    ACF_out[(r, s)] = tmp_acf.mean(axis=1)
                else:
                    ACF_out[(r, s)] = tmp_acf.loc[:, elecs.index[elecs['zone'] == r]].mean(axis=1)
        # Also do a pairwise comparison
        for s2 in states[(i+1):]:
            s_class_2 = state_classifiers[s2]
            # Subsample according to the one with the lower amount
            smaller_class = np.min([len(s_class), len(s_class_2)])
            print(smaller_class, s, s2)
            s_class_1 = np.random.choice(s_class, smaller_class)
            s_class_1.sort()
            s_class_2 = np.random.choice(s_class_2, smaller_class)
            s_class_2.sort()

            compare_acf_out = pd.DataFrame()
            compare_cc_out = pd.DataFrame()
            compare_ami_out = pd.DataFrame()
            if calc_acf:
                tmp_acf = acfs.loc[np.in1d(acfs.index.get_level_values(0), s_class_1)].groupby(level=1).mean()
                tmp_acf2 = acfs.loc[np.in1d(acfs.index.get_level_values(0), s_class_2)].groupby(level=1).mean()
            # Also automutual information
            if calc_ami:
                tmp_ami1 = ami.loc[np.in1d(ami.index.get_level_values(0), s_class_1)].groupby(level=1).mean()
                tmp_ami2 = ami.loc[np.in1d(ami.index.get_level_values(0), s_class_2)].groupby(level=1).mean()
            for r in rois:
                if r == 'Total':
                    if calc_acf:
                        compare_acf_out[(r, s)] = tmp_acf.mean(axis=1)
                        compare_acf_out[(r, s2)] = tmp_acf2.mean(axis=1)
                    if calc_ami:
                        compare_ami_out[(r, s)] = tmp_ami1.mean(axis=1)
                        compare_ami_out[(r, s2)] = tmp_ami2.mean(axis=1)

                else:
                    if calc_acf:
                        compare_acf_out[(r, s)] = tmp_acf.loc[:, elecs.index[elecs['zone'] == r]].mean(axis=1)
                        compare_acf_out[(r, s2)] = tmp_acf2.loc[:, elecs.index[elecs['zone'] == r]].mean(axis=1)
                    if calc_ami:
                        compare_ami_out[(r, s)] = tmp_ami1.loc[:, elecs.index[elecs['zone'] == r]].mean(axis=1)
                        compare_ami_out[(r, s2)] = tmp_ami2.loc[:, elecs.index[elecs['zone'] == r]].mean(axis=1)
            if calc_cc:
                compare_cc_out[s] = crosses.loc[np.in1d(crosses.index.get_level_values(0), s_class_1)].mean(axis=0)
                compare_cc_out[s2] = crosses.loc[np.in1d(crosses.index.get_level_values(0), s_class_2)].mean(axis=0)
            # print((~pd.isnull(compare_cc_out)).any(axis=0))
            if calc_acf:
                compare_acf_out.to_pickle(save_path.replace('XXXX', f'{s}VS{s2}_acf_nosubs') + '.pkl')
            if calc_cc:
                compare_cc_out.to_pickle(save_path.replace('XXXX', f'{s}VS{s2}_cc_nosubs') + '.pkl')
            if calc_ami:
                compare_ami_out.to_pickle(save_path.replace('XXXX', f'{s}VS{s2}_ami_nosubs') + '.pkl')
        #  For each state add the averaged cross correlation to the output
        if calc_cc:
            CC_out[s] = crosses.loc[np.in1d(crosses.index.get_level_values(0), s_class)].mean(axis=0)

    # Print how many and which time points were included in the averaging
    print(f'{included_points} were included and {excluded_points} excluded')
    print(np.setdiff1d(valid_time_points, tmp_classifier.index))
    # Save the data
    # print(ACF_out)
    if calc_acf:
        ACF_out.to_pickle(save_path.replace('XXXX', 'averaged_acf_nosubs') + '.pkl')
    if calc_cc:
        CC_out.to_pickle(save_path.replace('XXXX', 'averaged_cc_nosubs') + '.pkl')
    print(f'{band} calculated. {time.time()-t01:.4}')
print(f'{patID} calculated without any serious errors. Total time {time.time()-t0:.4f}')
