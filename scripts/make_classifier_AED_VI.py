#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 11:58:30 2021

@author: paulmueller

This script reads all header files of a patient, looks for corresponing VI files.
It creates a new DataFrame which has all VI values  but excludes seizures already.
The DataFrame has the columns:
    index(timestamp),VI,date,low_AED(bool),sws(bool)
"""

import sys
import socket
import pandas as pd
import datetime
import glob
import os
import warnings
import numpy as np
# Check on which machine you are and add the path to epilepsiae_space_time_resolved (estr)
host = socket.gethostname()
# We assume that you are on the BIHC. If this is not the case add your machine and the location below
estr_dir = '/data/gpfs-1/users/muellepm_c/scripts/iEEG_STC'
sys.path.insert(0, estr_dir)

# Importing on the cluster only works within a try statement for unkonwn reasonss
try:
    import iEEG_STC.utils as utils
    import iEEG_STC.additional_functions as a_f
    from iEEG_STC.reader import ECOGReader
except ModuleNotFoundError:
    raise Exception('ImportError. Could not import iEEG_STC')
else:
    print('Importing completed')


error_mode = 1  # 0 raise non match errors, 1 warn only, all  other ignore
# Path where all patients processed data lies (and meta)
path_to_general = '/data/gpfs-1/users/muellepm_c/work/LRTC_project'
# path_to_general = '/Users/paulmueller/BIH_Neuro/LRTC_project'  # '<PATH_TO_PROJECT>'
# Path to the raw data. If data is not found there path_to_general will be checked as well
path_to_raw_data = '/data/gpfs-1/groups/ag_meisel/work/epilepsiae_data'
# path_to_raw_data = '/Users/paulmueller/BIH_Neuro/LRTC_project'
# File with the configuration of the run. It will be search in the patients meta for the specific
CONFIGFILE = 'psd_0.125_full_length_5.0.config'
# Drop that many seconds before seizure onset and offset
drop_around_seizure = 60*10
# Drop that many seconds before subclinical seizure onset and offset
drop_around_subclinical = 60*2
# Get all patient ids
pats = [g.split('/')[-1] for g in glob.glob(f'{path_to_general}/pat*')]
# Cycle through all patients
for patID in pats:
    print(patID)
    # Read the low and high AED days for the pat and convert to datetime.date
    AED_days = pd.read_csv(os.path.join(path_to_general, 'all_aed_in_days.csv'), index_col=0).loc[patID].squeeze()
    AED_days = AED_days.transform(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d').date())
    # Read the configuration file
    config = a_f.read_config_file(f'{path_to_general}/{patID}/meta/{CONFIGFILE}')

    # Find all header files of the data
    head_files = sorted(glob.glob(f'{path_to_raw_data}/{patID}/*.head'))
    # If no head files where found there check if they are in path_to_general instead
    if len(head_files) == 0:
        head_files = sorted(glob.glob(f'{path_to_general}/{patID}/data/*/*.head'))
    if len(head_files) == 0:
        raise FileNotFoundError('There are no head files in the commonly known locations.')
    # Read the header files to construct the timestamps for which we should have ACFs and also get the recording IDs
    recording_IDs = []
    valid_time_points = np.array([])
    for hf in head_files:
        # Read the header file
        tmp = ECOGReader(hf)
        # Add the recording ID
        recording_IDs.append(tmp.head_dict['recordingID'])
        # Get the starting timestampe
        start_ts = tmp.head_dict['start_ts']
        # Construct all timestamps for which we should have acfs
        times = np.arange(config['cut_sym_after_filter_in_sec'],
                                      tmp.head_dict['duration_in_sec']-config['cut_sym_after_filter_in_sec'],
                                      config['acf_length']*(1-config['acf_overlap']))
        times = times[:-int((config['acf_overlap'])/(1-config['acf_overlap']))]
        times += start_ts.timestamp()
        valid_time_points = np.append(valid_time_points, times)
    valid_time_points.sort()
    
    # Find VI files
    # vi_paths = []
    classifier = pd.Series(dtype=float, name='VI')
    for r in recording_IDs:
        # Construct the path of vigilance file
        tmp_path = os.path.join(path_to_general, patID, 'processed',
                                '30_1_hanning_0.5_constant_256_60_True',
                                f'30_1_hanning_0.5_constant_256_60_True_VI_Reed_corrected_{r}.csv')
        # Read all VI files into one Series which will become the classifier
        if os.path.isfile(tmp_path):
            tmp = pd.read_csv(tmp_path, index_col=0).squeeze(axis=1)
            tmp.name = 'VI'
            if any(pd.isnull(tmp.values)):
                print(r)
                print(tmp)
                tmp = tmp.dropna()
                print(any(pd.isnull(tmp.values)))
            classifier = pd.concat([classifier, tmp])
        else:
            if error_mode == 0:
                raise FileNotFoundError(f'{tmp_path} not found')
            elif error_mode == 1:
                warnings.warn(f'{tmp_path} not found')


    # Align VI to timestamps defined from header files. min_vals is constructed from the acf_length and the vi length
    # which is 30 seconds
    classifier = utils.align_all_in_delta_t(classifier.to_frame(), times=valid_time_points,
                                            delta_t=config['acf_length'], min_vals=config['acf_length']//30)
        
    # Exclude seizures
    seizures = pd.read_csv(f'{path_to_general}/{patID}/meta/{patID}_seizures.csv')
    print(len(classifier))
    classifier = utils.drop_seizures(classifier, seizures, delta_t=drop_around_seizure)
    print(len(classifier))
    # Drop around subclincial seizures
    subclinical = pd.read_csv(f'{path_to_general}/{patID}/meta/{patID}_subseizures.csv', index_col=0)
    subclinical = subclinical.rename({'onset': 'onset_ts', 'offset': 'offset_ts'}, axis=1)
    subclinical = subclinical.apply(pd.to_datetime).applymap(lambda x: x.timestamp())
    classifier = utils.drop_seizures(classifier, subclinical, delta_t=drop_around_subclinical)
    print(len(classifier))
    # Convert timestamps to datetime.date and check if they match the low and high AED days exactly
    classifier = utils.condense_dates(classifier)
    if not set(classifier['date']) == set(AED_days.values):
        raise Exception(f"""AED days are not the same as VI data day. 
AED days: {set(AED_days.values)}
VI days:  {set(classifier['date'])}""")

    # Add low AED day as column to VI (True if low AED, else is high AED)
    classifier['low_AED'] = False
    classifier.loc[classifier['date'] == AED_days['low_AED'], 'low_AED'] = True
    
    # For both high and low AED day construct the where the SWS is seperatly and append the sws column (True if sws)
    # SWS is defined as VI > VI.mean() + 1*VI.std() see Reed et al., 2017
    classifier['sws'] = False
    # For the low AED day
    where_low_aed_sws = classifier.loc[classifier['low_AED'], 'VI'] > (classifier.loc[classifier['low_AED'],
                                                                                      'VI'].mean() +
                                                                       classifier.loc[classifier['low_AED'],
                                                                                      'VI'].std())
    classifier.loc[classifier['low_AED'], 'sws'] = where_low_aed_sws
    # For the high AED day
    where_high_aed_sws = classifier.loc[~classifier['low_AED'], 'VI'] > (classifier.loc[~classifier['low_AED'],
                                                                                        'VI'].mean() +
                                                                         classifier.loc[~classifier['low_AED'],
                                                                                        'VI'].std())
    classifier.loc[~classifier['low_AED'], 'sws'] = where_high_aed_sws
    
    # Save to meta
    classifier.to_csv(f'{path_to_general}/{patID}/meta/{patID}_classifier_subs_dropped_with_overlap.csv')
    print(f'{patID}_classifier.csv written')
