#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 12:18:35 2021

@author: paulmueller
"""

import pandas as pd
from datetime import datetime, timedelta, time
import time
import numpy as np
import os


# =============================
# Extract electrode data from txt
# =============================================================================
def electrodes_convert_txt_to_csv(path):
    """Read elecs.txt file in path and converts it to a csv file"""
    pat_location_identifier = path.split('/')[-2]  # Get path to patient files
    electrode_setup_file = path+'/elecs.txt'
    esave = path + f'/{pat_location_identifier}_electrodes.csv'  # Save the data as csv here
    # Make sure to not overwrite files. You should delete them first
    if os.path.isfile(esave):
        print(f'{esave} already exists. Writing aborted. Move or delete the old file first.')
        return None
    electrodes = pd.DataFrame()
    kind = None    
    with open(electrode_setup_file, 'r') as ef:
        # Read first line to get patient id
        line = ef.readline()
        if 'patient' in line:
            pat_id = line.split('FR_')[-1].split(' ')[0]
            # Raise error if the path of the file does not match with the patient itself
            if pat_id not in pat_location_identifier:
                raise Exception(f'Wrong txt file for path: {pat_location_identifier} / {pat_id}')
        # Extrect the informations on the electrodes line by line
        for line in ef.readlines():
            attributes = line.split('\t')
            attributes[-1] = attributes[-1][:-1]
            # Make the header for the columns in the output
            if 'electrode_array' in attributes:
                attributes.append('kind')
                attributes.remove('electrode_array')
                electrodes = pd.DataFrame(columns=attributes)
                continue
            # Get the kind of electrodes (grid, strip, depth, eeg ...)
            if pat_id in line:
                kind = line.split('(')[-1].split(')')[0].replace(' ', '')
                continue
            # Skipp lines which have not enough attributes
            if len(attributes) < 2:
                continue
            # Acutally append all information of one electrode
            else:
                attributes.append(kind)
                electrodes = electrodes.append(pd.Series(attributes, index=electrodes.columns), ignore_index=True)
    # Delete parenthesis
    electrodes.loc[:, 'moniker'] = electrodes.loc[:, 'moniker'].apply(lambda x: x.replace('(', '').replace(')', ''))
    electrodes.set_index('moniker', drop=True, inplace=True)
    # Only ECOG electrodes will not have x == '-'
    subset = electrodes['x'] != '-'
    # Delete electrodes which have the same name because this must be some error
    electrodes = electrodes.loc[subset].loc[~electrodes.loc[subset].duplicated(keep=False, subset=['x', 'y', 'z'])]
    # Catching pat922 which has another format
    if 'name' not in electrodes.columns:
        print(f'Adding name columns for {pat_id}')
 
        electrodes['name'] = electrodes.index
    # Save the electrode data
    electrodes.to_csv(esave)


# =============================================================================
# Extract the AED dosage
# =============================================================================
def convert_to_timestamp(time_str):
    """Convert a time formated string into a time stamp"""
    try:
        return time.mktime(datetime.strptime(time_str, "%B %d, %Y").timetuple())
    except ValueError:  # September is not standard
        time_str = time_str.replace('Sept.', 'Sep.')
        return time.mktime(datetime.strptime(time_str, "%b. %d, %Y").timetuple())


def try_convert_to_mg(load_str):
    """
    If the convert to mg fails it returns the load_str as given.
    You should than check if the data is in 'Value Unit' format with Unit either mg or g
    """
    if pd.isna(load_str):
        return load_str
    else:
        splitted = load_str.split(' ')
        if len(splitted) == 2:
            # Determine unit (mg or g)
            if splitted[1] == 'mg':
                return float(splitted[0])
            elif splitted[1] == 'g':
                return 1000*float(splitted[0])
            else:
                return load_str
        else:
            return load_str


def get_aed_loads_in_mg_ddd(in_aed):
    """Take aeds as dataframe and add mg and defined daily dose (ddd) and structure by aed"""
    # Only if ever the format changes. So far I did not entcounter enddates
    if all(pd.isna(in_aed['enddate'])) or all(in_aed['enddate'] == 0):
        print('Enddates were found. This script should be revised to allow for this.')
    # Read the defined daily doses lookup table
    daily_dose = pd.read_csv(f"{__file__.split('/make_e')[0]}/daily_doses.csv")
    # New frame with aed as columns and dates as index
    out_aed = pd.DataFrame(columns=set(in_aed['medicament']), index=sorted(set(in_aed['startdate'])))
    # Convert time to timestamp as new column
    out_aed.insert(0, column='timestamp', value=[convert_to_timestamp(x) for x in out_aed.index])
    # Convert aed load to mg
    for ind, row in in_aed.iterrows():
        out_aed.loc[row['startdate'], row['medicament']] = try_convert_to_mg(row['dosage'])
    # For each aed add a column with its values in ddd
    for C in out_aed.columns:
        if C == 'timestamp':
            continue
        try:
            out_aed.loc[:, f'{C}/DDD'] = out_aed.loc[:, C].astype(float)/float(daily_dose.loc[0, C])
        except KeyError:
            raise Exception(f'Missing DDD for {C}')
    # Sort by administration
    out_aed.sort_values('timestamp', inplace=True)
    out_aed.index = [datetime.fromtimestamp(t) for t in out_aed['timestamp']]
    return out_aed


def get_sum_ddd(aed_frame):
    """
    Sum all ddd for all aeds to one overall ddd
    """
    sum_ddd = pd.Series(index=aed_frame.index, name='sumDDD', dtype=float)
    # Check in which columns ddds are saved
    ddd_bool = np.array([True if 'DDD' in x else False for x in aed_frame.columns])
    for i, row in aed_frame.iterrows():
        sum_ddd.loc[i] = np.sum(row.loc[ddd_bool])
    sum_ddd.index.name = 'startdate'
    return sum_ddd


def drugs_to_continuous_time(drugs_frame, recording_window_frame):
    """ Construct a dataframe with drug values for every day in the recording window. Fills in zeros where needed or
    repeats values where necessary. This is due to the annoying convention to only state drugs
    when something was changed. """
    # Construct the full range of the recording for each drug as an unfilled dataframe
    end = datetime.strptime(recording_window_frame.loc['enddate'], '%Y-%m-%d')
    start_t = datetime.strptime(recording_window_frame.loc['startdate'], '%Y-%m-%d')
    out_frame = pd.DataFrame(columns=drugs_frame.columns,
                             index=[start_t+timedelta(days=dt) for dt in range((end-start_t).days+1)])
    # Convert the dates to timestamps
    out_frame['timestamp'] = [datetime.timestamp(i) for i in out_frame.index]
    # Fill in the existing values of the drugs values
    for col in drugs_frame.columns:
        for ind in drugs_frame[col].index:
            out_frame.loc[ind, col] = drugs_frame.loc[ind, col]

    # Fill in the missing dates with drug values being same as the next earlier
    for col in out_frame.columns:
        for ind in out_frame.index:
            if pd.isnull(out_frame.loc[ind, col]):  # isnull checks for nan values (0 is not detected which is intended)

                # Check if there is a previous drug value, starting with one day before
                t = ind - timedelta(days=1)
                while True:

                    if not np.any(pd.isnull(out_frame)):
                        break
                    if t < np.min(drugs_frame.index):
                        out_frame.loc[ind, col] = 0
                        print(f'Drug concentration for {col}, {ind} could not be found and is assumed to be 0.')
                        break
                    elif t in drugs_frame.index:
                        if not pd.isnull(drugs_frame.loc[t, col]):
                            out_frame.loc[ind, col] = drugs_frame.loc[t, col]
                            break
                    t -= timedelta(days=1)
            else:
                continue
    out_frame.sort_values('timestamp', inplace=True)
    return out_frame


def aed_convert_txt_to_csv(path, recording_window_frame=None):
    """Take the path to an aed.txt file and transform it to csv file with aed loads in mg and per ddd.
    If recording_window_frame is given also fill all dates within the recording correctly."""

    pat_location_identifier = path.split('/')[-2]
    drug_file = path + '/drugs.txt'
    drug_save = path + f'/{pat_location_identifier}_AED.csv'
    if os.path.isfile(drug_save):
        print(f'{drug_save} already exists. Writing aborted. Move or delete the old file first.')
        return None
    drug_flag = False  # Will be set to True when the drugs come in the file
    with open(drug_file, 'r') as DF:
        line = DF.readline()
        # Check if patient in file matches the file name
        if 'patient' in line:
            pat_id = line.split('FR_')[-1].split(' ')[0]
            if pat_id not in pat_location_identifier:
                raise Exception(f'Wrong txt file for path: {pat_location_identifier} / {pat_id}')
        # Read the dates line by line
        for line in DF.readlines():
            # Set drug_flag to True if the header starting with start_date is found
            if line.startswith('startdate'):
                drug_flag = True
                line = line.split('\t')
                line[-1] = line[-1][:-1]
                #  Create the drugs dataframe with its header
                drugs = pd.DataFrame(columns=line)
                continue
            # Read a date and all its according drugs
            if drug_flag:
                # Check if the number of drugs matches with the number in the header header
                if len(line.split('\t')) != len(drugs.columns):
                    drug_flag = False
                    continue
                # Add date with drugs to the DataFrame drugs
                line = line.split('\t')
                line[-1] = line[-1][:-1]
                df = pd.DataFrame([line], columns=drugs.columns)
                drugs = drugs.append(df)
                continue
    # Convert the strings in drugs to float values for the load in mg and add a per ddd column
    drugs = get_aed_loads_in_mg_ddd(drugs)
    # Fill in all dates in the recording_window if it is given
    if recording_window_frame is not None:
        drugs = drugs_to_continuous_time(drugs, recording_window_frame)
    # Add a column given the summed ddd per day
    drugs = drugs.join(get_sum_ddd(drugs))
    # Save the drugs to a csv file
    drugs.to_csv(drug_save)


# =============================================================================
# Extract the seizure times   
# =============================================================================
def seizures_convert_txt_to_csv(path):
    """Read in the file with the seizures and converts it to a csv file full of datetime objects"""
    seizure_file = path + '/seizures.txt'
    pat_location_identifier = path.split('/')[-2]
    seizure_save = path+f'/{pat_location_identifier}_seizures.csv'
    # Check if the file *seizures.csv file already exists and abort writing if so
    if os.path.isfile(seizure_save):
        print(f'{seizure_save} already exists. Writing aborted. Move or delete the old file first.')
        return None
    # Extract the seizrues from the file by reading it line by line
    seizures = pd.DataFrame(columns=['onset_ts', 'offset_ts'])
    with open(seizure_file, 'r') as SF:
        line = SF.readline()
        if 'patient' in line:
            pat_id = line.split('FR_')[-1].split(' ')[0].replace('\n', '')
            # Check if pat_id in the file matches the one in its name
            if pat_id not in pat_location_identifier:
                raise Exception(f'Wrong txt file for path: {pat_location_identifier} / {pat_id}')
        for line in SF.readlines():
            if line.startswith('#'):
                continue
            elif line == '\n':
                continue
            # Only read lines which do not start with # or are empty
            else:
                splitted_line = line.split('\t')
                # Ignore lines which have not at least an start and enddate
                if len(splitted_line) < 2:
                    continue
                # We discard the ms precission and convert strings to datetime objects
                onset = datetime.strptime(
                            splitted_line[0].split('.')[0], "%Y-%m-%d %H:%M:%S")
                onset = (time.mktime(onset.timetuple()))
                offset = datetime.strptime(
                            splitted_line[1].split('.')[0], "%Y-%m-%d %H:%M:%S")
                offset = (time.mktime(offset.timetuple()))
                # Append the seizure the seizure on and offset to seizrues
                seiz = pd.DataFrame([[onset, offset]], columns=['onset_ts', 'offset_ts'])
                seizures = pd.concat([seizures, seiz])
                 #   seizures.append(seiz)
    # Save the seizures to a csv file
    tmp = pd.read_csv(seizure_file.replace('.txt', '_meta.txt'), delimiter='\t')

    tmp['onset'] = tmp['onset'].apply(lambda x: datetime.strptime(':'.join(
                                      '.'.join(x.split('.')[:-1]).split(':')[1:]),
                                                                  " %d.%m.'%y %H:%M:%S").timestamp())
    # with open(seizure_file.replace('.txt', '_meta.txt'), 'r') as SFM:
    #     s_meta = SFM.readline()
    seizures = pd.merge(seizures, tmp, left_on='onset_ts', right_on='onset')

    seizures.to_csv(seizure_save, index=False)


def fetch_pat_meta_data_from_adm_file(path):
    """ Get meta data, i.e., sex and age from the admissions txt file"""
    # admission file is currently per default drug.txt
    pat_location_identifier = path.split('/')[-2]
    adm_file = path+'/drugs.txt'
    adm_save = path+f'/{pat_location_identifier}_meta_full.csv'
    # Abort writing if the *meta.csv file already exists
    # if os.path.isfile(adm_save):
    #     print(f'{adm_save} already exists. Writing aborted. Move or delete the old file first.')
    #     return None
    # If sex or age are not found leave them as false
    sex = False
    age = False
    onsetage = False
    do_foci, do_s_types, do_check_surg, do_histology, do_loc, do_follow = False, False, False, False, False, False
    foci = []
    s_types = []
    attention = False
    verbal_declarat_memory = False
    nonverbal_declarat_mem = False
    language = False
    visuospatial_functions =False
    executive_functions = False
    Surgery = False
    Histology = []
    Localisation = []
    FollowUp =  []


    with open(adm_file, 'r') as AF:
        for line in AF.readlines():
            # if sex and age:  # Stop reading as sone as sex and age are found
            #     break
            if 'gender' in line:  # Transform gender to shorthands
                if 'female' in line:
                    sex = 'F'
                elif 'male' in line:
                    sex = 'M'
            if 'patient age:' in line:
                age = int(line.split('patient age:')[-1].split(')')[0])
            if 'onsetage:' in line:
                onsetage = int(line.split('onsetage:')[-1].split(',')[0])
            # foci related
            if 'seizureType' in line:
                do_foci = False
            if do_foci:

                tmp = line.split('\t')
                foci.append(tmp[1:3])
            if 'number	ieeg_based	localisation' in line:
                do_foci = True
            # Seizure type realted
            if 'complications' in line:
                do_s_types = False
            if do_s_types:
                s_types.append(line.split('\t')[:-1])
            if 'Seizuretype' in line:
                do_s_types = True
            # Cognitive testing stuff
            if 'attention' in line:

                attention = line.split('\t')[-1].replace('\n', '')
            if 'verbal_declarat_memory' in line:
                verbal_declarat_memory = line.split('\t')[-1].replace('\n', '')
            if 'nonverbal_declarat_mem' in line:
                nonverbal_declarat_mem = line.split('\t')[-1].replace('\n', '')
            if 'language' in line:
                language = line.split('\t')[-1].replace('\n', '')
            if 'visuospatial_functions' in line:
                visuospatial_functions = line.split('\t')[-1].replace('\n', '')
            if 'executive_functions' in line:
                executive_functions = line.split('\t')[-1].replace('\n', '')
            # Surgery stuff
            if do_check_surg:
                if 'date' in line:
                    Surgery = line.split('\t')[-1].replace('\n', '')
                do_check_surg = False
            if 'Surgery' in line:
                do_check_surg = True
            # Localization
            if 'Complications:' in line:
                do_loc = False
            if do_loc:
                Localisation.append(line.replace('\n', '').split('\t'))
            if 'region	subregion	lateralisation' in line:
                do_loc = True
            # Follow UP
            if 'histology' in line:
                do_follow = False
            if do_follow:
                FollowUp.append(line.replace('\n', '').split('\t'))
            if 'date	interval	outcome	commentary' in line:

                do_follow = True

            # Histology
            if do_histology:
                Histology.append(line.replace('\n','').split('\t'))
            if 'histology' in line:
                do_histology = True




    # print({'sex': [sex], 'age': [age], 'onsetage': [onsetage], 'eeg_focus': foci,
    #        'seizureTypeFrequecy': s_types, 'attention': attention, 'verbal_declarat_memory': verbal_declarat_memory,
    #        'nonverbal_declarat_mem': nonverbal_declarat_mem, 'language': language,
    #        'visuospatial_functions': visuospatial_functions, 'executive_functions': executive_functions,
    #        'Surgery': Surgery, 'Localisation': Localisation, 'Histology': Histology,
    #        'FollowUp': FollowUp})
    out = {'sex': sex, 'age': age, 'onsetage': onsetage, 'eeg_focus': foci,
     'seizureTypeFrequecy': s_types, 'attention': attention, 'verbal_declarat_memory': verbal_declarat_memory,
     'nonverbal_declarat_mem': nonverbal_declarat_mem, 'language': language,
     'visuospatial_functions': visuospatial_functions, 'executive_functions': executive_functions,
     'Surgery': Surgery, 'Localisation': Localisation, 'Histology': Histology,
     'FollowUp': FollowUp}
    df = pd.Series(dtype=object, name=pat_location_identifier)
    for key, val in out.items():
        df[key] = val
    df = df.to_frame().T
    print(df)
   # df = pd.DataFrame.from_dict({'sex': [sex], 'age': [age], 'onsetage': [onsetage], 'eeg_focus': foci})
    # Save the meta data to a csv file
    # print(df)
    df.to_csv(adm_save, index=False)
    return df

# === Fetch interictal stuff ===

def convert_to_datetime_from_str(time_str):
    return datetime.strptime(time_str, "%d.%m.'%y %H:%M:%S")



def interictal_events_txt_to_csv(path):
    pat_location_identifier = path.split('/')[-2]
    file = path + '/IED.txt'
    ied = pd.read_csv(file, delimiter='\t')
    if len(ied) == 0:
        if np.all(ied.columns == ['total:', '0 interictal event(s)']):
            ied = pd.DataFrame(columns=['type', 'pattern', 'electrode'])
        else:
            print(ied)
            raise KeyError(f'{pat_location_identifier} IED file has no interictal events but columns dont match either')
    elif np.all(ied.columns == ['type', 'pattern', 'electrode']):
        number_of_ied = int(ied.iloc[len(ied)-1, 1].split(' ')[0])
        if len(ied) - 1 != number_of_ied:
            raise ValueError(f'{pat_location_identifier}: {len(ied)} ieds found but {number_of_ied} expected.')
        else:
            ied = ied.iloc[:len(ied)-1, :]
            print(f'{pat_location_identifier}: succesfully found {number_of_ied} ieds.')
    else:
        raise KeyError(f'{pat_location_identifier} IED file has interictal events but cant identify columns')
    ied.to_csv(f'{path}/{pat_location_identifier}_interictal_events.csv', index=False)


def sublclinical_seizrues_txt_to_csv(path):
    pat_location_identifier = path.split('/')[-2]
    file = path + '/subseizures.txt'
    subseizures = pd.read_csv(file, delimiter='\t')
    if len(subseizures) == 0:
        if np.all(subseizures.columns == ['total:', '0 subclinical seizure(s)']):
            subseizures = pd.DataFrame(columns = ['#', 'onset',  'offset'])
        else:
            raise KeyError(f'{pat_location_identifier} subseizure file has no subseizure but columns dont match either')
    elif np.all(subseizures.columns == ['#', 'onset',  'offset']):
        number_of_subs = int(subseizures.iloc[len(subseizures) - 1, 1].split(' ')[0])
        if len(subseizures) - 1 != number_of_subs:
            raise ValueError(f'{pat_location_identifier}: {len(subseizures)} subseizures found but {number_of_subs} expected.')
        else:
            subseizures = subseizures.iloc[:len(subseizures) - 1, :]
            print(f'{pat_location_identifier}: succesfully found {number_of_subs} subseizures.')
    else:
        raise KeyError(f'{pat_location_identifier} subseizure file has subseizures but cant identify columns')

    subseizures['onset'] = subseizures['onset'].apply(convert_to_datetime_from_str)

    for ind in subseizures.index:

        if pd.isnull(subseizures.loc[ind, 'offset']):
            subseizures.loc[ind, 'offset'] = subseizures.loc[ind, 'onset'] + timedelta(seconds=1)
            print('Making offset subclinical 1 sec after onset.')
        elif len(subseizures.loc[ind, 'offset'].split(' ')) == 1:
            time_in_hms = datetime.strptime(subseizures.loc[ind, 'offset'], '%H:%M:%S').time()
            subseizures.loc[ind, 'offset'] = datetime.combine(subseizures.loc[ind, 'onset'].date(),
                                                              time_in_hms)
        elif len(subseizures.loc[ind, 'offset'].split(' ')) == 2:
            subseizures.loc[ind, 'offset'] = convert_to_datetime_from_str(subseizures.loc[ind, 'offset'])
        else:
            raise ValueError(f"Cant convert {subseizures.loc[ind, 'offset']}")
        # Make a sanity check if any time crossed the zero
        if subseizures.loc[ind, 'offset'] - subseizures.loc[ind, 'onset'] > timedelta(hours=4):
            raise ValueError(f"{pat_location_identifier} subseizure event at {subseizures.loc[ind, 'onset']} was converted to >4 hours" +
                             f"Please check this manually")
    subseizures.to_csv(f'{path}/{pat_location_identifier}_subseizures.csv', index=False)


if __name__ == '__main__':
    import glob
    path_to_project = '/Users/paulmueller/BIH_Neuro/LRTC_project'
    rec_window = pd.read_csv(f'{path_to_project}/all_rec_days.csv', index_col=0)
    all_meta_data = pd.DataFrame(dtype=object)
    for pat_path in glob.glob(f'{path_to_project}/pat*/meta'):
        pat = pat_path.split('/')[-2]
        print(pat_path)
        # electrodes_convert_txt_to_csv(pat_path)
        # aed_convert_txt_to_csv(pat_path, recording_window_frame=rec_window.loc[pat])
        # seizures_convert_txt_to_csv(pat_path)
        all_meta_data = pd.concat([all_meta_data, fetch_pat_meta_data_from_adm_file(pat_path)])
        # interictal_events_txt_to_csv(pat_path)
        # sublclinical_seizrues_txt_to_csv(pat_path)
    print(all_meta_data)
    all_meta_data.to_csv(f'{path_to_project}/all_meta_data.csv')
    print('Done')
