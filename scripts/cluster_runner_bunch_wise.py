#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 11:12:36 2021

@author: paulmueller
Run for each patient a calc script but bunch up the calculation be a set of datafiles to keep runs short
"""
import os
import subprocess
import numpy as np
import glob


def write_submit_file(cmd, time_in_sec, job_name):
    """Create the submit file for the cluster."""

    h = str(int(time_in_sec // 3600)).zfill(2)
    m = str(int((time_in_sec % 3600) // 60)).zfill(2)
    s = str(int((time_in_sec % 3600) % 60)).zfill(2)
      
    new_sub_file_str = '#!/bin/bash\n\n'
    new_sub_file_str += f'#SBATCH --job-name={job_name}\n'
    new_sub_file_str += f'#SBATCH --output={job_name}.out\n'
    new_sub_file_str += f'#SBATCH --error={job_name}.err\n\n'
    new_sub_file_str += '#SBATCH --ntasks=1\n'
    new_sub_file_str += '#SBATCH --nodes=1\n'
    new_sub_file_str += '#SBATCH --mem-per-cpu=16G\n'

    new_sub_file_str += f'#SBATCH --time={h}:{m}:{s}\n \n'
    new_sub_file_str += cmd
    with open(f'{job_name}_submit.sh', 'w') as sub_file:
        sub_file.write(new_sub_file_str)


# Set to False for script testing
calculate = True
# Name of the script to be executed
CALCSCRIPT = '/data/gpfs-1/users/muellepm_c/scripts/iEEG_STC/scripts/calc_STC_simple.py'
# Project path where output will be saved and where the config file can be found
# (under PATH/patID/[data, processed, meta])
OUTPUT_PATH = '/data/gpfs-1/users/muellepm_c/work/LRTC_project'
# Path to the data
DATA_PATH = f'/data/gpfs-1/groups/ag_meisel/work/epilepsiae_data'
# Configuration file to use which must be stored in each patID/meta directory
CONFIG_FILE = 'psd_0.125_full_length_5.0.config'  # (for VI calculation VI_Reed_30_full_length_5.0.config)
# Name of the configuration for the cluster job name
config_name = CONFIG_FILE.replace('.config', '')
# Into how many cluster jobs should this process be splitted
approx_calc_length = 12000  # Test how long one calculation take son your machine (in sec)
max_job_length = 3600 * 20  # Set a limit on how long a job batch should run at max (in sec)

# Cycle through all patients
for pat in glob.glob(f'{OUTPUT_PATH}/pat*'):
    patID = pat.split('/')[-1]
    print(patID)
    #if patID != 'pat922':
    #     continue
    # Get all data files from the group folder
    DATAFILES = sorted(glob.glob(f'{DATA_PATH}/{patID}/*.data'))
    print(DATAFILES)
    if len(DATAFILES) == 0:
        raise FileNotFoundError('No datafiles found')
    # Get the configuration file
    PAT_CONFIG_FILE = f'{OUTPUT_PATH}/{patID}/meta/' + CONFIG_FILE
    print(PAT_CONFIG_FILE)

    job_batches = np.arange(0, len(DATAFILES), max_job_length // approx_calc_length, dtype=np.int32)
    job_batches = np.append(job_batches, len(DATAFILES))
    # Write the submit files for each batch, batches are split to
    for i in range(len(job_batches)-1):
        command = f'python3.8 -u {CALCSCRIPT} {PAT_CONFIG_FILE} '
        name = f'{patID}_{config_name}_{DATAFILES[job_batches[i]][-9: -5]}to{DATAFILES[job_batches[i + 1]-1][-9: -5]}'
        # Add all data files of a batch to the command string
        for F in DATAFILES[job_batches[i]: job_batches[i+1]]:
            command += f'{F} '
        # Change cwd to the submits directory
        os.chdir('/data/gpfs-1/users/muellepm_c/scratch/submit_scripts')
        # Write submit script
        write_submit_file(command, max_job_length, name)
        # Run the calculation
        if calculate:
            print(f'Starting job number: {i}')
            print(f'Job_name: {name}\n')
            p = subprocess.Popen(['sbatch', f'{name}_submit.sh'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            p.wait()
