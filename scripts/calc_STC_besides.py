#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 10:03:59 2021
@author: paulmueller
Takes a configuration file as first argument and datafiles as the others to calculate autocorrelation functions and
crosscorrelation functions from the data. Then calculates lrtc and lrsc measures from these.
"""
import os
import socket
import sys
import time
from shutil import copy

from datetime import datetime
import numpy as np
import pandas as pd
import ast
import glob

t0 = time.time()

# Check on which machine you are and add the path to epilepsiae_space_time_resolved (estr)
host = socket.gethostname()
# We assume that you are on the BIHC. If this is not the case add your machine and the location below
estr_dir = '/data/gpfs-1/users/muellepm_c/scripts/iEEG_STC'
sys.path.insert(0, estr_dir)

# Importing on the cluster only works within a try statement for unkonwn reasonss
try:
    import iEEG_STC.utils as utils
    import iEEG_STC.additional_functions as a_f
    from iEEG_STC.reader import ECOGReader
except ModuleNotFoundError:
    raise Exception('ImportError. Could not import iEEG_STC')
else:
    print('Importing completed')


force = False  # Force calculation even if data exists already. This overwrites old data

# Path construction from the sys arguments
patID = sys.argv[1]
bands = (ast.literal_eval(sys.argv[2]), ast.literal_eval(sys.argv[3]))
path_to_general = '/data/gpfs-1/users/muellepm_c/work/LRTC_project'
CONFIGFILE = f'{path_to_general}/{patID}/meta/psd_0.125_full_length_5.0.config'
# Read the parameters form the file
calc_parameters = a_f.read_config_file(CONFIGFILE)
calc_parameters['bands'] = (bands,)
print(calc_parameters)
#patID = calc_parameters['patID']  # For convenience stored in varibale
# Construct the project general path
#path_to_general = CONFIGFILE.split(patID)[0]
# Path to where the head files of the raw data are stored
path_to_raw = '/data/gpfs-1/groups/ag_meisel/work/epilepsiae_data'
#'/Users/paulmueller/BIH_Neuro/LRTC_project' # 'PATH_TO_RAW_DATA'
# Construct the paths to all the other data
locations = a_f.data_file_locations_from_settings(calc_parameters, path_to_general)
# Read the configuration file
calc_parameters = a_f.read_config_file(CONFIGFILE)
classifier = pd.read_csv(os.path.join(locations['meta'], f'{patID}_classifier_subs_dropped_with_overlap.csv'), index_col=0)
if np.any(pd.isnull(classifier)):
    raise ValueError('Something in the classifier is not correct')
# Read in the quality frame
quality = pd.read_csv(os.path.join(locations['meta'], f"{patID}_segments_goodness.csv"), index_col=0)
# Read in additional meta parameters. First the electrode data with region of interest
elecs = pd.read_csv(os.path.join(locations['meta'], f'{patID}_likely_zone_b.csv'), index_col=0)
# Read interictal events and delete them from the elecs
ievents = pd.read_csv(os.path.join(locations['meta'], f"{patID}_interictal_events.csv"), index_col=0)
elecs = elecs.loc[~np.in1d(elecs.index, ievents['electrode'])]
# Get the distances between the cannels
dists = utils.get_channels_dists(elecs)
bins = np.arange(176)
# Find all head files of the to read in the meta data
head_files = sorted(glob.glob(f'{path_to_raw}/{patID}/*.head'))
if len(head_files) == 0:
    raise FileNotFoundError(f'There are no head files in {path_to_raw}/{patID}')

t1 = time.time()
print(f'0. Initialization took {t1-t0:.4f}sec')
for band in calc_parameters['bands']:
    TC_data = pd.DataFrame()
    TC_data.index.name = 'Time'
    SC_data = pd.DataFrame()
    SC_data.index.name = 'Time'
    shuffled_TC_data = pd.DataFrame()
    shuffled_TC_data.index.name = 'Time'
    shuffled_SC_data = pd.DataFrame()
    shuffled_SC_data.index.name = 'Time'

    included_points = 0
    excluded_points = 0
    valid_time_points = []
    # Construct the general save path
    band_str = f"_{band[0]:03d}_{band[1]:03d}"
    save_path = os.path.join(locations['processed'], locations['settings'] + f'{band_str}_XXXX')
    for hf in head_files[:1]:
        t1 = time.time()
        # Reads the .head file of the data
        print(hf)
        t1 = time.time()
        recID = hf.split('/')[-1].replace('.head', '')
        # Delete the electrodes from reacording which are probably bad
        rec_elecs = elecs.loc[
            np.intersect1d(quality.loc[recID].index[quality.loc[recID].values.astype(bool)], elecs.index)]
        if len(rec_elecs) == 0:
            print(f'skipping {recID} as no electrodes are good')
            continue
        # Read the head of the recording. Necessary only for timing issues
        tmp_head = ECOGReader(hf)
        start_ts = tmp_head.head_dict['start_ts']
        # Get the path to the power spectral density/band powers
        psd_path = os.path.join(path_to_general, patID, 'data', recID, locations['settings'],
                                locations['settings'] + band_str + '.csv')
        # Read the band powers and drop the band name
        psd = pd.read_csv(psd_path, index_col=1)
        psd.drop('Band', inplace=True, axis=1)

        psd = psd.loc[:, np.in1d(psd.columns, rec_elecs.index)]
        # Calculate the distances between the remaining electrodes
        dists = utils.get_channels_dists(rec_elecs)
        # Go through all acf/cc windows (defined by 'acf_length') of this data bunch
        times = np.arange(calc_parameters['cut_sym_after_filter_in_sec'],
                          tmp_head.head_dict['duration_in_sec'] - calc_parameters['cut_sym_after_filter_in_sec'],
                          calc_parameters['acf_length'] * (1 - calc_parameters['acf_overlap']))
        times = times[:-int((calc_parameters['acf_overlap']) / (1 - calc_parameters['acf_overlap']))]
        for i in times:
            if isinstance(start_ts, datetime.datetime):
                start_ts = start_ts.timestamp()
            current_t = start_ts + i
            if current_t not in classifier.index:
                # Count if a time point was excluded due to not being in classifier (Probably an issue of quality)
                excluded_points += 1
                print(f'{current_t} excluded due to classifier')
            else:
                # If the time point is ok save that it is included
                included_points += 1
                valid_time_points.append(current_t)
                # Give the psd window on which acf and cc will be calculated
                data_window = psd.loc[np.logical_and(psd.index >= i, psd.index < i + calc_parameters['acf_length'])]
                # Shuffle if a surrogate run should be done
                shuffled_data_window = utils.shuffle_all_channels(data_window)
                # Calculalte ACF
                acf = utils.estimated_autocorrelation(data_window).mean(axis=1).squueze()
                shuffled_acf = utils.estimated_autocorrelation(shuffled_data_window).mean(axis=1).squueze()

                # Calculate the measure (lrtc) here it is when the data crosses first time below the half maximum
                ind, hm = utils.hwhm(acf, hmfunc=lambda x: utils.hm_from_lag_x_med_3_2(x, x=1))
                TC_data.loc[current_t, 'TC'] = ind
                TC_data.loc[current_t, 'HM'] = hm
                # Calculate the measure (lrtc) here it is when the data crosses first time below the half maximum
                ind, hm = utils.hwhm(shuffled_acf, hmfunc=lambda x: utils.hm_from_lag_x_med_3_2(x, x=1))
                shuffled_TC_data.loc[current_t, 'TC'] = ind
                shuffled_TC_data.loc[current_t, 'HM'] = hm

                # Calculate crosscorrelation
                cc = utils.cc_by_binned(data_window, dists)
                SC_data.loc[current_t,
                            'SC (AUC 7 - 79 mm)'] = (cc.loc[np.logical_and(cc.index >= 9, cc.index < 79)]).squeeze()
                shuffled_cc = utils.cc_by_binned(shuffled_data_window, dists)
                shuffled_SC_data.loc[current_t,
                            'SC (AUC 7 - 79 mm)'] = (shuffled_cc.loc[np.logical_and(shuffled_cc.index >= 9,
                                                                                    shuffled_cc.index < 79)]).squeeze()

    print(f"Saving TC/SC to {save_path.replace('XXXX','TC/SC')}")
    TC_data.to_pickle(save_path.replace('XXXX', 'TC') + '.pkl')
    SC_data.to_pickle(save_path.replace('XXXX', 'SC') + '.pkl')

    shuffled_TC_data.to_pickle(save_path.replace('XXXX', 'TC') + '_surr01.pkl')
    shuffled_SC_data.to_pickle(save_path.replace('XXXX', 'SC') + '_surr01.pkl')

    t4 = time.time()
t5 = time.time()
print(f'Calculation finished without error. / {t5-t0:.4f}sec ')
