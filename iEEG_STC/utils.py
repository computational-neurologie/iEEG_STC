#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 09:14:45 2021

@author: paulmueller
"""
import numpy as np
import scipy.signal as scs
import pandas as pd
from datetime import date, datetime, timedelta
from scipy.spatial.distance import cdist
import scipy.optimize as sco
from sklearn.metrics import mutual_info_score
# from numba import jit


def check_for_uniques(data, thres=2.):
    """ Check if more than thres values are repeated in the data"""
    return len(set(data)) >= (len(data) - thres)


def psd_welch(data, fs, norm=False, welch_res=1, welch_overlap=0.5,
              welch_detrend='constant', welch_window='hanning', not_uniques_thres_frac=16, **kwargs):
    """
    Calculate the power spectrum along each column/channel (ignoring Time) using welchs method,
    data: input (DataFrame) with each column being one channel
    fs: (float) sample frequency
    norm: (bool) if the psd should be normalized
    welch args: as in scipy.signal.welch
    not_uniques_thres_frac: (float) fraction of data maximally can be repeated.
                            If more np.nan is returned for the channel
    **kwargs: are ignored and are only for convenience to feed dictonaries into the function
    """
    # Define the length of the windows used to calculate the power spectrum with welchs method
    nperseg = len(data)
    if welch_res is not None:
        nperseg = fs // welch_res
    # Calculate the overlap of the windows
    local_overlap = nperseg * welch_overlap
    # Check if column has enough unique values to calculate a psd
    calc_psd_at = data.columns[[check_for_uniques(data[ch],
                                thres=len(data) / not_uniques_thres_frac) for ch in data.columns]]
    # Exlude the time for psd calculation
    calc_psd_at = calc_psd_at[calc_psd_at != 'Time']
    # Calculate the frequency bins of the psd (necessary because calc_psd_at can be empty
    freqs = np.arange(0, int(1+nperseg/2))*fs/nperseg
    # Define the DataFrame to save the psd into
    psd = pd.DataFrame(columns=data.columns[data.columns != 'Time'], index=freqs)
    psd.index.name = 'Frequency'
    # Calculate the psd using welchs method
    if len(calc_psd_at) > 0:
        w, pw = scs.welch(data[calc_psd_at].T, fs=fs, window=welch_window, nperseg=nperseg,
                          noverlap=local_overlap, nfft=None, detrend=welch_detrend,
                          scaling='density', axis=-1, return_onesided=True)
        psd[calc_psd_at] = pw.T
    # Normalize
    if norm:
        psd[calc_psd_at] = psd[calc_psd_at].apply(lambda x: np.full(len(x),np.nan) if np.all(x==0)  else x/np.sum(x))
    return psd


def get_vi_reed(data, fs, welch_res=1, welch_overlap=0.5, welch_detrend='constant', welch_window='hanning',
                band_power_window_length_in_secs=30, not_uniques_thres_frac=16, **kwargs):
    """
    Calculate the vigilance index as in Reed et al., 2017 as ratio of frequency powers
    (theta+delta+spindle)/(alpha+high_beta)
    band_power_window_length_in_secs:  window on which the powers are calculated (defaults to 30s as in Reed)
    other arguments as in psd_welch ->
    data: input (DataFrame) with each column being one channel
    fs: (float) sample frequency
    welch args: as in scipy.signal.welch
    not_uniques_thres_frac: (float) fraction of data maximally can be repeated.
                            If more np.nan is returned for the channel
    **kwargs: are ignored and are only for convenience to feed dictonaries into the function
    """

    result = pd.Series(name='VI_Reed')
    result.index.name = 'Time'
    # Convert the window length from sec to ticks
    window_ticks = int(band_power_window_length_in_secs*fs)
    # Calculate time points where the power is assessed, every band_power_window_length_in_secs
    time_in_ticks = np.arange(start=0, stop=len(data['Time']), step=int(window_ticks))
    for T in time_in_ticks:
        segment = data.iloc[T:T+window_ticks].copy()
        if len(segment) != window_ticks:
            continue
        segment.reset_index(inplace=True, drop=True)
        # Calculate the psd
        psd = psd_welch(segment, fs=fs, welch_res=welch_res, welch_detrend=welch_detrend, welch_window=welch_window,
                        welch_overlap=welch_overlap, not_uniques_thres_frac=not_uniques_thres_frac, norm=True)
        psd = psd.mean(axis=1)
        # Explicitly calculate the VI as in Reed et al., 2017
        # delta power
        delta_array = np.logical_and(psd.index >= 1, psd.index <= 4)
        delta_power = psd.loc[delta_array].sum()
        # theta power
        theta_array = np.logical_and(psd.index >= 4, psd.index <= 7)
        theta_power = psd.loc[theta_array].sum()
        # spindle power
        spindle_array = np.logical_and(psd.index >= 11, psd.index <= 16)
        spindle_power = psd.loc[spindle_array].sum()
        # alpha power
        alpha_array = np.logical_and(psd.index >= 8, psd.index <= 13)
        alpha_power = psd.loc[alpha_array].sum()
        # high beta power
        high_beta_array = np.logical_and(psd.index >= 20, psd.index <= 40)
        high_beta_power = psd.loc[high_beta_array].sum()
        # Set output to np.nan if any power was np.nan (probably due to not enough unique values in data)
        if (np.array([alpha_power, high_beta_power, spindle_power, theta_power, delta_power]) == 0).any():
            result.at[data.iloc[T].loc['Time']] = np.nan
        else:
            result.at[data.iloc[T].loc['Time']] = (delta_power+theta_power+spindle_power)/(alpha_power+high_beta_power)
    return result


def get_band_power(data, fs, bands, band_power_mode='median', do_log=True, norm=True, welch_res=1, welch_overlap=0.5,
                   welch_detrend='constant', welch_window='hanning', not_uniques_thres_frac=16,  **kwargs):
    """
    Calculate the power in certain frequency bands and use some averaging method (mean, median)
    bands: ((f0,f1), (f2,f3), ...) band borders to calculate the power in
    band_power_mode: return median or mean values within each band (default: median)
    do_log: Transorm log10 the results before returning
    other args as in psd_welch ->
    data: input (DataFrame) with each column being one channel
    fs: (float) sample frequency
    norm: (bool) if the psd should be normalized
    welch args: as in scipy.signal.welch
    not_uniques_thres_frac: (float) fraction of data maximally can be repeated.
                            If more np.nan is returned for the channel
    **kwargs: are ignored and are only for convenience to feed dictonaries into the function
    """
    result = pd.DataFrame()
    # Calculate psd on the whole given data
    psd = psd_welch(data, fs=fs, welch_res=welch_res, welch_detrend=welch_detrend, welch_window=welch_window,
                    welch_overlap=welch_overlap, not_uniques_thres_frac=not_uniques_thres_frac, norm=norm)
    # Go through all desired bands and calculate the mean/median (log) power
    for band in bands:
        band_array = np.logical_and(psd.index >= band[0], psd.index <= band[1])
        band_psd = psd.loc[band_array, psd.columns != 'Frequency']
        # Use specified mean or median
        if band_power_mode == 'median':
            band_power = band_psd.apply(lambda x: np.nan if pd.isnull(x).any() else np.median(x))
            band_power.name = str(band)
        elif band_power_mode == 'mean':
            band_power = band_psd.apply(lambda x: np.nan if np.isnull(x).any() else np.mean(x))
            band_power.name = str(band)
        else:
            raise Exception('Median or mean has to be specified, band_power_mode')
        # Log10 transform power
        if do_log:
            band_power = band_power.apply(np.log10)
        #result = pd.concat([result, band_power.T])
        result = result.append(band_power)
    return result


def get_band_power_segments(data, fs, bands, band_power_window_length_in_secs, band_power_mode='median', do_log=True,
                            norm=True, welch_res=1, welch_overlap=0.5, welch_detrend='constant', welch_window='hanning',
                            not_uniques_thres_frac=16,  **kwargs):
    """
    Segment the data and calculate the band power as in get_band_power
    band_power_window_length_in_secs: length of segments to calculate the band power
    other args as in get_band_power ->
    bands: ((f0,f1), (f2,f3), ...) band borders to calculate the power in
    band_power_mode: return median or mean values within each band (default: median)
    do_log: Transorm log10 the results before returning
    other args as in psd_welch ->
    data: input (DataFrame) with each column being one channel
    fs: (float) sample frequency
    norm: (bool) if the psd should be normalized
    welch args: as in scipy.signal.welch
    not_uniques_thres_frac: (float) fraction of data maximally can be repeated.
                            If more np.nan is returned for the channel
    **kwargs: are ignored and are only for convenience to feed dictonaries into the function
    """
    result = pd.DataFrame()
    # Convert the window length from sec to ticks
    window_ticks = int(band_power_window_length_in_secs*fs)
    # Calculate time points where the power is assessed
    time_in_ticks = np.arange(start=0, stop=len(data['Time']), step=int(window_ticks))
    # Calculate the mean power in bands
    for T in time_in_ticks:
        segment = data.iloc[T:T+window_ticks]  # segment the data
        if len(segment) != window_ticks:  # This will exclude the last segment if it is too short
            continue
        segment.reset_index(inplace=True, drop=True)
        # Calculate the band power
        band_power = get_band_power(segment, fs=fs, bands=bands, band_power_mode=band_power_mode,
                                    welch_res=welch_res, welch_detrend=welch_detrend,
                                    welch_window=welch_window, welch_overlap=welch_overlap,
                                    not_uniques_thres_frac=not_uniques_thres_frac, norm=norm, do_log=do_log)
        # Add a time axis
        band_power.insert(0, 'Time', data['Time'][0]+np.full(len(band_power), T/fs))
       # result = pd.concat([result, band_power])
        result = result.append(band_power)
    # Reset the dataframes indices and reorder it after time and band
    result.reset_index(inplace=True)
    result = result.rename(columns={'index': 'Band'})
    result.sort_values(['Time', 'Band'], inplace=True)
    result.reset_index(inplace=True, drop=True)
    return result


def shuffle_all_channels(data):
    """Randomly shuffle the data by permuting every column in the data seperately"""
    data = data.copy(deep=True)
    data = data.apply(np.random.permutation)
    return data


# Cross channel calculation
def crosscorrelation(data_set):
    """ Convenience function to calculate the crosscorrelation with pandas"""
    cc = data_set.corr()
    return cc


def get_channels_dists(elecs):
    """Calculate the euclidian distance of channels from x,y,z coordinates"""
    pos = elecs.loc[:, ['x', 'y', 'z']].to_numpy().astype(float)
    dists = cdist(pos, pos)
    return pd.DataFrame(dists, columns=elecs.index, index=elecs.index)


def cc_by_dist(data_set, dists):
    """
    Calculate the crosscorrelation for all pairs of channels by their distance
    data_set: data of all channels
    dists: matrix of distances of the channels. Use get_channels_dists to obtain it
    returns dataframe with 4 columns (distance, crosscorrelation, channel name A, channel name B)
    """
    tmp_len = len(data_set.columns)
    # Raise an error if distances are not found for all channel pairs
    if not np.in1d(data_set.columns, dists.columns).all():
        raise KeyError(f'data_set has channel which is not in dists: {np.setxor1d(data_set.columns,dists.index)}')
    # Use only the distances of the channels in data_set
    else:
        dists = dists.loc[data_set.columns, data_set.columns]
    # Calculate the crosscorrelation
    cc = crosscorrelation(data_set)
    # Reformate the results to 4 columns (distance, crosscorrelation, channel name A, channel name B=
    out = pd.DataFrame()#columns=['dist', 'cc', 'chA', 'chB'])
    for i_key in range(0, tmp_len):
        for j_key in range(i_key, tmp_len):
            ch_a, ch_b = cc.columns[i_key], cc.columns[j_key]
            out = pd.concat([out, pd.Series([dists.loc[ch_a, ch_b], cc.loc[ch_a, ch_b], ch_a, ch_b],
                                            index=['dist', 'cc', 'chA', 'chB']).to_frame().T])
            # s                         index=['dist', 'cc', 'chA', 'chB']), ignore_index=True)
    # Sort by distance ascending
    out.sort_values('dist', inplace=True)
    out.reset_index(inplace=True, drop=True)
    return out


def cc_by_binned(data_set, dists, bins=np.linspace(0, 200, 201)):
    """
    Return crosscorrelation average over distance bins
    data_set: data of all channels
    dists: matrix of distances of the channels. Use get_channels_dists to obtain it
    bins: in cm
    """
    out = pd.Series(index=bins[:-1], dtype=float)
    cc_with_names = cc_by_dist(data_set, dists)
    for i in range(len(out.index)):
        out.iloc[i] = np.mean(cc_with_names.loc[np.logical_and(cc_with_names['dist'] >= bins[i],
                                                               cc_with_names['dist'] < bins[i+1]), 'cc'])
    return out


def _cavagna_cc_main_loop(data_set_as_numpy):
    """
    Calculate correlation measure from Cavagna et al., 2010 (dot product), data_set_as_numpy should be centred around
    its mean
    returns matrix of cavagna_correlation
    """
    num_ch = np.shape(data_set_as_numpy)[1]
    cavagna_cc_out = np.zeros((num_ch, num_ch))
    for i in range(num_ch):
        for j in range(num_ch):
            cavagna_cc_out[i, j] = np.dot(data_set_as_numpy[:, i], data_set_as_numpy[:, j])
    return cavagna_cc_out


def cavagna_cc(data_set):
    """
    Convert dataset to numpy and calculate correlation measure from Cavagna et al., 2010 (dot product)
    returns matrix of cavagna_correlation
    """
    num_ch = len(data_set.columns)
    data_set_as_numpy = data_set.copy().to_numpy()
    # Substract the mean
    data_set_as_numpy -= np.repeat(data_set_as_numpy.mean(axis=1)[:, np.newaxis], num_ch, axis=1)
    # Calc
    cavagna_cc_out = _cavagna_cc_main_loop(data_set_as_numpy)
    return cavagna_cc_out


def cavagna_cc_by_binned(data_set, dists, bins=np.linspace(0, 200, 201)):
    """
    Calculate the Cavagna et al., 2010 correlation average in bins (in cm)
    """
    # Move time to Index
    if 'Time' in data_set:
        data_set = data_set.set_index('Time', drop=True)
    # Remove the band column (should be one band only anyways but is not checked)
    if 'Band' in data_set:
        data_set = data_set.drop('Band', axis=1)
    # Raise error if distances are missing
    if not np.in1d(data_set.columns, dists.columns).all():
        raise KeyError(f'data_set has channel which is not in dists: {np.setxor1d(data_set.columns,dists.index)}')
    else:
        dists = dists.loc[data_set.columns, data_set.columns]
    # Calculate the Cavagna correlation
    cavagna_cc_with_names = pd.DataFrame(cavagna_cc(data_set), index=data_set.columns, columns=data_set.columns)
    # Convert matrix to Series with bins as index (index give upper border of bins
    out = pd.Series(index=bins[1:], name='cavagna_cc_by_dist')
    for i in range(len(out.index)):
        tmp = cavagna_cc_with_names[np.logical_and(dists >= bins[i], dists < bins[i+1])].values
        if np.isnan(tmp).all():
            continue
        out.iloc[i] = np.nanmean(tmp)
    return out


# LRTC calculation
def normalize(x):
    """ Normalize data to [0,1]"""
    return (x-np.min(x))/(np.max(x)-np.min(x))


def _estimated_autocorrelation_1d(x):
    """ Estimate the autocorrelation of x
     https://stackoverflow.com/questions/14297012/estimate-autocorrelation-using-python"""
    if np.isnan(x).all():  # Return np.nan if data is only np.nan
        return np.full(len(x), np.nan)
    n = len(x)
    variance = x.var()
    x = x-x.mean()  # Center data around mean
    r = np.correlate(x, x, mode='full')[-n:]  # Self correlate the data (gets less good the larger lag )
    result = r/(variance*(np.arange(n, 0, -1)))  # Normalize through variance and  len of overlap
    return result


def estimated_autocorrelation(data):
    """Calculate the autocorrelation column wise and rename the index to lag"""
    auto_result = data.copy(deep=True).apply(_estimated_autocorrelation_1d)
    auto_result.index.name = 'Lag'
    return auto_result


def lag_x(data, x=1):
    """Give a seris with only the values at the x'th lag"""
    raw = data.drop(['Time', 'Lag'], errors='ignore', axis=1)
    result = raw.iloc[[x]]  # Extract the x'th lag value
    result.set_index(np.array([f'Lag{x}']), inplace=True, drop=True)
    result = result.squeeze(axis=0)  # Convert DataFrame to Series
    return result


def hwhm(data, hmfunc=lambda data: hm_from_lag_x_med_3_2(data, x=1)):
    """
    Calculate the value when the data falls first time below HM and HM.
    hmfunc should take exactly one argument the data
    """
    # If Time is available we can return the lag in units of time. If it is not available the lag will be an integer
    if 'Time' in data.columns:
        dt = (np.sum(data['Time'][1:])-np.sum(data['Time'][:-1]))/(len(data['Time'])-1)
    elif 'Lag' in data.columns:
        dt = (np.sum(data['Lag'][1:])-np.sum(data['Lag'][:-1]))/(len(data['Lag'])-1)
    elif data.index.name in ['Time', 'Lag']:
        dt = (np.sum(data.index[1:])-np.sum(data.index[:-1]))/(len(data.index)-1)
    else:
        dt = 1
    # For some reason if it is not done like this inplace changes were observed
    data = data.copy().drop(['Time', 'Lag'], errors='ignore', axis=1)
    # Calculate the hm
    hm = hmfunc(data)
    # Calculate the first value when the data drops below the hm
    ind = data.lt(hm).apply(lambda x: np.nan if ~ (x.any()) else np.argmax(x))
    # Scale by dt
    ind = ind*dt
    ind = ind.rename(f'HW{hm.name}')
    return ind, hm


def hm_from_lag_x_3_2(data, x=1):
    """
    Take minimum between 1/3 and 1/2 of the data as lower and at_lag_x as upper -> hm = (upper + lower)/2
    """
    at_lag_x = lag_x(data, x)
    result = (at_lag_x + data.iloc[len(data)//3:len(data)//2].apply(np.min))/2
    result.name = f'HM1/3to1/2from{at_lag_x.name}'
    return result


def hm_from_lag_x_2(data, x=1):
    """
    Take minimum between x and 1/2 of the data as lower and at_lag_x as upper -> hm = (upper + lower)/2
    """
    at_lag_x = lag_x(data, x)
    result = (at_lag_x + data.iloc[x:len(data)//2].apply(np.min))/2
    result.name = f'HMto1/2from{at_lag_x.name}'
    return result


def hm_from_lag_x_av_3_2(data, x=1):
    """
    Take average between 1/3 and 1/2 of the data as minimum for HM calculation
    """
    at_lag_x = lag_x(data, x)
    result = (at_lag_x + data.iloc[len(data)//3:len(data)//2].apply(np.mean))/2
    result.name = f'HMav1/3to1/2from{at_lag_x.name}'
    return result


def hm_from_lag_x_med_3_2(data, x=1):
    """
    Take average between 1/3 and 1/2 of the data as minimum for HM calculation
    """
    at_lag_x = lag_x(data, x)
    result = (at_lag_x +
              data.iloc[len(data)//3:len(data)//2].apply(lambda y: np.nan if np.isnan(y).any() else np.median(y)))/2
    result.name = f'HMmed1/3to1/2from{at_lag_x.name}'
    return result

def hm_from_lag_x_med_8_4(data, x=1):
    """
    Take average between 1/3 and 1/2 of the data as minimum for HM calculation
    """
    at_lag_x = lag_x(data, x)
    result = (at_lag_x +
              data.iloc[len(data)//8:len(data)//4].apply(lambda y: np.nan if np.isnan(y).any() else np.median(y)))/2
    result.name = f'HMmed1/3to1/2from{at_lag_x.name}'
    return result

def _acw0_or_nan(row, scale):
    """ Calculate the first time when row drops below 0 and returns where this would be in scale.
    Returns np.nan if row consists only of nan/0"""
    if any(pd.isnull(row)):
        return np.nan
    else:
        return scale[np.argmax(row <= 0)]


def acw0(data):
    """Calculates the first times when data drops below 0"""
    # If Time or Lag is in columns this is used as time scale else integers are used
    if 'Lag' in data.columns:
        scale = data['Lag']
        result = data.loc[:, data.columns != 'Lag'].apply(lambda x:  _acw0_or_nan(x, scale))

    elif 'Time' in data.columns:
        scale = data['Time']
        result = data.loc[:, data.columns != 'Time'].apply(lambda x: _acw0_or_nan(x, scale))
    else:
        result = data.apply(lambda x: _acw0_or_nan(x, data.index))

    result.name = 'ACW0'
    return result


def generalized_exponential(x, tau, a, c):
    return a * np.exp(- x / tau) + c


def fit_exponential_and_give_decay_time(data):

    if isinstance(data, pd.Series):
        data = data.to_frame()
    result = pd.DataFrame()
    for col in data.columns:
        popt, pcov = sco.curve_fit(generalized_exponential, data.index, data[col])
        result[col] = pd.Series(popt, index=['tau', 'a', 'c'])

    return result


# Entropy measures
def calc_MI(x, y, bins=None):
    if bins is None:
        bins = int(np.round(np.sqrt(np.min((len(x),len(y)))/5),0))
    try: 
        c_xy = np.histogram2d(x, y, bins)[0]
        mi = mutual_info_score(None, None, contingency=c_xy)
        return mi, bins
    except ValueError:
        return np.nan, bins


def auto_MI_1d(X, bins=None, until_lag=None, normalize=False):
    if until_lag is None:
        until_lag = len(X)//2
    if bins is None:
        bins = int(np.round(np.sqrt(until_lag/5),0))
    auto_out = np.zeros(len(X)-until_lag)
    #auto_out[0] = calc_MI(X[:until_lag], X[:until_lag], bins=bins)[0]
    for i in range(0, until_lag):
        auto_out[i] =calc_MI(X[:until_lag], X[i:i+until_lag], bins=bins)[0]
    if np.any(pd.isnull(auto_out)):
        return np.full(until_lag, np.nan)
    if normalize:
        auto_out = auto_out/auto_out[0]
    return auto_out



# Postprocessing
def condense_dates(data):
    """
    Add a column date to the data. Dates are constructed over 24 h periods and
    if only a few datapoints (larger 23 or lower 1h) in one 24 h period exists they will be attributed to the next
    best full date.
    raises erros if this procedure fails
    """
    data = data.copy()
    # Make sure time is a column and not the index by reseting the index
    if 'Time' not in data.columns:
        data.index.name = 'Time'
        data.reset_index(inplace=True)
    # Transform the time to dates from strings or from datetime objects
    try:
        data['date'] = data.loc[:, 'Time'].transform(date.fromtimestamp)
    except ValueError:
        data['date'] = [x.date() for x in data['Time']]
    # Make time the index again
    data.set_index('Time', inplace=True, drop=True)
    # Get the possible dates
    included_dates = set(data['date'])
    if len(included_dates) < 2:
        raise ValueError('Not enough dates found')
    elif len(included_dates) == 2:
        return data
    # Attribute times to dates and check if some dates only have a few time stemps and
    # then attribute them to the nearset date (by hour larger 23 or smaller 1)
    for i in included_dates:
        # Check that each date has approximately a fair share of time stamps and if kick it and redistribute
        if (np.sum(data['date'] == i))/len(data['date']) < 1/(len(included_dates)+1):
            for ts in data.loc[data['date'] == i].index:
                tsformat = datetime.fromtimestamp(ts)
                if tsformat.hour >= 23:  # Give larger hours to the next day
                    if i + timedelta(days=1) in included_dates:
                        data.loc[ts, 'date'] = i + timedelta(days=1)
                    else:
                        raise Exception(f'Missing values lead to not associatable {tsformat}')
                elif tsformat.hour <= 1:  # Give low hours to the previous day
                    if i - timedelta(days=1) in included_dates:
                        data.loc[ts, 'date'] = i - timedelta(days=1)
                    else:
                        raise Exception(f'Missing values lead to not associatable {tsformat}')
                else:
                    raise Exception(f'Date allocation error. Can not identify with which date {tsformat}' +
                                    f' should be associated.')
    return data


def align_all_in_delta_t(data, times, delta_t, min_vals=1):
    """
    Get for each t in times the mean over data[t] to data[t] +delta_t
    This allows to align time series of different sampling frequency according to times and a delta_t
    data: data which should be aligned
    times: starting time points to which data should be aligned
    delta_t: width of window which should be asigned to every time
    min_vals: minimum values in each [t, t+delta_t] to include data. Is ignored elsewise
    """
    if isinstance(data, pd.Series):
        new_data = pd.Series()
    else:
        new_data = pd.DataFrame(columns=data.columns)
    for T in times:
        # Locate which times of data fit into one time window [t, t+delta_t]
        locater = np.logical_and(data.index >= T, data.index < T + delta_t)
        # Ignores times if not enough values are found
        if np.sum(locater) < min_vals:
            print(f'Data should be realigned as we had min_vals lead to the delation of {T}')
            continue
        new_data.loc[T] = (data.loc[locater].mean())
    return new_data


def drop_seizures(data, seizures, delta_t):
    """
    Drop seizure segements from the data. Delta_t is the range around a seizure were data should be dropped
    """
    data = data.copy()
    for T in data.index:
        if any(np.logical_and(seizures['onset_ts'] >= float(T)-delta_t, seizures['offset_ts'] <= (float(T)+delta_t))):
            data.drop(T, inplace=True)
    return data


# ### detrended fluctuation analysis from https://github.com/dokato/dfa

def calc_rms(x, scale):
    """
    windowed Root Mean Square (RMS) with linear detrending.

    Args:
    -----
      *x* : numpy.array
        one dimensional data vector
      *scale* : int
        length of the window in which RMS will be calculaed
    Returns:
    --------
      *rms* : numpy.array
        RMS data in each window with length len(x)//scale
    """
    # making an array with data divided in windows
    shape = (x.shape[0] // scale, scale)
    X = np.lib.stride_tricks.as_strided(x, shape=shape)
    # vector of x-axis points to regression
    scale_ax = np.arange(scale)
    rms = np.zeros(X.shape[0])
    for e, xcut in enumerate(X):
        coeff = np.polyfit(scale_ax, xcut, 1)
        xfit = np.polyval(coeff, scale_ax)
        # detrending and computing RMS of each window
        rms[e] = np.sqrt(np.mean((xcut - xfit) ** 2))
    return rms


def dfa(x, scale_lim=[5, 9], scale_dens=0.25, show=False):
    """
    Detrended Fluctuation Analysis - measures power law scaling coefficient
    of the given signal *x*.
    More details about the algorithm you can find e.g. here:
    Hardstone, R. et al. Detrended fluctuation analysis: A scale-free
    view on neuronal oscillations, (2012).
    Args:
    -----
      *x* : numpy.array
        one dimensional data vector
      *scale_lim* = [5,9] : list of length 2
        boundaries of the scale, where scale means windows among which RMS
        is calculated. Numbers from list are exponents of 2 to the power
        of X, eg. [5,9] is in fact [2**5, 2**9].
        You can think of it that if your signal is sampled with F_s = 128 Hz,
        then the lowest considered scale would be 2**5/128 = 32/128 = 0.25,
        so 250 ms.
      *scale_dens* = 0.25 : float
        density of scale divisions, eg. for 0.25 we get 2**[5, 5.25, 5.5, ... ]
      *show* = False
        if True it shows matplotlib log-log plot.
    Returns:
    --------
      *scales* : numpy.array
        vector of scales (x axis)
      *fluct* : numpy.array
        fluctuation function values (y axis)
      *alpha* : float
        estimation of DFA exponent
    """
    # cumulative sum of data with substracted offset
    y = np.cumsum(x - np.mean(x))
    scales = (2 ** np.arange(scale_lim[0], scale_lim[1], scale_dens)).astype(int)
    fluct = np.zeros(len(scales))
    # computing RMS for each window
    for e, sc in enumerate(scales):
        fluct[e] = np.sqrt(np.mean(calc_rms(y, sc) ** 2))
    # fitting a line to rms data
    coeff = np.polyfit(np.log2(scales), np.log2(fluct), 1)
    if show:
        import matplotlib.pyplot as plt
        fluctfit = 2 ** np.polyval(coeff, np.log2(scales))
        plt.loglog(scales, fluct, 'bo')
        plt.loglog(scales, fluctfit, 'r', label=r'$\alpha$ = %0.2f' % coeff[0])
        plt.title('DFA')
        plt.xlabel(r'$\log_{10}$(time window)')
        plt.ylabel(r'$\log_{10}$<F(t)>')
        plt.legend()
        plt.show()
    return scales, fluct, coeff[0]


def main():
    return


if __name__ == '__main__':
    main()
