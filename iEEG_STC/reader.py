#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 09:52:30 2021

@author: paulmueller
Contains the ECOGReader which can read EPILEPSIAE data files and apply downsampling and notch filter
"""

import pandas as pd
import numpy as np
from datetime import datetime
import scipy.signal as scs


class ECOGReader:
    """
    Workflow:
        1. Initialize object -> Reads headfile
        2. Call self.read_data() ->  Read datafile and applies conversion factor
        3. Preprocess, e.g., apply notch filter and cut of edges
    """
    def __init__(self, file):
        """
        Looks for the header file either in the folder of the data_file
        Reads in the header file
        """
        self.data_file = file.replace('.head', '.data')
        self.head_file = file.replace('.data', '.head')
        self.head_dict = {"segment_length": None, "data_changed": False,
                          "recordingID": self.data_file.split('/')[-1].split('.')[-2]}
        self.head_dict['patID'] = 'pat' + self.head_dict['recordingID'].split('_')[0][:-5]
        # Read in the keywords and values of the header file
        with open(self.head_file) as ccc:
            for line in ccc:
                line = line.replace('\n', '')  # Remove the endlines
                key, value = line.split('=')
                # Transform values to correct format and save to the head_dict
                if key == 'start_ts':
                    self.head_dict[key] = datetime.strptime(value + '000', "%Y-%m-%d %H:%M:%S.%f")
                elif key == 'conversion_factor':
                    self.head_dict[key] = float(value)
                elif key == 'elec_names':
                    self.head_dict[key] = np.array(value[1:-2].split(','))
                else:
                    self.head_dict[key] = int(value)
        # Varibles for later use
        self.data = None  # Holds the data
        self.segment_start_tiks = []  # Gives the index where a segment of length segment_length starts

    def read_data(self):
        """
        Read in the full data_file, applies the conversion factor, adds the time in seconds and
        saves it as pandas DataFrame to self.data.
        """
        with open(self.data_file, 'r') as f:
            a = np.fromfile(f, dtype=np.int16)
        length = int(len(a)/self.head_dict['num_channels'])
        data = np.zeros((length, self.head_dict['num_channels']))
        time_steps = np.arange(0, length)
        for i in time_steps:
            data[i, :] = a[i*self.head_dict['num_channels']:(i+1)*self.head_dict['num_channels']]

        self.data = pd.DataFrame(data * self.head_dict['conversion_factor'], columns=self.head_dict['elec_names'])
        # Additionally adding the time_points needed for future processing as the first item
        self.data.insert(0, 'Time', time_steps/self.head_dict['sample_freq'])

        if len(self.data.columns) < 2:
            raise Exception('No Channels found')
        duplicated_channels = self.data.columns[self.data.columns.duplicated()]
        if len(duplicated_channels) > 0:
            print(f'{duplicated_channels} found. They will be deleted')
            self.delete_channels(duplicated_channels)

    def __get_segment_length_in_ticks(self, length):
        return int(length*self.head_dict['sample_freq'])     

    def print_data(self):
        """ Print data to command line. """
        print(self.data.to_string(index=False))
    
    def get_segmented_channels_data(self, elec_names='all'):
        """
        elec_names : (list) The desired channels to extract. 'all' returns all channels. defaults to 'all'
        Returns
        -------
        modified head_dict, (first_segment,second_segment,...).
        """
        elec_names = np.array(elec_names)
        if elec_names == 'all':
            elec_names = self.head_dict["elec_names"]
        missing_channels = np.in1d(elec_names, self.head_dict["elec_names"])
        if any(missing_channels):
            raise KeyError(f' The channels {elec_names[missing_channels]} do not exist in the data.')
        segment_channel_series = []
        seg_length = int(self.head_dict['sample_freq']*self.head_dict["segment_length"])
        for seg_start in self.segment_start_tiks:
            df = self.data.iloc[list(range(seg_start, seg_start+seg_length)),
                                self.data.columns.get_indexer(["Time"]+elec_names)]
            df.reset_index(inplace=True, drop=True)
            segment_channel_series.append(df)
        modified_head_dict = self.head_dict
        modified_head_dict['elec_names'] = elec_names
        modified_head_dict['num_channels'] = len(elec_names)
        return modified_head_dict, segment_channel_series

    def assign_data(self, data):
        """Assign new values to the data. All channels must be given."""
        original_channels = sorted(self.data.columns)
        new_channels = sorted(data.columns)
        if new_channels != original_channels:
            raise Exception('Missing channels.')
        else:
            self.data = data
            self.head_dict["data_changed"] = True

    def delete_channels(self, elec_names):
        """ Delete the channels given in elec_names and adjust the head_dict accordingly """
        for ch in elec_names:
            if ch in self.head_dict['elec_names']:
                self.data.drop(ch, axis=1, inplace=True)
                self.head_dict['elec_names'] = self.head_dict['elec_names'][self.head_dict['elec_names'] != ch]
                self.head_dict['num_channels'] -= 1
            else:
                print(f'{ch} is anyways not in the data and will be ignored.')
    
    def down_sample(self, down_sample_to):
        """
        Down sample the data to down_sample_to by ignoring data which is no multiple of this new frequency
        down_sample_to : (float) Frequency the data should be down sampled to.
            If the sample frequency is not dividable by this frequency an error is raised.
        """
        if self.head_dict['sample_freq'] % down_sample_to != 0:
            raise Exception(f"DivisionError. {self.head_dict['sample_freq']} / {down_sample_to} is no integer.")
        else:
            sample_factor = int(self.head_dict['sample_freq'] // down_sample_to)
        tmp = self.data.loc[:, self.data.columns != 'Time'].apply(lambda x: scs.decimate(x, sample_factor))
        tmp.insert(0, 'Time', self.data.loc[:, 'Time'].values[::sample_factor])
        
        self.data = tmp
        del tmp
        self.head_dict['sample_freq'] = down_sample_to
        self.head_dict['num_samples'] = len(self.data)

    def cut_data_sym(self, cut_sym):
        """ Cut of data at beginning and end of the data
        cut_sym : (float) seconds to cut of at end and beginning
        """
        to_cut = int(cut_sym*self.head_dict['sample_freq'])
        self.data = self.data.iloc[to_cut:-to_cut]
        self.data.reset_index(inplace=True, drop=True)
        self.head_dict['num_samples'] = len(self.data)
    
    def newnotch1(self, cutoff_hz=[47.5, 52.5, 97.5, 102.5]):
        """
        Notch filter frequencies f0-f1, f2-f3, f4-f5,...etc .
        Default frequencies between 47.5-52.5, 97.5-102.5 Hz are cut out
        """
        nyq_rate = (self.head_dict['sample_freq']/2)  # calculate signal Nyquist frequency
        width = .6/nyq_rate  # filter width
        dip = 15.0  # desired attenuation
        # compute filter coefficients
        n, beta = scs.kaiserord(dip, width)
        n = n | 1
        # build bandpass window array normalized by nyquist frequency
        cutoff_hz_nyq = np.array(cutoff_hz)/nyq_rate
        # Create the finite impulse response filter from the window settings and cut off frequencies
        taps = scs.firwin(n, cutoff_hz_nyq, window=('kaiser', beta), pass_zero='bandstop')
        # Apply the filter forward and backard on all channels excluding the Time axis
        self.data = self.data.apply(lambda d: scs.filtfilt(taps, [1.0], d) if d.name not in ["Time"] else d)

    def default_data_read(self, down_sample_to, notch_filter, cut_sym_after_filter_in_sec,
                          path_to_quality=None, include_electrodes='all', **kwargs):
        """Convenience function which reads the data, deletes segments if a quality frame is given,
        includes only certain channels, downsamples, applies a notch filter and cuts of the borders.
        **kwargs are ignored"""
        self.read_data()
        # Check in the given quality file if there are channels to be excluded
        if path_to_quality is not None:
            quality = pd.read_csv(path_to_quality, index_col=0).loc[self.head_dict['recordingID']]
            to_delete_ch = np.array(self.head_dict['elec_names'])[~np.in1d(self.head_dict['elec_names'],
                                                                  quality.index[quality.values.astype(bool)])]
            self.delete_channels(to_delete_ch)
        # Exclude all channels whiche are not in include electrodes
        if isinstance(include_electrodes, str):
            if include_electrodes == 'all':
                pass
            else:
                raise ValueError(f'include_electrodes={include_electrodes} cannot be resolved')
        else:
            self.delete_channels(np.array(self.head_dict['elec_names'])[~np.in1d(self.head_dict['elec_names'],
                                                                                 include_electrodes)])
        if len(self.head_dict['elec_names']) == 0:
            raise ValueError('There are no channels left to read')
        self.newnotch1(notch_filter)
        self.down_sample(down_sample_to)
        self.cut_data_sym(cut_sym_after_filter_in_sec)
        return self.data


def main():
    pass


if __name__ == '__main__':
    main()
