#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 09:40:34 2021

@author: paulmueller
"""
import ast
import os
import numpy as np
import glob
import pandas as pd


def make_path(path):
    """Create directory at path (str) if it does not allready exists"""
    try:
        os.makedirs(path)
    except (FileExistsError, OSError):
        pass


def read_config_file(file):
    """
    Read the config file (str) and parse it into a dictionary
    """
    calc_parameters = {}
    # Read the configuration file line by line
    with open(file, 'r') as f:
        for line in f.readlines():
            line = line.replace(' ', '')
            if line.startswith('#'):
                continue
            elif '#' in line:  # Ignore comments marked with #
                line = line.split('#')[0]
            elif line == '\n':
                continue
   
            splitted_line = line.split('=')
            calc_parameters[splitted_line[0]] = splitted_line[1].replace('\n', '').replace(' ', '')
    calc_parameters = _convert_parameters_to_correct_type(calc_parameters)
    return calc_parameters


def _convert_parameters_to_correct_type(parameters):
    """ Try to convert the values of parameters to the tyoe inferring through ast.literal_eval
    and leave them as str if not possible"""
    for key in parameters:
        try:
            parameters[key] = ast.literal_eval(parameters[key])
            continue
        except ValueError:
            pass
    
    return parameters


def data_file_locations_from_settings(settings_as_dict, path_to_general):
    """Infer file locations from settings and the path_to_general
    returns locations dict ->
    data: raw data file
    head: raw data head file
    psd: path to power spectrums
    processed: path to post processed files"""
    locations = dict()
    locations['meta'] = path_to_general + '/' + settings_as_dict['patID'] + '/meta/'
    settings = (f"{settings_as_dict['band_power_window_length_in_secs']}_{settings_as_dict['welch_res']}_" +
                f"{settings_as_dict['welch_window']}_{settings_as_dict['welch_overlap']}_" +
                f"{settings_as_dict['welch_detrend']}_{settings_as_dict['down_sample_to']}_" +
                f"{settings_as_dict['cut_sym_after_filter_in_sec']}_{settings_as_dict['log']}")
    if 'recordingID' in settings_as_dict.keys():
        data_path = path_to_general + '/' + settings_as_dict['patID'] + '/data/' + settings_as_dict['recordingID']
        locations['psd'] = data_path + '/' + settings
   
    locations['processed'] = path_to_general + '/' + settings_as_dict['patID'] + '/processed/' + settings
    locations['settings'] = settings
     
    return locations


def check_if_calcparams1_in_calcparams2(calcparams1, calcparams2):
    """Check if calcparams1 shares all values with calcparmas2 but calcparams2 can have more params"""
    subset = True
    for key in calcparams1:
        try:
            subset *= (calcparams1[key] == calcparams2[key])
        except KeyError:
            subset = False   
    return subset


def read_and_give_frame(glob_string, quality_frame):
    """Read multiple files (use * or ? in glob_string) and concatenates them all into one dataframe.
    Uses the quality_frame to kick out times for which we know that the data is bad."""
    out = pd.DataFrame()
    # Get all possible files
    for file in glob.glob(glob_string):
        splitted = file.split('_')
        # Check if files are surrogate data to get the recording ID correct (used in quality frame)
        if 'surr' in splitted[-1]:
            recording_id = splitted[-3] + '_'+splitted[-2]
        else:
            recording_id = splitted[-2] + '_' + splitted[-1].replace('.csv', '')
        # Get the quality of the data from quality frame through the recording ID
        q = quality_frame.loc[recording_id].astype(bool)
        tmp = pd.read_csv(file, index_col=0)
        tmp = tmp.loc[:, np.intersect1d(q.index, tmp.columns)]
        tmp = tmp.loc[:, q]
    
        out = out.append(tmp)

    # Convert the index to time and measure if it is save as 'timestring measure'
    if isinstance(out.index[0], str) and len(out.index[0].split(' ')) == 2:
        # Delete errors in the data where no measure and time is given
        out = out.loc[np.array([len(i.split(' ')) == 2 for i in out.index])]

        time, measure = np.array([i.split(' ') for i in out.index]).T
        time = time.astype(float)
        out['Measure'] = measure
        out.index = time
        out.index.name = 'Time'
    return out


def read_series_without_quality(glob_string):
    """Read multiple files (use * or ? in glob_string) and concatenates them all into one Series.
    Does not look at quality of data"""
    out = pd.Series()
    for file in glob.glob(glob_string):
        tmp = pd.read_csv(file, index_col=0, squeeze=True)
    
        out = out.append(tmp)
    if isinstance(out.index[0], str) and len(out.index[0].split(' ')) == 2:
        time, measure = np.array([i.split(' ') for i in out.index]).T
        time = time.astype(float)
        out['Measure'] = measure
        out.index = time
        out.index.name = 'Time'
    return out
