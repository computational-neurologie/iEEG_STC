#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 15:03:00 2021

@author: paulmueller
"""

import iEEG_STC.get_approx_roi as gar
import pandas as pd
import numpy as np 
import mne


def eeg_to_mni(name, path_to_ref='Okamoto_et_al_2004_NeuroImage.csv'):
    """
    Convert standard eeg positions to approximated MNI positions
    name:  name of eeg electrode
    path_to_ref: path to reference data for conversion of eeg to mni. (default: Okamoto_et_al_2004_NeuroImage.csv)
    """
    conversion_table = pd.read_csv(path_to_ref, index_col=0, header=0)
    if name.upper() in conversion_table.index:
        return (conversion_table.loc[name, 'MNI x'],
                conversion_table.loc[name, 'MNI y'],
                conversion_table.loc[name, 'MNI z'])
    else:
        return False


def delete_duplicated_vertices(label1, label2):
    """Delete vertices from label1 and label2 if they occur in both"""
    common = np.intersect1d(label1.vertices, label2.vertices)
    for val in common:
        label1.vertices = label1.vertices[label1.vertices != val]
        label2.vertices = label2.vertices[label2.vertices != val]
    return label1, label2

    
def make_mueller2021_annot_files(path_to_brain_in_freesurfer,
                                 subject_in_freesurfer):
    """
    Needs freesurfer installed
    Goes through all vertices of a given freesurfer compatible brain and assigns it a label (MT, LIP, LPFC, OFC, ACC)
    If non of these is found the vertex is ignored.
    With these assigned vertices labels are created and saved to .annot files (lh, rh)
    """
    # Definition of all possible labels, their names and their colors
    lh_MT = mne.Label(vertices=[], name='MT', color=np.array((245, 66, 66, 255))/255, hemi='lh')
    rh_MT = mne.Label(vertices=[], name='MT', color=np.array((245, 66, 66, 255))/255, hemi='rh')
    lh_LIP = mne.Label(vertices=[], name='LIP', color=np.array((245, 179, 66, 255))/255, hemi='lh')
    rh_LIP = mne.Label(vertices=[], name='LIP', color=np.array((245, 179, 66, 255))/255, hemi='rh')
    lh_LPFC = mne.Label(vertices=[], name='LPFC', color=np.array((81, 173, 103, 255))/255, hemi='lh')
    rh_LPFC = mne.Label(vertices=[], name='LPFC', color=np.array((81, 173, 103, 255))/255, hemi='rh')
    lh_OFC = mne.Label(vertices=[], name='OFC', color=np.array((82, 138, 242, 255))/255, hemi='lh')
    rh_OFC = mne.Label(vertices=[], name='OFC', color=np.array((82, 138, 242, 255))/255, hemi='rh')
    lh_ACC = mne.Label(vertices=[], name='ACC', color=np.array((125, 36, 166, 255))/255, hemi='lh')
    rh_ACC = mne.Label(vertices=[], name='ACC', color=np.array((125, 36, 166, 255))/255, hemi='rh')
    # Go through every label of the hemis=0 (Should be left)
    i = 0
    while True:
        try:
            # Get vertex in MNI
            vertex_in_mni = np.round(mne.vertex_to_mni(i, hemis=0, subject=subject_in_freesurfer), 0)
            # Get most likely roi
            lz, dist = gar.assign_roi(*vertex_in_mni)
            print(i, lz, dist, vertex_in_mni)
            # Assign vertex to labels based on most likely roi
            if lz == 'MT':
                lh_MT.vertices = np.append(lh_MT.vertices, i)
            elif lz == 'LIP':
                lh_LIP.vertices = np.append(lh_LIP.vertices, i)
            elif lz == 'LPFC':
                lh_LPFC.vertices = np.append(lh_LPFC.vertices, i)
            elif lz == 'OFC':
                lh_OFC.vertices = np.append(lh_OFC.vertices, i)
            elif lz == 'ACC':
                lh_ACC.vertices = np.append(lh_ACC.vertices, i)
            
            i += 1
        except IndexError:
            break
    # Go through every label of the hemis=1 (Should be right)
    i = 0
    while True:
        try:
            # Get vertex in MNI
            vertex_in_mni = np.round(mne.vertex_to_mni(i, hemis=1, subject=subject_in_freesurfer), 0)
            # Get most likely roi
            lz, dist = gar.assign_roi(*vertex_in_mni)
            print(i, lz, dist, vertex_in_mni)
            # Assign vertex to labels based on most likely roi
            if lz == 'MT':
                rh_MT.vertices = np.append(rh_MT.vertices, i)
            elif lz == 'LIP':
                rh_LIP.vertices = np.append(rh_LIP.vertices, i)
            elif lz == 'LPFC':
                rh_LPFC.vertices = np.append(rh_LPFC.vertices, i)
            elif lz == 'OFC':
                rh_OFC.vertices = np.append(rh_OFC.vertices, i)
            elif lz == 'ACC':
                rh_ACC.vertices = np.append(rh_ACC.vertices, i)
            i += 1
        except IndexError:
            break

    # Join the labels to a list and check if two labels have the same vertices. Delete them in both if so
    lLabels = [lh_MT, lh_LIP, lh_LPFC, lh_OFC, lh_ACC]
    rLabels = [rh_MT, rh_LIP, rh_LPFC, rh_OFC, rh_ACC]
    for i in range(len(lLabels)):
        for j in range(len(lLabels)):
            if i == j:
                continue
            else:
                lLabels[i], lLabels[j] = delete_duplicated_vertices(lLabels[i], lLabels[j])
                rLabels[i], rLabels[j] = delete_duplicated_vertices(rLabels[i], rLabels[j])
    # Write the annot files
    mne.write_labels_to_annot(lLabels, subject=subject_in_freesurfer, subjects_dir=path_to_brain_in_freesurfer,
                              parc='mueller2021_afni', annot_fname='lh.mueller2021_afni.annot', overwrite=True)
    mne.write_labels_to_annot(rLabels, subject=subject_in_freesurfer, subjects_dir=path_to_brain_in_freesurfer,
                              parc='mueller2021_afni', annot_fname='rh.mueller2021_afni.annot', overwrite=True)

    
if __name__ == '__main__':
    make_mueller2021_annot_files()
