#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 09:28:25 2021

@author: paulmueller

Needs AFNI installed
"""
import numpy as np
import subprocess as sp

# Definitions of the regions also found in Murray et al., 2014
# using the Glasser et al., 2016 atlas and its literals in AFNI which must be installed to use the latter functions
MT = {'NAME': 'MT', 'COLOR': (0.8392156862745098, 0.15294117647058825, 0.1568627450980392),
      'MNI_Glasser_HCP_v1.0': ['Medial_Superior_Temporal_Area', 'Middle_Temporal_Area',
                               # Changes  / Comment out next lines to get old version
                               'Area_V3CD', 'Area_FST', 'Area_V4t', 'Area_Lateral_Occipital_3',
                               'Area_Lateral_Occipital_2', 'Area_Lateral_Occipital_1', 'Area_PH']
      }
LIP = {'NAME': 'LIP', 'COLOR': (1.0, 0.4980392156862745, 0.054901960784313725),
       'MNI_Glasser_HCP_v1.0': ['Medial_IntraParietal_Area',
                                'Ventral_IntraParietal_Complex', 'Medial_IntraParietal_Area',
                                'Area_Lateral_IntraParietal_dorsal', 'Anterior_IntraParietal_Area']
       }
LPFC = {'NAME': 'LPFC', 'COLOR': (0.17254901960784313, 0.6274509803921569, 0.17254901960784313),
        'MNI_Glasser_HCP_v1.0': ['Area_8Ad', 'Area_8Av', 'Area_9_Posterior', 'Area_8C',
                                 'Area_posterior_9-46v', 'Area_46', 'Area_anterior_9-46v',
                                 'Area_9-46d', 'Inferior_6-8_Transitional_Area',
                                 'Superior_6-8_Transitional_Area', 'Area_8B_Lateral',
                                 'Superior_Frontal_Language_Area', 'Area_44', 'Area_45',
                                 # Changes / Comment out next lines to get old version
                                 'Area_9-46d', 'Area_9_anterior', 'Area_IFJa', 'Area_IFJp', 'Area_IFSp',
                                 'Area__IFSa', 'Area_47l', 'Area_posterior_47r']
        }
OFC = {'NAME': 'OFC', 'COLOR': (0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
       'MNI_Glasser_HCP_v1.0': ['Orbital_Frontal_Complex', 'posterior_OFC_Complex',
                                # Changes / Comment out next line to get old version
                                'Area_47s', 'Area_47m', 'Area_11l', 'Area_13l', 'Area_anterior_10p', 'Polar_10p',
                                'Area_posterior_10p', 'Area_10d', 'Area_anterior_47r']
       }
ACC = {'NAME': 'ACC', 'COLOR': (0.5803921568627451, 0.403921568627451, 0.7411764705882353),
       'MNI_Glasser_HCP_v1.0': ['Area_33_prime', 'Area_Posterior_24_prime', 'Anterior_24_prime', 'Area_a24',
                                'Area_dorsal_32', 'Area_posterior_24', 'Area_anterior_32_prime', 'Area_s32',
                                'Area_p32_prime']
       }
murray_regions = [MT, LIP, LPFC, OFC, ACC]


def assign_roi(x, y, z, regions=murray_regions, valid_atlases=['MNI_Glasser_HCP_v1.0'], return_subparc=False,
               max_dist=9.5, cwd=None):
    """ Use AFNI to assign a region of interest (roi) to an electrode position (x,y,z)
    x,y,z: electrode coordinates in MNI space (mm)
    regions: are defined in the beginning of this file but you can provide custom ones as well
    valid_atlases: atlases to use from the AFNI available ones (default: 'MNI_Glasser_HCP_v1.0')
    return_subparc: if True also return the regions from the valid_atlases not only regions (default: False)
    max_dist: Maximum distance to region to attribute electrode to it (default: 9.5)
    cwd: Change current working directory (default: None = executing directory)
    """
    min_dist = np.inf
    likely_zone = None  # Closest zone/region found
    atlas = None
    sub_parc = None  # Area as in the valid_atlases
    # Command in AFNI to get rois
    command = ['whereami', str(-x), str(-y), str(z), '-space', 'MNI', '-max_search_radius', str(max_dist)]
    # Add the valid atlases to the command
    for A in valid_atlases:
        command.append('-atlas')
        command.append(A)
    # Run AFNI whereami
    process = sp.Popen(command, stdout=sp.PIPE, stderr=sp.PIPE, cwd=cwd)
    stdout, stderr = process.communicate()
    # Read output of AFNI whereami
    stdout = stdout.decode('utf-8')
    # Check if any zone was found
    if 'Within' in stdout:
        for line in stdout.split('\n'):
            # Extract cooridantes to check for AFNI error
            if '{MNI}' in line:
                coords = np.array([float(val.replace(' ', '').split('mm')[0]) for val in line.split(',')])
                # Raise an error if fishy coordinate mismatch happened. This should never occur
                if any(coords != np.array([x, y, z])):
                    raise Exception(f'Cooridnates do not match {coords}, {x},{y},{z})')
            # Use this atlas for the next checks
            elif 'Atlas' in line:
                tmp = line.split(':')[0].replace(' ', '')[5:]
                if tmp in valid_atlases:
                    atlas = tmp
                    del tmp
            # Get rois in which the electrode is located
            elif 'Focus point' in line or 'Within' in line:
                if atlas in valid_atlases:
                    dist, area = line.split(':')
                    # If it is in the focus point the distance is 0
                    if 'Focus point' in line:
                        dist = 0
                    # Else extract the distance
                    else:
                        dist = float(dist.split(' ')[-2])
                    # Extract area
                    area = area.replace(' ', '')
                    # Check if the is in any of the regions
                    for ROI in regions:
                        if any(zone+'?' in area+'?' for zone in ROI[atlas]):
                            # Check if the distance is smaller than any other regions and then replace the closeest
                            if dist < min_dist:
                                min_dist = dist
                                likely_zone = ROI['NAME']
                                sub_parc = area
                            # If the distance is equal to the smallest dist, add this region to the existing closest
                            elif dist == min_dist:
                                if likely_zone != ROI['NAME']:
                                    likely_zone = likely_zone + ', ' + ROI['NAME']
                                sub_parc = sub_parc + ', ' + area
                        else:
                            pass
    if return_subparc:
        return likely_zone, min_dist, sub_parc
    else:
        return likely_zone, min_dist
