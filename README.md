Spatial and temporal correlations are interdependent, change with vigilance 
and anti-epileptic drug action, and are follow a cortical gradient. 
This was shown in Müller and Meisel 2022 on data from the EPILEPSIAE data bank.
The here provided scripts were used to entangle this information from the iEEG
data.

Workflow:

1. Put iEEG_STC/iEEG_STC on your PYTHONPATH


2. Changes in the scripts:
   
   a. Change the <PATH_TO_PROJECT> to the path to your data and <PATH_TO_RAW> to your raw data path 
      (in scripts/calc_ACF_CC.py, scripts/calc_LRTC_CC_simple.py, scripts/make_electrode_positions_and_drug_times.py,
      scripts/elec_to_whereami.py)
   
   optional: change the default_patient in scripts/elec_to_whereami.py


3. Build look up tables
   
   a. make_electrode_positions_and_drug_times.py (converts EPILEPSIAE information to csv format. 
   Save drug.txt, elecs.txt and seizures.txt to the patients meta folder. These files should just be the plain text 
   copys of the EPILEPSIAE web pages with the corresponding information)
   
   b. elec_to_whereami.py -> Read electrodes.csv position and converts them to likely zones 
   (zones are from Murray et al. 2014)


4. Vigilance Index after Reed et al. 2018 
   
   a. Calculate vigilance indices via running scripts/calc_LRTC_CC_simple.py with a config file containing "VI_Reed"
   
   b. Build a classifier file with scripts/make_classifier_AED_VI.py. This then has low/high AED and SWS/nonSWS


5. Calculate spatial/temporal correlations
   
   a. For the state wise (low/high AED and SWS/nonSWS) analysis run scripts/calc_ACF_CC.py -> STC measures are 
   implemented in iEEG_STC/utils.py and can be applied to the output of cripts/calc_ACF_CC.py
   
   b. For segment wise calculation use calc_STC_simple.py

Suggested project structure:

raw_data/patientID/data_files.dat

project/patientID/data/dir_per_data_file

project/patientID/meta

project/patientID/processed/figures

Other subdirectories should be written by the scripts themselves